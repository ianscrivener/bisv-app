
README (BISV) 
-------------------------

DEVELOPED FOR:
	Office of Environment and Heritage
	April 2015

DEVELOPED BY:
	NGIS
	t +61-8-9355 6000 
	f +61-8-9355 6099
	Suite 1a, 53-63 Burswood Road 
	PO Box 126 Burswood WA 6100
	www.ngis.com.au

SITE DETAILS:
	URL: [HOST NAME]/BISV/index.html

CONFIGURATION DETAILS:
	User configuration file is located relative to this file: /app/config.js
	Edit config.js using a text editor such as Notepad++