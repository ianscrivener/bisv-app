﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BISV.aspx.cs" Inherits="BISV" %>
<!DOCTYPE html>
  <html  xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta charset="utf-8" />
      <!--<meta http-equiv="X-UA-Compatible" content="IE=Edge">
      <meta http-equiv="Content-Type" content="text/html" charset="utf-8">-->
      <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no" />
      <title>Biodiversity Investment Spatial Viewer</title>
      <link rel="stylesheet" href="//js.arcgis.com/3.14/esri/css/esri.css" />
      <link rel="stylesheet" href="//js.arcgis.com/3.14/dijit/themes/tundra/tundra.css" />
      <link rel="stylesheet" href="//js.arcgis.com/3.14/dojox/layout/resources/ExpandoPane.css" />
      <link rel="stylesheet" href="app/ngisWidget/css/TOC.css" />
      <link rel="stylesheet" href="app/ngisWidget/css/mapNavigationWidget.css" />
      <link rel="stylesheet" href="app/ngisWidget/css/mapScaleControlWidget.css" />
      <link rel="stylesheet" href="app/ngisWidget/css/contentPaneWidget.css" />
      <link rel="stylesheet" href="app/ngisWidget/css/drawWidget.css" />
      <link rel="stylesheet" href="app/ngisWidget/css/selectWidget.css" />
      <link rel="stylesheet" href="app/ngisWidget/css/searchWidget.css" />
      <link rel="stylesheet" href="app/ngisWidget/css/printWidget.css" />
      <link rel="stylesheet" href="app/ngisWidget/css/areaReportWidget.css" />
      <link rel="stylesheet" href="app/ngisWidget/css/infoReportWidget.css" />
      <link rel="stylesheet" href="css/footer.css" />
      <link rel="stylesheet" href="css/index.css" />
    </head>
    
    <body class="tundra">
      <div id="disclaimer" style="display: none;" data-dojo-type="dijit/Dialog" data-dojo-id="disclaimerDialog" title="Terms and Disclaimer Agreement">
        <div id="disclaimerText" style="width:400px; border:1px solid #b7b7b7; background:#fff; padding:8px; margin:0 auto; overflow:auto;">
        </div><br />
        <table>
          <tr><td>
            <button id="acceptButton" data-dojo-type="dijit/form/Button" type="button">I Accept</button>
          </td><td>
            <button id="declineButton" data-dojo-type="dijit/form/Button" type="button">I Decline</button>
          </td></tr>
        </table>
      </div>
      <div id="loadingOverlay" class="loadingOverlay pageOverlay">
        <div class="loadingMessage">Loading Map...</div>
      </div>
    	<div id="mainContainer" data-dojo-type="dijit/layout/BorderContainer" data-dojo-props='persist:false, gutters:false'> 
<!-- TITLE PANEL --------------------------------------------------------------------------------------------------------------------------->
          <div id="titlePane" data-dojo-type="dijit/layout/ContentPane" data-dojo-props='region:"top", splitter:false'>        
            <div id="titleContainer" data-dojo-type="dijit/layout/BorderContainer" data-dojo-props='persist:false, gutters:false'>                 
              <div id="titleInnerPane1" data-dojo-type="dijit/layout/ContentPane" data-dojo-props='region:"center", splitter:false'>
                <div id="panelTitle">
                  <div id="panelTitleText">
                    <h1 class="title">Biodiversity Investment Spatial Viewer</h1>
                  </div>
<!-- TOOLBAR PANEL --------------------------------------------------------------------------------------------------------------------------->
                  <div id="mainToolbar" class="panelToolbar"> <!--data-dojo-type="dijit/MenuBar"-->
                    <div id="toolSearch" tabindex="1" data-dojo-type="dijit/MenuBarItem" title="Search + Info"></div>
                    <div id="toolSelect" tabindex="2" data-dojo-type="dijit/MenuBarItem" title="Select + Info"></div>
                    <div id="toolReport" tabindex="3" data-dojo-type="dijit/MenuBarItem" title="Show/Hide Report Panel" class="toggleButton"></div>
                    <div id="toolDraw" tabindex="4" data-dojo-type="dijit/MenuBarItem" title="Annotate / Draw"></div>
                    <div id="toolMeasure" tabindex="5" data-dojo-type="dijit/MenuBarItem" title="Measure"></div>
                    <div id="toolPrint" tabindex="6" data-dojo-type="dijit/MenuBarItem" title="Print Map"></div>
                    <div id="toolBookmark" tabindex="7" data-dojo-type="dijit/MenuBarItem" title="Bookmark"></div>
                    <div id="toolHelp" tabindex="8" data-dojo-type="dijit/MenuBarItem" title="Help + Contact"></div>
                    <div id="logout"></div>
                  </div>
                </div>
              </div>
              <div id="titleInnerPane2" data-dojo-type="dijit/layout/ContentPane" data-dojo-props='region:"right", splitter:false'>
                <div id="panelLogo">
                  <img src="images/waratah15x.png" width="66" height="64" alt="Link to NSW Government home page" />
                  <img src="images/deptitle15x.png" width="103" height="62" alt="Link to Office of Environment and Heritage home page" />
                </div>
              </div>
            </div>
          </div>
<!-- TASK PANEL --------------------------------------------------------------------------------------------------------------------------->
          <div id="taskExpandoPanel" data-dojo-type="dojox/layout/ExpandoPane" data-dojo-props='region:"left", splitter:true, minSize:300'>
            <div id="taskPanel" data-dojo-type="dijit/layout/BorderContainer" data-dojo-props='persist:false, gutters:false'>
              <div id="taskStack" data-dojo-type="dijit/layout/StackContainer" data-dojo-props='region:"top", splitter:true, minSize:100'>
                <div id="panelBookmark" data-dojo-type="app/ngisWidget/contentPaneWidget" data-dojo-props='title:"Bookmark"'>
                  <fieldset>
                    <div class="taskContentPanel" id="bookmark">
                    </div>
                  </fieldset>
                </div>
                <div id="panelSearch" data-dojo-type="app/ngisWidget/contentPaneWidget" data-dojo-props='title:"Search + Info"'>
                  <div class="taskContentPanel" id="searchPanel"></div>
                </div>
                <div id="panelSelect" data-dojo-type="app/ngisWidget/contentPaneWidget" data-dojo-props='title:"Select + Info"'>
                  <div class="taskContentPanel" id="selectPanel"></div>
                </div>
                <div id="panelReport" data-dojo-type="app/ngisWidget/contentPaneWidget" data-dojo-props='title:"Report"'>
                </div>
                <div id="panelDraw" data-dojo-type="app/ngisWidget/contentPaneWidget" data-dojo-props='title:"Annotate / Draw"'>
                  <div class="taskContentPanel" id="drawPanel"></div>
                </div>
                <div id="panelMeasure" data-dojo-type="app/ngisWidget/contentPaneWidget" data-dojo-props='title:"Measure"'>
                  <div class="taskContentPanel" id="measurePanel"></div>
                </div>
                <div id="panelPrint" data-dojo-type="app/ngisWidget/contentPaneWidget" data-dojo-props='title:"Print Map"'>
                  <div class="taskContentPanel" id="printPanel">
                  </div>
                </div>
                <div id="panelHelp" data-dojo-type="app/ngisWidget/contentPaneWidget" data-dojo-props='title:"Help + Contact"'>
                  <div class="taskContentPanel" id="helpPanel">
                    <div class="contact" id="helpContent">
                    </div>
                  </div>
                </div>
              </div>
<!-- MAP LAYERS PANEL --------------------------------------------------------------------------------------------------------------------------->
              <div id="panelLayers" data-dojo-type="app/ngisWidget/contentPaneWidget" data-dojo-props='region: "center", title:"Map Layers"'>
                <div data-dojo-type="dijit/TitlePane" data-dojo-props="title: 'Base Maps'"><div class="basemapContentPanel" id="basemapGallery"></div></div>
                <div data-dojo-type="dijit/TitlePane" data-dojo-props="title: 'Information Layers to Display on Map'"><div class="tocContentPanel" id="toc"></div></div>
              </div>
            </div>
          </div>
<!-- MAP --------------------------------------------------------------------------------------------------------------------------->
          <div id="centerContainer" data-dojo-type="dijit/layout/BorderContainer" data-dojo-props='region:"center"'>              
            <div class="contentCollapseWrapper">
              <div id="TOCContentCollapse" class="contentCollapse open"><div class="contentCollapseLabelOpen">&laquo;</div><div class="contentCollapseLabelClosed">&raquo;</div></div>
            </div>
            <div id="mapPane" data-dojo-type="dijit/layout/ContentPane" data-dojo-props='region:"center"'>
              <div id="mapDiv">
                <div id="mapNavigation"></div>
                <div id="mapScaleControl"></div>
              </div>
            </div>
<!-- REPORT --------------------------------------------------------------------------------------------------------------------------->
            <div id="resultsExpandoPanel" data-dojo-type="dojox/layout/ExpandoPane" data-dojo-props='region:"bottom", splitter:true, minSize:120, title:"&nbsp;Report Results"' style="display: block;">
              <div id="reportHint" class="reportHint"></div>
              <div id="resultsPane" data-dojo-type="dijit/layout/TabContainer" style="width: 100%;">
                <div id="infoReportTab" class="reportContentPane" data-dojo-type="dijit/layout/ContentPane" title="Plan Info Report" data-dojo-props="selected:true">
                  <div id="infoReport">
                  </div>
                </div>
                <div id="areaReportTab" class="reportContentPane" data-dojo-type="dijit/layout/ContentPane" style="height: 100%;" title="Area Aggregate Report">
                  <div id="areaReport">
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
      
      <!-- configure Dojo -->
      <script>
        // Create a dojoConfig object *before* we load dojo.js (via //js.arcgis.com/3.11/)
        // Note: Package location and blank.html creates an absolute path that is derived from the path of the current HTML file. 
        //       Using absolute paths is necessary for the Dojo 1.8 loader to resolve local modules correctly.
        var dojoConfig = {
          async: true,
          isDebug: true,             // !!! DEV USE ONLY
          dojoBlankHtmlUrl: location.pathname.replace(/\/[^/]*$/, '') + '/blank.html',
          paths: { agsjs: location.pathname.replace(/\/[^/]+$/, '') + '/agsjs' },
          // Register the correct location of the local "app" package so we can load
          // Dojo from the CDN whilst still being able to load local modules.
          packages: [{
            name: "app",
            location: location.pathname.replace(/\/[^/]*$/, '') + '/app' 
          }]
        };
      </script>
      <!-- load ArcGIS JS API (includes Dojo) -->
      <script type="text/javascript" src="//js.arcgis.com/3.11/"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
      <script>
        require([
          "app/ngisWidget/contentPaneWidget", "dijit/form/Select", "dijit/form/TextBox", "dijit/form/Button", "dijit/MenuBarItem", "dijit/TitlePane", "dijit/layout/TabContainer", "dijit/layout/ContentPane",
          "app/utility", "app/uiwireup", "app/startmap", "dojo/ready"
        ], function (
          contentPaneWidget, Select, TextBox, Button, MenuBarItem, TitlePane, TabContainer, ContentPane,
          utility, UIWireUp, StartMap, ready) {
          ready(function () {
            var accepted = function() {
              // Load map
              StartMap.start(true, document.getElementById("hdnuserguid").value, false);
            };
            var rejected = function() {
              window.location.href = "http://www.environment.nsw.gov.au"; // redirect 
              //history.go(-1); // Navigate back
            };
            utility.page.startLoading();
            utility.page.loadDisclaimer(accepted, rejected);
          });
        });
      </script>
      <form id="form1" runat="server" style="width: 0%; height: 0%">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <asp:HiddenField ID="hdnuserguid" runat="server" />
        <asp:HiddenField ID="hdnservertoken" runat="server" />
      </form>

      <!--#include virtual="/includes/decc/inc_DECCfooter_v2.inc"-->
    </body>
  </html>