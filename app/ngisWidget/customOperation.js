/*
 | Author: NGIS 2015 (www.ngis.com.au) -> Kristy Carter
 | Module: CustomOperation
 | Description: Custom undo operation - ESRI UndoManager custom operations for drawing and editing graphics on the map.
 | Usage: Used in ngisWidget.DrawWidget
 |
 | CHANGE LOG:
 |   2015-04-01 Kristy Carter: Initial code release.
 */
 
 define([
  "dojo/_base/declare", "esri/OperationBase", "dojo/_base/lang",
], 
function(declare, OperationBase, lang) 
{
  var customOp = {};
  
  customOp.Add = declare(OperationBase, {
    label: "Add graphic",
    
    constructor: function ( /*graphicsLayer, graphic*/ params) {
      params = params || {};
      if (!params.graphicsLayer) {
        console.error("customOperation.Add:: graphicsLayer is not provided!");
        return;
      }
      this.graphicsLayer = params.graphicsLayer;
      if (!params.graphic) {
        console.error("customOperation.Add:: no graphics provided!");
        return;
      }
      this._graphic = params.graphic;
      
    },

    performUndo: function () {
      this.graphicsLayer.remove(this._graphic);
    },

    performRedo: function () {
      this.graphicsLayer.add(this._graphic);
    }   
  });

  customOp.Delete = declare(OperationBase, {
    label: "Delete graphic",
    
    constructor: function ( /*graphicsLayer, graphic*/ params) {
      params = params || {};
      if (!params.graphicsLayer) {
        console.error("customOperation.Delete:: graphicsLayer is not provided!");
        return;
      }
      this.graphicsLayer = params.graphicsLayer;
      if (!params.graphic) {
        console.error("customOperation.Delete:: no graphics provided!");
        return;
      }
      this._graphic = params.graphic;
    },
    
    performUndo: function () {
      this.graphicsLayer.add(this._graphic);
    },

    performRedo: function () {
      this.graphicsLayer.remove(this._graphic);
    }
  });


  customOp.Update = declare(OperationBase, {
    label: "Edit graphic",
    
    constructor: function ( /*graphicsLayer, originalGeometry, graphic*/ params) {
      params = params || {};
      if (!params.graphicsLayer) {
        console.error("customOperation.Add:: graphicsLayer is not provided!");
        return;
      }
      this.graphicsLayer = params.graphicsLayer;
      if (!params.graphic) {
        console.error("customOperation.Add:: no graphics provided!");
        return;
      }
      this._originalGeometry = lang.clone(params.originalGeometry);
      this._graphic = params.graphic;
      this._updatedGeometry = lang.clone(this._graphic.geometry);
    },

    performUndo: function () {
      //re-instate previous geometry
      this._graphic.setGeometry(this._originalGeometry);
    },

    performRedo: function () {
      //re-instate updated geometry
      this._graphic.setGeometry(this._updatedGeometry);
    }   
  });

  return customOp;
});