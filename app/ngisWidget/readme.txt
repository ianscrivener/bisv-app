 | Module: MapNavigationWidget
 | Description: Map Navigation Widget - user controls for map navigation.
 |   Provides the following user interface elements...
 |    Home button - Return the map to the initial/default extent.
 |    Previous button - Move back one position in map extent history and display the extent in the map.
 |    Next button - Move forward one position in map extent history and display the extent in the map.
 | API:
 |  [Properties] map, extent, loaded, theme, visible
 |  [Methods] startup(), destroy(), hide(), show(), home(), previous(), next()
 |  [Events] home, previous, next, load
 | Usage:
 |    [html]:
 |        <link rel="stylesheet" href="app/ngisWidget/css/mapNavigationWidget.css" />
 |        </head>
 |        ..
 |        <body>
 |        <div id="mapDiv">
 |          <div id="mapNavigation"></div>
 |        </div>
 |    [css (to override position of navigation button stack)]:
 |      .mapNavigationContainer {
 |        position: absolute;
 |        top: 95px;
 |        left: 20px;
 |        z-index: 50;
 |      }
 |    [js]:
 |      var mapNavigationWidget = new MapNavigationWidget({
 |         theme: "MapNavigation",
 |         map: map,
 |         extent: null,  // will use current map extent
 |         visible: true,
 |      }, "mapNavigation");
 |      mapNavigationWidget.startup();
 
 
 
 | Module: MapScaleControlWidget
 | Description: Map Scale Control Widget - user control for manually adjusting the visible map scale.
 |    Provides user interface elements that allow the user to change the current map scale by selecting from the drop down list or entering any numeric scale.
 | API:
 |  [Properties] map, scaleOptions, loaded, visible
 |  [Methods] startup(), destroy(), hide(), show()
 |  [Events] load
 | Usage:
 |    [html]:
 |        <link rel="stylesheet" href="app/ngisWidget/css/mapScaleControlWidget.css" />
 |        </head>
 |        ..
 |        <div id="mapDiv">
 |          <div id="mapScaleControl"></div>
 |        </div>
 |    [css (to override position of map scale control)]:
 |      #mapScaleControl {
 |        position: absolute;
 |        bottom: 60px;
 |        left: 20px;
 |        z-index: 50;
 |      }
 |    [js]:
 |      var mapScaleControl = new MapScaleControlWidget({
 |         map: map,
 |         scaleOptions: [500000, 100000, 50000, 10000, 5000, 1000],
 |         visible: true,
 |      }, "mapScaleControl");
 |      mapScaleControl.startup();