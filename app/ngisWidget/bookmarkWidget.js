/*
 | Author: NGIS 2015 (www.ngis.com.au) -> Kristy Carter
 | Module: BookmarkWidget
 | Description: Bookmark Widget - user control for saving map viewer state.
 |    Extends the ArcGIS Bookmark Widget to allow the user to save and restore BISV search criteria, layer visibility 
 |    and map extent as a bookmark. Bookmarks are persisted in the browser's local storage or, (worst case) a cookie.
 | Usage: See readme.
 |
 | CHANGE LOG:
 |   2015-04-14 Kristy Carter: Initial code release.
 |   2015-07-21 Kristy Carter: Implement workaround for ArcGIS Server's group layer visibility bugginess.
 |   2015-08-04 Kristy Carter: Implement accessibility, allow non-mouse users access to bookmark items via tab->enter.
 */
 define([
	"dojo/_base/declare",
  "dojo/Evented",
	"dijit/_WidgetBase",
	"esri/dijit/Bookmarks",
  "esri/layers/layer",
  "esri/layers/ArcGISDynamicMapServiceLayer",
  "esri/dijit/BasemapGallery",
  "esri/dijit/Basemap",
  "dojo/on",
  "dijit/registry",
  "dojo/query", 
  "dojo/dom-attr",
	"dojo/json",
	"dojo/cookie",
	"dojo/_base/lang",
  "dojo/_base/array",
  "dojo/NodeList-dom"
], 
function (
  declare,
  Evented,
  _WidgetBase, 
  Bookmarks, 
  Layer,
  ArcGISDynamicMapServiceLayer,
  BasemapGallery,
  Basemap,
  on,
  registry,
  query, 
  domAttr,
  JSON, 
  cookie, 
  lang,
  array
){

	var Widget = declare("ngisWidget.bookmarkWidget", [_WidgetBase, Evented], {
    
    clickHandlers: [],
    layerVisibility: [],
    otherSerialisableData_Get: null,
    otherSerialisableData_Set: null,
    storeName: "bisvBookmarkItems",
    
    constructor: function(options, srcRefNode) {
      /// <summary>
      /// Widget constructor.
      /// <param name="options" type="Object">
      ///   Hash of initialisation options. The hash can contain any of the widget's properties, excluding read-only properties.
      /// </param>
      /// <param name="srcRefNode" type="DOMNode|String">
      ///   HTML source node reference.
      /// </param>
      /// </summary>
      this.domNode = srcRefNode; // widget node
      // Properties
      this.set("map", options.map);
      this.set("baseMapGallery", options.baseMapGallery);
      this.set("bookmarks", options.bookmarks);
      this.set("editable", options.editable);
      this.set("useLocalStorage", this._supportsLocalStorage());
      this.set("otherSerialisableData_Get", options.otherSerialisableData_Get);
      this.set("otherSerialisableData_Set", options.otherSerialisableData_Set);
    },
    
    postCreate: function() {
			this.inherited(arguments);
			var bookmarks = this.bookmarks;
      if (this.useLocalStorage) {
        this.bookmarkItems = window.localStorage.getItem(this.storeName);
      } else {
			  this.bookmarkItems = cookie(this.storeName);
      }
			if (this.bookmarkItems === undefined) {
				this.bookmarkItems = [];
			} else {
				this.bookmarkItems = JSON.parse(this.bookmarkItems);
			}
			this.bookmarks = new Bookmarks({
				map: this.map,
				editable: this.editable,
				bookmarks: lang.mixin(this.bookmarkItems, bookmarks)
			}, this.domNode);
      this.own(
			  on(this.bookmarks, 'edit', lang.hitch(this, this._editBookmarkItem)),
			  on(this.bookmarks, 'remove', lang.hitch(this, this._removeBookmarkItem))
      );
      this._manageBookmarkItemListeners();
		},
    
    destroy: function() {
			this.inherited(arguments);
      this.map = null;
      this.bookmarks.destroy();
    },
    
    _clickBookmarkItem: function (index) {
      this._beforeBookmarkRefresh();
      this._setVisibleLayers(this.bookmarkItems[index].layerVisibility);
      this._setVisibleBaseMapID(this.bookmarkItems[index].visibleBasemapID);
      if (this.otherSerialisableData_Set) {
        if (this.bookmarkItems[index].otherData) {
          var otherData = JSON.parse(this.bookmarkItems[index].otherData);
          this.otherSerialisableData_Set(otherData);
        }
      }
      this._afterBookmarkRefresh();
    },
    
		_editBookmarkItem: function(evt) {
      var isAdd = (this.bookmarkItems ? (this.bookmarks.bookmarks.length != this.bookmarkItems.length) : true );
      if (!isAdd) { // Edit
        // Sync names for all this.bookmarks.bookmarks and this.bookmarkItems
        for (var i=0; i<this.bookmarkItems.length; i++) {
          this.bookmarkItems[i]["name"] = this.bookmarks.bookmarks[i]["name"];
        }
      } else {  // Add
        // Sync this.bookmarks.bookmarks and this.bookmarkItems
        var newId = this.bookmarks.bookmarks.length - 1;
        var newItem = this.bookmarks.toJson()[newId];
        if (this.otherSerialisableData_Get) {
          newItem["otherData"] = JSON.stringify(this.otherSerialisableData_Get());
        }
        this._getVisibleLayers();
        newItem["layerVisibility"] = JSON.stringify(this.layerVisibility);
        this._getVisibleBaseMapID();
        newItem["visibleBasemapID"] = JSON.stringify(this.visibleBasemapID);
        if (!this.bookmarkItems) {
          this.bookmarkItems = [];
        }
        this.bookmarkItems.push(newItem);
        this._manageBookmarkItemListeners();
      }
      this.persistBookmarks();
		},
    
    _removeBookmarkItem: function(evt) {
      // Sync this.bookmarks.bookmarks with this.bookmarkItems
      var json = this.bookmarks.toJson();
      var match = false;
      if (this.bookmarkItems) {
        for (var i=0; i<this.bookmarkItems.length; i++) {
          match = false;
          if (json[i]) {
            if ( (this.bookmarkItems[i]["name"] == json[i]["name"]) && (JSON.stringify(this.bookmarkItems[i]["extent"]) == JSON.stringify(json[i]["extent"]) )) {
              match = true;
            }
          }
          if (!match) {
            this.bookmarkItems.splice(i, 1);
            break;
          }
        }
        this.persistBookmarks();
        this._manageBookmarkItemListeners();
      }
    },
    
    _supportsLocalStorage: function() {
      try {
        return ('localStorage' in window && window['localStorage'] !== null);
      } catch(e){
        return false;
      }
    },
    
		persistBookmarks: function() {
      if (this.useLocalStorage) {
        window.localStorage.setItem(this.storeName, this._exportBookmarks());
      } else {
    	  cookie(this.storeName, this._exportBookmarks(), {
				  expires: 365
			  });
      }
    },
    
		_exportBookmarks: function() {
			return JSON.stringify(this.bookmarkItems);
		},
    
    _manageBookmarkItemListeners: function() {
      // Stop listening to all bookmark item click event handlers 
      for (var i=0; i<this.clickHandlers.length; i++) {
        this.clickHandlers[i].remove();
      }
      this.clickHandlers.length = 0; //empty click handler array
      // Add new bookmark items click event handlers
      var bookmarkItemNodes = query(".esriBookmarkLabel"); // get all bookmarkItem dom nodes
      for (i=0; i<bookmarkItemNodes.length; i++) {
        this._setupListener(bookmarkItemNodes[i], i);
      }
      this._addTabbables();
    },

    _addTabbables: function () {
      // Add tabindex accessibility to all clickable bookmark divs
      //var bookmarkItemNodes = query(".esriBookmarkLabel"); // get all esriBookmarkLabel dom nodes
      var i = 8;
      query(".esriBookmarkLabel").forEach(function (node) {
        domAttr.set(node, "tabindex", "0"); // (++i).toString());
        this._setupEnterClickListener(node);
      }, this);
      query(".esriAddBookmark").forEach(function (node) {
        domAttr.set(node, "tabindex", "0"); //  (++i).toString());
        this._setupEnterClickListener(node);
      }, this);
      query(".esriBookmarkEditImage").forEach(function (node) {
        domAttr.set(node, "tabindex", "0"); //  (++i).toString());
        this._setupEnterClickListener(node);
      }, this);
      query(".esriBookmarkRemoveImage").forEach(function (node) {
        domAttr.set(node, "tabindex", "0"); //  (++i).toString());
        this._setupEnterClickListener(node);
      }, this);
    },
    
    _setupEnterClickListener: function (node) {
      // Simulate mouse click when user presses ENTER on a tabbed to (focused) node
      // Note: use keyup event rather than keypress event because bookmark edit fails to hold focus on the resulting text input with keypress
      var eClickHandler = on(node, "keyup", lang.hitch(this, function(evt) {
        if (evt.which === 13) {
          node.click(); // simulate a mouse click
        }
      }));
      this.clickHandlers.push(eClickHandler);
      this.own(eClickHandler); // ensure listeners are cleaned up when widget is destroyed
    },

    _setupListener: function(node, index) {
      var clickHandler = on(node, "click", lang.hitch(this, function() {
        this._clickBookmarkItem(index);
      }));
      this.clickHandlers.push(clickHandler);
      this.own(clickHandler); // ensure listeners are cleaned up when widget is destroyed
    },

    _getVisibleBaseMapID: function () {
      this.visibleBasemapID = "unknown";
      var baseMap = this.baseMapGallery.getSelected();
      this.visibleBasemapID = baseMap.id;
    },

    _getVisibleLayers: function() {
      this.layerVisibility.length = 0;
      this.layerVisibility = [];
      var nonGraphicsLayers = this.map.layerIds;
      // Process non graphics layer map layers
      array.forEach(nonGraphicsLayers, function (id) {
        var layer = this.map.getLayer(id);
        // Only process ArcGISDynamicServiceLayer
        if (layer instanceof ArcGISDynamicMapServiceLayer) {
          var children = [];
          if (layer.layerInfos) {
            children = array.map(layer.layerInfos, function (layerInfo) {
              return { id: layerInfo.id, visible: layerInfo.visible };
            });
          }
          this.layerVisibility.push({ id: id, visible: layer.visible, sub: children });
        }
      }, this);
      // Process graphics layers
      var graphicsLayers = this.map.graphicsLayerIds;
      array.forEach(graphicsLayers, function(id) {
        var layer = this.map.getLayer(id);
        this.layerVisibility.push({id: id, visible: layer.visible, sub: null});
      }, this);
    },
    
    _setVisibleBaseMapID: function(json) {
      var visibleBasemapID = JSON.parse(json);
      if (!visibleBasemapID) {
        return;
      }
      if (!this.baseMapGallery) {
        return;
      }
      if (visibleBasemapID != "unknown") {
        this.baseMapGallery.select(visibleBasemapID);
      }
    },

    _setVisibleLayers: function (json) {
      var layerVisibility = JSON.parse(json);
      array.forEach(layerVisibility, function(item) {
        var layer = this.map.getLayer(item.id);
        if (layer) {
          layer.setVisibility(item.visible);
          if (item.sub) {
            var visibleSubLayerIDs = [];
            array.forEach(item.sub, function(subItem) {
              if (subItem.visible) {
                visibleSubLayerIDs.push(subItem.id);
              }
            });
            // << KC BUG FIX
            //    ... ISSUE: If there are any group layers in visibleSubLayerIDs - ArcGIS will automatically set ALL sub layers of a group visible in the next line of code regardless if sub layer is in visibleSubLayerIDs or not.
            layer.setVisibleLayers(visibleSubLayerIDs); // set visibility for group layers
            //    ==> WORKAROUND: now that the required group layers are visible and ALL sub layers of those group layers, call setVisibleLayers again with only the sub layers. This sets the sub layer visibility as expectd.
            var subInfos = layer.layerInfos;
            var visibleChildLayerIDs = [];
            if (subInfos !== null) {
              for (var i = 0; i < subInfos.length; i++) {
                if (visibleSubLayerIDs.indexOf(subInfos[i].id) > -1) { // should it be vis
                  if (subInfos[i].subLayerIds == null) { // is it a child
                    visibleChildLayerIDs.push(subInfos[i].id);
                  }
                }
              }
            }
            layer.setVisibleLayers(visibleChildLayerIDs); // set visibility for bottom level sub layers only
            // >> KC BUG FIX
          }
        }
      }, this);
    },

    _beforeBookmarkRefresh: function() {
      this.emit("beforeBookmarkRefresh", {});
    },

    _afterBookmarkRefresh: function() {
      this.emit("afterBookmarkRefresh", {});
    }

    //_clearAllTocCheckboxes: function () {
    //  var tocCheckboxNodes = query(".tocContentPanel input[type=checkbox]"); // get all bookmarkItem dom nodes
    //  for (var i=0; i<tocCheckboxNodes.length; i++) {
    //    var widget = registry.getEnclosingWidget(tocCheckboxNodes[i]);
    //    widget.set('checked', false);
    //    console.log(widget.get('checked'));
    //  }
    //},
    //_reportAllTocCheckboxes: function () {
    //  var tocCheckboxNodes = query(".tocContentPanel input[type=checkbox]"); // get all bookmarkItem dom nodes
    //  for (var i=0; i<tocCheckboxNodes.length; i++) {
    //    var widget = registry.getEnclosingWidget(tocCheckboxNodes[i]);
    //    console.log(widget.get('checked'));
    //  }
	  //}

	});
  
  return Widget;

});