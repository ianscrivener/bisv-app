/*
 | Author: NGIS 2014 (www.ngis.com.au) -> Kristy Carter
 | Module: MapNavigationWidget
 | Description: Map Navigation Widget - user controls for map navigation.
 |   Provides the following user interface elements...
 |    Home button - Return the map to the initial/default extent.
 |    Previous button - Move back one position in map extent history and display the extent in the map.
 |    Next button - Move forward one position in map extent history and display the extent in the map.
 |    Map busy animation overlay - Map busy animation displayed whilst map is refreshing.
 | Notes: This module is a derivative work based on ESRI HomeButton widget.
 | Usage: See readme.
 |
 | CHANGE LOG:
 |   2015-03-09 Kristy Carter: Initial code release.
 */
 
define([
  "dojo/Evented",
  "dojo/_base/declare",
  "dojo/_base/lang",
  "dojo/has", // feature detection
  "dijit/_WidgetBase",
  "dijit/a11yclick", // Custom press, release, and click synthetic events which trigger on a left mouse click, touch, or space/enter keyup.
  "dijit/_TemplatedMixin",
  "dojo/on",
  "dojo/Deferred",
  "esri/toolbars/navigation",
	"dojo/text!./templates/mapNavigationWidget.html", // template html
  "dojo/i18n!./nls/jsapi", // localization
  "dojo/dom-class",
  "dojo/dom-style"
],
function (
  Evented,
  declare,
  lang,
  has,
  _WidgetBase,
  a11yclick,
  _TemplatedMixin,
  on,
  Deferred,
  Navigation,
  dijitTemplate,
  i18n,
  domClass,
  domStyle
) {
  var Widget = declare("ngisWidget.MapNavigationWidget", [_WidgetBase, _TemplatedMixin, Evented], {
    templateString: dijitTemplate, // template HTML
    
    // default options
    options: {
      theme: "mapNavigation",
      map: null,
      extent: null,
      fit: false,
      visible: true
    },
    
    constructor: function(options, srcRefNode) {
      /// <summary>
      /// Widget constructor.
      /// <param name="options" type="Object">
      ///   Hash of initialisation options. The hash can contain any of the widget's properties, excluding read-only properties.
      /// </param>
      /// <param name="srcRefNode" type="DOMNode|String">
      ///   HTML source node reference.
      /// </param>
      /// </summary>
      var defaults = lang.mixin({}, this.options, options); // mix in param options with default
      this.domNode = srcRefNode; // widget node
      this._i18n = i18n; // store localized strings
      // Properties
      this.set("loaded", false);
      this.set("map", defaults.map);
      this.set("theme", defaults.theme);
      this.set("visible", defaults.visible);
      this.set("extent", defaults.extent);
      this.set("fit", defaults.fit);
      // Listeners
      this.watch("theme", this._updateThemeWatch);
      this.watch("visible", this._visible);
      // CSS classes
      this._css = {
        container: "mapNavigationContainer",
        home: "home",
        previous: "previous",
        next: "next",
        loading: "loading",
        disabled: "disabled", 
        mapBusyOverlay: "mapBusyOverlay",
        hidden: "hidden"
      };
    },
    
    postCreate: function() {
      /// <summary>
      /// Widget initialisations: a11yclick (accessibility) listener for buttons.
      /// </summary>
      this.inherited(arguments);
      // Ensure that event handlers are unregistered when the widget is destroyed
      this.own(
        on(this._homeNode, a11yclick, lang.hitch(this, this.home)),
        on(this._previousNode, a11yclick, lang.hitch(this, this.previous)),
        on(this._nextNode, a11yclick, lang.hitch(this, this.next))
      );
    },
    
    startup: function() {    
      /// <summary>
      /// Start widget - called by user.
      /// </summary>
      // Ensure map was specified
      if (!this.map) {
        this.destroy();
        console.log('MapNavigationWidget::map required before startup!');
      }
      // Ensure map is loaded before initialising.
      if (this.map.loaded) {
        this._init();
      } else {
        on.once(this.map, "load", lang.hitch(this, function() {
          this._init();
        }));
      }
    },
    
    // connections/subscriptions will be cleaned up during the destroy() lifecycle phase
    destroy: function() {
      /// <summary>
      /// Connections/subscriptions will be cleaned up during the destroy() lifecycle phase.
      /// </summary>
      this.inherited(arguments);
    },
    
    /* ---------------- */
    /*   Public Events  */
    /* ---------------- */
    // home
    // load
    
    /* ---------------- */
    /* Public Functions */
    /* ---------------- */
    
    home: function() {
      /// <summary>
      /// Show home extent in the map.
      /// <returns type="dojo/promise/Promise">Asynchronous communication.</returns>
      /// </summary>
      var def = new Deferred(); // deferred to return  
      var defaultExtent = this.get("extent"); // get extent property
      this._showLoading(this._homeNode); // show loading spinner         
      // Event object
      var homeEvt = {
        extent: defaultExtent
      };
      if (defaultExtent) {
        // Extent is not the same as current extent
        if (this.map.extent !== defaultExtent) {
          // Set map extent
          this.map.setExtent(defaultExtent, this.get("fit")).then(lang.hitch(this, function() {
            this._hideLoading(this._homeNode); // hide loading spinner
            // Home event
            this.emit("home", homeEvt);
            def.resolve(homeEvt);
          }), lang.hitch(this, function(error) {
            if (!error) {
              error = new Error("HomeButton::Error setting map extent");
            }
            homeEvt.error = error;
            // Home event
            this.emit("home", homeEvt);
            def.reject(error);
          }));
        } else {
          // Same extent
          this._hideLoading(this._homeNode); // hide loading spinner
          // Home event
          this.emit("home", homeEvt);
          def.resolve(homeEvt);
        }
      } else {
        this._hideLoading(this._homeNode); // hide loading spinner
        var error = new Error("HomeButton::Home extent is undefined");
        // Home event
        homeEvt.error = error;
        this.emit("home", homeEvt);
        def.reject(error);
      }
      return def.promise;
    },
    
    previous: function() {
      /// <summary>
      /// Move back one position in map extent history and display the extent in the map.
      /// </summary>
      this._showLoading(this._previousNode); // show loading animation
      // Set map extent
      this.navToolbar.zoomToPrevExtent();
      // Emit previous event
      this.emit("previous");
    },
    
    next: function() {
      /// <summary>
      /// Move forward one position in map extent history and display the extent in the map.
      /// </summary>
      this._showLoading(this._nextNode); // show loading animation
      // Set map extent
      this.navToolbar.zoomToNextExtent();
      // Emit previous event
      this.emit("next");
    },
    
    show: function() {
      /// <summary>
      /// Show widget
      /// </summary>
      this.set("visible", true);
    },
    
    hide: function() {
      /// <summary>
      /// Hide widget
      /// </summary>
      this.set("visible", false);
    },
    
    /* ----------------- */
    /* Private Functions */
    /* ----------------- */
    
    _init: function() {
      /// <summary>
      /// Perform map based widget initialisations.
      /// </summary>
      // Show or hide widget
      this._visible();
      // if no extent set, set extent to map extent
      if (!this.get("extent")) {
        this.set("extent", this.map.extent);   
      }
      // Previous and Next Extent tools
      this.set("navToolbar", new Navigation(this.map));
      this.navToolbar.on("extent-history-change", lang.hitch(this, function() {
        this._extentHistoryChanged();
      }));
      this._extentHistoryChanged();
      // Wireup map busy overlay to map refresh events
      this.own(
        on(this.map, "update-start", lang.hitch(this, this._showMapBusy)),
        on(this.map, "update-end", lang.hitch(this, this._hideMapBusy))
      );
      // Widget is now loaded - emit the loaded event
      this.set("loaded", true);
      this.emit("load", {});
    },
    
    _showMapBusy: function() {
      /// <summary>
      /// Show the map busy overlay animation.
      /// </summary>
      domClass.remove(this._mapBusyNode, this._css.hidden);
    },
    
    _hideMapBusy: function() {      
      /// <summary>
      /// Hide the map busy overlay animation.
      /// </summary>
      domClass.add(this._mapBusyNode, this._css.hidden);
    },
    
    _extentHistoryChanged: function() {
      /// <summary>
      /// Event handler for map navigation toolbar extent changed. Enables or disables the previous/next buttons.
      /// </summary>
      if (this.navToolbar.isFirstExtent()) {
        this._disableButton(this._previousNode);
      } else {
        this._enableButton(this._previousNode);
      }
      if (this.navToolbar.isLastExtent()) {
        this._disableButton(this._nextNode);
      } else {
        this._enableButton(this._nextNode);
      }
      this._hideLoading(this._previousNode);
      this._hideLoading(this._nextNode);
    },
    
    _disableButton: function(node) {
      /// <summary>
      /// Disable the button
      /// <param name="node" type="Object|String">
      ///   A DOM node or its node id (as a string).
      /// </param>
      /// </summary>
      domClass.add(node, this._css.disabled);
    },
    
    _enableButton: function(node) {
      /// <summary>
      /// Disable the button
      /// <param name="node" type="Object|String">
      ///   A DOM node or its node id (as a string).
      /// </param>
      /// </summary>
      domClass.remove(node, this._css.disabled);
    },
    
    _showLoading: function(node) {
      /// <summary>
      /// Show loading animation on button
      /// <param name="node" type="Object|String">
      ///   A DOM node or its node id (as a string).
      /// </param>
      /// </summary>
      domClass.add(node, this._css.loading);
    },
    
    _hideLoading: function(node) {
      /// <summary>
      /// Hide loading animation on button
      /// <param name="node" type="Object|String">
      ///   A DOM node or its node id (as a string).
      /// </param>
      /// </summary>
      domClass.remove(node, this._css.loading);
    },
    
    _updateThemeWatch: function(attr, oldVal, newVal) {
      /// <summary>
      /// There changed - ensure classes correctly managed.
      /// </summary>
      domClass.remove(this.domNode, oldVal);
      domClass.add(this.domNode, newVal);
    },
    
    _visible: function() {
      /// <summary>
      /// Show or hide widget.
      /// </summary>
      if(this.get("visible")){
        domStyle.set(this.domNode, 'display', 'block');
      }
      else{
        domStyle.set(this.domNode, 'display', 'none');
      }
    }
  });
  
  return Widget;
    
});