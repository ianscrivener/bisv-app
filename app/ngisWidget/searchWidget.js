/*
 | Author: NGIS 2015 (www.ngis.com.au) -> Kristy Carter
 | Module: SearchWidget
 | Description: Search Widget - user interface controls for searching for features in the map and displaying the resulting selected features on the map.
 | Usage: See readme.
 |
 | CHANGE LOG:
 |   2015-05-14 Kristy Carter: Initial code release.
 */
 
define([
	"dojo/_base/declare", "dojo/Evented", "dojo/dom", "dojo/_base/lang", "dojo/_base/array", "dojo/html",
  "dijit/_WidgetBase", "dijit/_TemplatedMixin", "dijit/_WidgetsInTemplateMixin", 
  "dojo/text!./templates/searchWidget.html", "dojo/dom-class", "dojo/on", "dojo/store/Memory",
  "esri/map", "esri/toolbars/draw", "esri/tasks/query", "esri/tasks/QueryTask", 
  "esri/graphic", "esri/layers/GraphicsLayer", "esri/geometry/Extent", "esri/geometry/Multipoint",
  "esri/Color", "esri/symbols/SimpleMarkerSymbol", "esri/symbols/SimpleLineSymbol", "esri/symbols/SimpleFillSymbol", 
  "dijit/form/FilteringSelect", "dijit/form/TextBox", "dijit/form/Button", "dijit/layout/ContentPane", 
  "dijit/layout/AccordionContainer", "dojox/widget/Standby"
],
function (declare, Evented, dom, lang, arrayUtils, html,
  _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin,
  template, domClass, on, Memory,
  Map, Draw, Query, QueryTask, 
  Graphic, GraphicsLayer, Extent, Multipoint,
  Color, SimpleMarkerSymbol, SimpleLineSymbol, SimpleFillSymbol
) {
  var Widget = declare("ngisWidget.SearchWidget", [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, Evented], {
    templateString: template,
    baseClass: "SearchWidget",

    // Custom setters
    noSelection: true,
    _setNoSelectionAttr: function (/*Boolean*/ value) { // attach to DOM node style attribute
      this._set("noSelection", value);
      this.clearSelectedNode.set("disabled", value);
      this.zoomToNode.set("disabled", value);
    },

    // Widget option defaults (static)
    options: {
      map: null
    },
    
    constructor: function(options, srcRefNode) {
      /// <summary>
      /// Widget constructor.
      /// <param name="options" type="Object">
      ///   Hash of initialisation options. The hash can contain any of the widget's properties, excluding read-only properties.
      /// </param>
      /// <param name="srcRefNode" type="DOMNode|String">
      ///   HTML source node reference.
      /// </param>
      /// </summary>
      this.domNode = srcRefNode; // widget node
      var defaults = lang.mixin({}, this.options, options); // mix in param options with defaults
      // Properties
      this.set("loaded", false);
      this.set("map", defaults.map);
      this.set("graphicsLayer", defaults.graphicsLayer);
      this.set("currentSelectionLayerURL", "");
      this.set("searchConfig", defaults.searchConfig);
    },
    
    postCreate: function() {
      /// <summary>
      /// General widget initialisations.
      /// </summary>
      this.inherited(arguments);
      this.busyNode.set("target", this.domNode); // dom.byId(this.baseClass + "_container"));
      this.own(
        on(this.clearSelectedNode, "click", lang.hitch(this, this._clearSelectedClick)),
        on(this.searchNode, "click", lang.hitch(this, this._searchClick)),
        on(this.zoomToNode, "click", lang.hitch(this, this._showSelectionClick))
      );
    },
    
    startup: function() {
      /// <summary>
      /// Start widget - called by user.
      /// </summary>
      this.inherited(arguments);
      // Ensure map was specified
      if (!this.map) {
        this.destroy();
        console.log(this.baseClass+'::map required before startup!');
      }
      // Ensure map is loaded before initialising
      if (this.map.loaded) {
        this._init();
      } else {
        on.once(this.map, "load", lang.hitch(this, function() {
          this._init();
        }));
      }
    },
    
    destroy: function() {
      /// <summary>
      /// Connections/subscriptions will be cleaned up during the destroy() lifecycle phase.
      /// </summary>
      this.inherited(arguments);
      this.map = null;
    },
    
    /* ---------------- */
    /* Public Functions */
    /* ---------------- */

    clear: function() {
      this._clearSelectedClick();
    },

    getSearchCriteria: function() {
      /// <summary>
      /// Return the current search criteria.
      /// </summary>
      var type = "";
      var criteria = [];
      switch (this.searchAccordionNode.selectedChildWidget) {
        case this.lotSearchNode:
          type = "lot";
          criteria = [this.lotNode.get("value"), this.planNode.get("value"), this.sectionNode.get("value")];
          break;
        case this.lgaSearchNode:
          type = "lga";
          criteria = [this.lgaNode.get("value")];
          break;
        case this.postcodeSearchNode:
          type = "postcode";
          criteria = [this.postcodeNode.get("value"), this.suburbNode.get("value")];
          break;
        case this.ibraSearchNode:
          type = "ibra";
          criteria = [this.regionNode.get("value"), this.subregionNode.get("value")];
        case this.cmaSearchNode:
          type = "cma";
          criteria = [this.cmaNode.get("value"), this.subcmaNode.get("value")];
        case this.llsSearchNode:
          type = "lls";
          criteria = [this.llsNode.get("value")];
        case this.mitchellSearchNode:
          type = "mitchell";
          criteria = [this.mitchellNode.get("value")];
      }
      return {type: type, criteria: criteria}; 
    },
    
    setSearchCriteria: function(searchCriteria) {
      /// <summary>
      /// Load the given search criteria into the search widget.
      /// </summary>
      this._clearSelectedClick();
      switch (searchCriteria.type) {
        case "lot":
          this.lotNode.set("value", searchCriteria.criteria[0]);
          this.planNode.set("value", searchCriteria.criteria[1]);
          this.sectionNode.set("value", searchCriteria.criteria[2]);
          this.searchAccordionNode.selectChild(this.lotSearchNode);
          break;
        case "lga":
          this.lgaNode.set("value", searchCriteria.criteria[0]);
          this.searchAccordionNode.selectChild(this.lgaSearchNode);
          break;
        case "postcode":
          this.postcodeNode.set("value", searchCriteria.criteria[0]);
          this.suburbNode.set("value", searchCriteria.criteria[1]);
          this.searchAccordionNode.selectChild(this.postcodeSearchNode);
          break;
        case "ibra":
          this.regionNode.set("value", searchCriteria.criteria[0]);
          this.subregionNode.set("value", searchCriteria.criteria[1]);
          this.searchAccordionNode.selectChild(this.ibraSearchNode);
          break;
        case "cma":
          this.cmaNode.set("value", searchCriteria.criteria[0]);
          this.subcmaNode.set("value", searchCriteria.criteria[1]);
          this.searchAccordionNode.selectChild(this.cmaSearchNode);
          break;
        case "lls":
          this.llsNode.set("value", searchCriteria.criteria[0]);
          this.searchAccordionNode.selectChild(this.llsSearchNode);
          break;
        case "mitchell":
          this.mitchellNode.set("value", searchCriteria.criteria[0]);
          this.searchAccordionNode.selectChild(this.mitchellSearchNode);
      }
    },    
    
    /* ----------------- */
    /* Private Functions */
    /* ----------------- */
    
    _init: function() {
      /// <summary>
      /// Map dependant widget initialisations.
      /// </summary>
      // Load drop down lists
      this.busyNode.show();
      this._loadLGA();
      this._loadPostcode();
      this._loadRegion();
      this._loadCMA();
      this._loadLLS();
      this._loadMitchell();
      this.own(
        on(this.postcodeNode, "change", lang.hitch(this, function (value) { this._loadSuburb(value); })),
        on(this.regionNode, "change", lang.hitch(this, function (value) { this._loadSubRegion(value); })),
        on(this.cmaNode, "change", lang.hitch(this, function (value) { this._loadSubCMA(value); }))
      );
      this.busyNode.hide();
      // Widget is now loaded - emit the loaded event
      this.set("loaded", true);
      this.emit("load", {});
    },
    
    _searchClick: function(evt) {
      /// <summary>
      /// Perform search.
      /// </summary>
      this.busyNode.show();
      this._beforeSelect();
      var url;
      var qry = new Query();
      qry.returnGeometry = true;
      switch (this.searchAccordionNode.selectedChildWidget) {
        case this.lotSearchNode:
          url = this.searchConfig.LOT_SEARCH.MAPSERVICE_LAYER_URL;
          var value = "";
          var subvalue = "";
          var lotNum = this.lotNode.get("value");
          var plan = this.planNode.get("value");
          var section = this.sectionNode.get("value");
          if (section !== "") {
            qry.where = this.searchConfig.LOT_SEARCH.FIELD_LOT + " = '" + lotNum + "' AND " +
                        this.searchConfig.LOT_SEARCH.FIELD_PLAN + " = '" + plan + "' AND " +
                        this.searchConfig.LOT_SEARCH.FIELD_SECTION + " = '" + section + "'";
          } else {
            qry.where = this.searchConfig.LOT_SEARCH.FIELD_LOT + " = '" + lotNum + "' AND " +
                        this.searchConfig.LOT_SEARCH.FIELD_PLAN + " = '" + plan + "' AND " +
                        this.searchConfig.LOT_SEARCH.FIELD_SECTION + " is null";
          }
          qry.outFields = [this.searchConfig.LOT_SEARCH.OBJECTID];
          break;
        case this.postcodeSearchNode:
          url = this.searchConfig.POSTCODE_SEARCH.MAPSERVICE_LAYER_URL;
          var postcode = this.postcodeNode.get("value");
          var suburb = this.suburbNode.get("value");
          if (suburb == "ALL") {
            if (postcode == "") {
              //qry.where = this.searchConfig.POSTCODE_SEARCH.FIELD_SUBURB + " <> ''";
              this.busyNode.hide();
              return;
            } else {
              qry.where = this.searchConfig.POSTCODE_SEARCH.FIELD_POSTCODE + " = '" + postcode + "'";
            }
          } else {
            if (postcode == "") {
              qry.where = this.searchConfig.POSTCODE_SEARCH.FIELD_SUBURB + " = '" + suburb + "'";
            } else {
              qry.where = this.searchConfig.POSTCODE_SEARCH.FIELD_POSTCODE + " = '" + postcode + "' AND " +
                          this.searchConfig.POSTCODE_SEARCH.FIELD_SUBURB + " = '" + suburb + "'";
            }
          }
          qry.outFields = [this.searchConfig.POSTCODE_SEARCH.OBJECTID];
          break;
        case this.lgaSearchNode:
          url = this.searchConfig.LGA_SEARCH.MAPSERVICE_LAYER_URL;
          value = this.lgaNode.get("value");
          qry.where = this.searchConfig.LGA_SEARCH.FIELD_LGA + " = '" + value + "'";
          qry.outFields = [this.searchConfig.LGA_SEARCH.OBJECTID];
          break;
        case this.ibraSearchNode:
          url = this.searchConfig.IBRA_SEARCH.MAPSERVICE_LAYER_URL;
          value = this.regionNode.get("value");
          subvalue = this.subregionNode.get("value");
          if (subvalue == "ALL") {
            qry.where = this.searchConfig.IBRA_SEARCH.FIELD_REGION + " = '" + value + "'";
          } else {
            qry.where = this.searchConfig.IBRA_SEARCH.FIELD_REGION + " = '" + value + "' AND " +
                        this.searchConfig.IBRA_SEARCH.FIELD_SUBREGION + " = '" + subvalue + "'";
          }
          qry.outFields = [this.searchConfig.IBRA_SEARCH.OBJECTID];
          break;
        case this.cmaSearchNode:
          url = this.searchConfig.CMA_SEARCH.MAPSERVICE_LAYER_URL;
          value = this.cmaNode.get("value");
          subvalue = this.subcmaNode.get("value");
          if (subvalue == "ALL") {
            qry.where = this.searchConfig.CMA_SEARCH.FIELD_CMA + " = '" + value + "'";
          } else {
            qry.where = this.searchConfig.CMA_SEARCH.FIELD_CMA + " = '" + value + "' AND " +
                        this.searchConfig.CMA_SEARCH.FIELD_SUBCMA + " = '" + subvalue + "'";
          }
          qry.outFields = [this.searchConfig.CMA_SEARCH.OBJECTID];
          break;
        case this.llsSearchNode:
          url = this.searchConfig.LLS_SEARCH.MAPSERVICE_LAYER_URL;
          value = this.llsNode.get("value");
          qry.where = this.searchConfig.LLS_SEARCH.FIELD_LLS + " = '" + value + "'";
          qry.outFields = [this.searchConfig.LLS_SEARCH.OBJECTID];
          break;
        case this.mitchellSearchNode:
          url = this.searchConfig.MITCHELL_SEARCH.MAPSERVICE_LAYER_URL;
          value = this.mitchellNode.get("value");
          qry.where = this.searchConfig.MITCHELL_SEARCH.FIELD_MITCHELL + " = '" + value + "'";
          qry.outFields = [this.searchConfig.MITCHELL_SEARCH.OBJECTID];
      }
      var qryTask = new QueryTask(url);
      this.currentSelectionLayerURL = "";
      qryTask.execute(qry, lang.hitch(this, lang.hitch(this, function (featureSet) {
        this._processQueryResults(featureSet, url);
      })));
    },

    _beforeSelect: function () {
      // Emit the before_select event
      this.emit("before_select", {});
    },

    _selectionChanged: function() {
      // Emit the selection_change event
      this.emit("selection_change", { selectedGraphics: this.graphicsLayer.graphics });
      this.set("noSelection", !(this.graphicsLayer.graphics.length > 0));
    },

    _processQueryResults: function(featureSet, url) {
      this.graphicsLayer.clear(); // clear existing selection on graphics layer
      var resultFeatures = featureSet.features;
      var fullExtent = null;
      var multiPoint = new Multipoint(this.map.spatialReference);
      for (var i=0; i<resultFeatures.length; i++) {
        var extent;
        var point;
        var graphic = resultFeatures[i];
        switch (graphic.geometry.type) {
          case "point":
            point = graphic.geometry;
            graphic.setSymbol(this._getSelectionPointSymbol());
            break;
          case "multipoint":
            extent = graphic.geometry.getExtent();
            graphic.setSymbol(this._getSelectionPointSymbol());
            break;
          case "polyline":
            extent = graphic.geometry.getExtent();
            graphic.setSymbol(this._getSelectionPolylineSymbol());
            break;
          case "polygon":
            extent = graphic.geometry.getExtent();
            graphic.setSymbol(this._getSelectionPolygonSymbol());
            break;
        }
        if (point) {
          multiPoint.addPoint(point);
          fullExtent = multiPoint.getExtent();
        } else {
          if (fullExtent) {
            fullExtent = fullExtent.union(extent);
          } else {
            fullExtent = extent;
          }
        }
        // Add graphic to the map graphics layer.
        graphic["sourceUrl"] = url; // add sourcUrl property to graphic
        this.graphicsLayer.add(graphic);
        this.currentSelectionLayerURL = url;
      }
      this.set("selectedCount", resultFeatures.length);
      // Zoom to selected
      this.set("fullExtent", fullExtent);
      if (fullExtent) {
        this.map.setExtent(fullExtent);
      }
      this._showResults(false);
      this._selectionChanged();
      this.busyNode.hide();
    },
    
    _showSelectionClick: function (evt) {
      /// <summary>
      /// Zoom to the selected features.
      /// </summary>
      if (this.fullExtent) {
        this.map.setExtent(this.fullExtent);
      }
    },

    _clearSelectedClick: function(evt) {
      /// <summary>
      /// Clear all selection graphics from graphics layer
      /// </summary>
      this.graphicsLayer.clear();
      this.currentSelectionLayerURL = "";
      this._showResults(true);
      this._selectionChanged();
    },
    
    _showResults: function(cleared) {
      var resultText = "";
      if (!cleared) {
        resultText += "<i><small>&#40;Selected feature count: " + this.selectedCount + "&#41;</small></i>";
      }
      html.set(this.resultsNode, resultText);
    },

    _loadSuburb: function(selectedPostcode) {
      /// <summary>
      /// Populate the Suburb filterSelect drop down with unique values.
      /// </summary>
      var outFieldname = this.searchConfig.POSTCODE_SEARCH.FIELD_SUBURB;
      //var selectedPostcode = this.postcodeNode.get('value');
      queryTask = new QueryTask(this.searchConfig.POSTCODE_SEARCH.MAPSERVICE_LAYER_URL);
      query = new Query();
      query.returnGeometry = false;
      query.outFields = [outFieldname];
      query.orderByFields = [outFieldname];
      if (selectedPostcode == "") {
        query.where = this.searchConfig.POSTCODE_SEARCH.FIELD_POSTCODE + " <> ''";
      } else {
        query.where = this.searchConfig.POSTCODE_SEARCH.FIELD_POSTCODE + " = " + selectedPostcode;
      }
      queryTask.execute(query, lang.hitch(this, function (results) {
        var val;
        var values = [];
        var testVals = {};
        if (selectedPostcode != "") {
          values.push({ name: "ALL" })  //add "ALL" option to drop down list
        }
        // Get unique values from results
        var features = results.features;
        arrayUtils.forEach(features, lang.hitch(this, function (feature) {
          val = feature.attributes[outFieldname];
          if (!testVals[val]) {
            testVals[val] = true;
            values.push({ name: val });
          }
        }));
        // Create memory data store for filterSelect data source
        var dataItems = {
          identifier: 'name',
          label: 'name',
          items: values
        };
        var suburbStore = new Memory({ data: dataItems });
        this.suburbNode.store = suburbStore;
        this.suburbNode.set('value', values[0].name); //select the first item
      }));
    },

    _loadPostcode: function() {
      /// <summary>
      /// Populate the Postcode filterSelect drop down with unique values.
      /// </summary>
      var outFieldname = this.searchConfig.POSTCODE_SEARCH.FIELD_POSTCODE;
      queryTask = new QueryTask(this.searchConfig.POSTCODE_SEARCH.MAPSERVICE_LAYER_URL);
      query = new Query();
      query.returnGeometry = false;
      query.outFields = [outFieldname];
      query.orderByFields = [outFieldname];
      query.where = outFieldname + " IS NOT NULL";
      queryTask.execute(query, lang.hitch(this, function (results) {
        var val;
        var values = [];
        var testVals = {};
        values.push({ name: "" })  //add "ALL" option to drop down list
        // Get unique values from results
        var features = results.features;
        arrayUtils.forEach (features, lang.hitch(this, function(feature) {
          val = feature.attributes[outFieldname].toString();
          if (!testVals[val]) {
            testVals[val] = true;
            values.push({ name: val });
          }
        }));
        // Create memory data store for filterSelect data source
        var dataItems = {
          identifier: 'name',
          label: 'name',
          items: values
        };
        var postcodeStore = new Memory({ data: dataItems });
        this.postcodeNode.store = postcodeStore;
        this.postcodeNode.set('value', values[0].name); //select the first item
        this._loadSuburb(values[0].name);
      }));
    },

    _loadLGA: function() {
      /// <summary> 
      /// Populate the LGA filterSelect drop down with unique values.
      /// </summary>
      var outFieldname = this.searchConfig.LGA_SEARCH.FIELD_LGA;
      queryTask = new QueryTask(this.searchConfig.LGA_SEARCH.MAPSERVICE_LAYER_URL);
      query = new Query();
      query.returnGeometry = false;
      query.outFields = [outFieldname];
      query.orderByFields = [outFieldname];
      query.where = outFieldname + " <> ''";
      queryTask.execute(query, lang.hitch(this, function (results) {
        var val;
        var values = [];
        var testVals={};
        // Get unique values from results
        var features = results.features;
        arrayUtils.forEach (features, lang.hitch(this, function(feature) {
          val = feature.attributes[outFieldname];
          if (!testVals[val]) {
            testVals[val] = true;
            values.push({ name: val });
          }
        }));
        // Create memory data store for filterSelect data source
        var dataItems = {
          identifier: 'name',
          label: 'name',
          items: values
        };
        var lgaStore = new Memory({data: dataItems});
        this.lgaNode.store = lgaStore;
        this.lgaNode.set('value', values[0].name); //select the first item
      }));
    },

    _loadRegion: function() {
      /// <summary>
      /// Populate the Bioregion filterSelect drop down with unique values.
      /// </summary>
      var outFieldname = this.searchConfig.IBRA_SEARCH.FIELD_REGION;
      queryTask = new QueryTask(this.searchConfig.IBRA_SEARCH.MAPSERVICE_LAYER_URL);
      query = new Query();
      query.returnGeometry = false;
      query.outFields = [outFieldname];
      query.orderByFields = [outFieldname];
      query.where = outFieldname + " <> ''";
      queryTask.execute(query, lang.hitch(this, function (results) {
        var val;
        var values = [];
        var testVals = {};
        // Get unique values from results
        var features = results.features;
        arrayUtils.forEach (features, lang.hitch(this, function(feature) {
          val = feature.attributes[outFieldname].toString();
          if (!testVals[val]) {
            testVals[val] = true;
            values.push({ name: val });
          }
        }));
        // Create memory data store for filterSelect data source
        var dataItems = {
          identifier: 'name',
          label: 'name',
          items: values
        };
        var regionStore = new Memory({ data: dataItems });
        this.regionNode.store = regionStore;
        this.regionNode.set('value', values[0].name); //select the first item
      }));
    },

    _loadSubRegion: function (selectedRegion) {
      /// <summary>
      /// Populate the Sub-bioregion filterSelect drop down with unique values.
      /// </summary>
      var outFieldname = this.searchConfig.IBRA_SEARCH.FIELD_SUBREGION;
      //var selectedPostcode = this.postcodeNode.get('value');
      queryTask = new QueryTask(this.searchConfig.IBRA_SEARCH.MAPSERVICE_LAYER_URL);
      query = new Query();
      query.returnGeometry = false;
      query.outFields = [outFieldname];
      query.orderByFields = [outFieldname];
      query.where = this.searchConfig.IBRA_SEARCH.FIELD_REGION + " = '" + selectedRegion + "'";
      queryTask.execute(query, lang.hitch(this, function (results) {
        var val;
        var values = [];
        var testVals = {};
        values.push({ name: "ALL" })  //add "ALL" option to drop down list
        // Get unique values from results
        var features = results.features;
        arrayUtils.forEach(features, lang.hitch(this, function (feature) {
          val = feature.attributes[outFieldname];
          if (!testVals[val]) {
            testVals[val] = true;
            values.push({ name: val });
          }
        }));
        // Create memory data store for filterSelect data source
        var dataItems = {
          identifier: 'name',
          label: 'name',
          items: values
        };
        var subregionStore = new Memory({ data: dataItems });
        this.subregionNode.store = subregionStore;
        this.subregionNode.set('value', values[0].name); //select the first item
      }));
    },

    _loadSubCMA: function (selectedCMA) {
      /// <summary>
      /// Populate the Sub-CMA filterSelect drop down with unique values.
      /// </summary>
      var outFieldname = this.searchConfig.CMA_SEARCH.FIELD_SUBCMA;
      //var selectedPostcode = this.postcodeNode.get('value');
      queryTask = new QueryTask(this.searchConfig.CMA_SEARCH.MAPSERVICE_LAYER_URL);
      query = new Query();
      query.returnGeometry = false;
      query.outFields = [outFieldname];
      query.orderByFields = [outFieldname];
      query.where = this.searchConfig.CMA_SEARCH.FIELD_CMA + " = '" + selectedCMA + "'";
      queryTask.execute(query, lang.hitch(this, function (results) {
        var val;
        var values = [];
        var testVals = {};
        values.push({ name: "ALL" })  //add "ALL" option to drop down list
        // Get unique values from results
        var features = results.features;
        arrayUtils.forEach(features, lang.hitch(this, function (feature) {
          val = feature.attributes[outFieldname];
          if (!testVals[val]) {
            testVals[val] = true;
            values.push({ name: val });
          }
        }));
        // Create memory data store for filterSelect data source
        var dataItems = {
          identifier: 'name',
          label: 'name',
          items: values
        };
        var subcmaStore = new Memory({ data: dataItems });
        this.subcmaNode.store = subcmaStore;
        this.subcmaNode.set('value', values[0].name); //select the first item
      }));
    },
    
    _loadCMA: function () {
      /// <summary>
      /// Populate the CMA filterSelect drop down with unique values.
      /// </summary>
      var outFieldname = this.searchConfig.CMA_SEARCH.FIELD_CMA;
      queryTask = new QueryTask(this.searchConfig.CMA_SEARCH.MAPSERVICE_LAYER_URL);
      query = new Query();
      query.returnGeometry = false;
      query.outFields = [outFieldname];
      query.orderByFields = [outFieldname];
      query.where = outFieldname + " <> ''";
      queryTask.execute(query, lang.hitch(this, function (results) {
        var val;
        var values = [];
        var testVals = {};
        // Get unique values from results
        var features = results.features;
        arrayUtils.forEach(features, lang.hitch(this, function (feature) {
          val = feature.attributes[outFieldname].toString();
          if (!testVals[val]) {
            testVals[val] = true;
            values.push({ name: val });
          }
        }));
        // Create memory data store for filterSelect data source
        var dataItems = {
          identifier: 'name',
          label: 'name',
          items: values
        };
        var cmaStore = new Memory({ data: dataItems });
        this.cmaNode.store = cmaStore;
        this.cmaNode.set('value', values[0].name); //select the first item
      }));
    },

    _loadLLS: function () {
      /// <summary> 
      /// Populate the LLS filterSelect drop down with unique values.
      /// </summary>
      var outFieldname = this.searchConfig.LLS_SEARCH.FIELD_LLS;
      queryTask = new QueryTask(this.searchConfig.LLS_SEARCH.MAPSERVICE_LAYER_URL);
      query = new Query();
      query.returnGeometry = false;
      query.outFields = [outFieldname];
      query.orderByFields = [outFieldname];
      query.where = outFieldname + " <> ''";
      queryTask.execute(query, lang.hitch(this, function (results) {
        var val;
        var values = [];
        var testVals = {};
        // Get unique values from results
        var features = results.features;
        arrayUtils.forEach(features, lang.hitch(this, function (feature) {
          val = feature.attributes[outFieldname];
          if (!testVals[val]) {
            testVals[val] = true;
            values.push({ name: val });
          }
        }));
        // Create memory data store for filterSelect data source
        var dataItems = {
          identifier: 'name',
          label: 'name',
          items: values
        };
        var llsStore = new Memory({ data: dataItems });
        this.llsNode.store = llsStore;
        this.llsNode.set('value', values[0].name); //select the first item
      }));
    },

    _loadMitchell: function () {
      /// <summary> 
      /// Populate the Mitchell filterSelect drop down with unique values.
      /// </summary>
      var outFieldname = this.searchConfig.MITCHELL_SEARCH.FIELD_MITCHELL;
      queryTask = new QueryTask(this.searchConfig.MITCHELL_SEARCH.MAPSERVICE_LAYER_URL);
      query = new Query();
      query.returnGeometry = false;
      query.outFields = [outFieldname];
      query.orderByFields = [outFieldname];
      query.where = outFieldname + " <> ''";
      queryTask.execute(query, lang.hitch(this, function (results) {
        var val;
        var values = [];
        var testVals = {};
        // Get unique values from results
        var features = results.features;
        arrayUtils.forEach(features, lang.hitch(this, function (feature) {
          val = feature.attributes[outFieldname];
          if (!testVals[val]) {
            testVals[val] = true;
            values.push({ name: val });
          }
        }));
        // Create memory data store for filterSelect data source
        var dataItems = {
          identifier: 'name',
          label: 'name',
          items: values
        };
        var mitchellStore = new Memory({ data: dataItems });
        this.mitchellNode.store = mitchellStore;
        this.mitchellNode.set('value', values[0].name); //select the first item
      }));
    },

    _getSelectionPointSymbol: function() {
      /// <summary> 
      /// Returns compiled selection point symbol
      /// </summary>
      return new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 10,
             new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 0, 0], 2),
             new Color([255, 255, 0, 0.5])));
    },
    
    _getSelectionPolygonSymbol: function() {
      /// <summary> 
      /// Returns compiled selection polygon symbol
      /// </summary>
      return new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
             new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
             new Color([255, 0, 0]), 2), new Color([255, 255, 0, 0.3]));
      //return new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,   // DEV Note: short dash won't print
      //       new SimpleLineSymbol(SimpleLineSymbol.STYLE_SHORTDASH,
      //       new Color([255, 0, 0]), 2), new Color([255, 255, 0, 0.3]));
    },
    
    _getSelectionPolylineSymbol: function() {
      /// <summary> 
      /// Returns compiled selection line symbol
      /// </summary>
      return new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 0, 0]), 2);
      //return new SimpleLineSymbol(SimpleLineSymbol.STYLE_SHORTDASH, new Color([255, 0, 0]), 2); // DEV Note: short dash won't print
    }
    
	})

  return Widget;
  
});