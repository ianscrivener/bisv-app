﻿/*
 | Author: NGIS 2015 (www.ngis.com.au) -> Kristy Carter
 | Module: AreaReportWidget
 | Description: Area Report Widget - user interface for presenting the area aggregate report.
 | Usage: See readme.
 |
 | CHANGE LOG:
 |   2015-06-10 Kristy Carter: Initial code release.
 */

define([
	"dojo/_base/declare", "dojo/Evented", "dojo/_base/array", "dojo/store/Memory",
  "dojo/dom", "dojo/dom-construct", "dojo/dom-style", "dojo/dom-attr", "dojo/on", "dojo/html", "dojo/_base/lang", "dojo/request/xhr",
  "dijit/_WidgetBase", "dijit/_TemplatedMixin", "dijit/_WidgetsInTemplateMixin", "dojo/text!./templates/areaReportWidget.html",
  "esri/map", "dijit/form/Select", "dijit/form/TextBox", "dijit/form/RadioButton", "dijit/Fieldset", "dijit/TitlePane",
  "dijit/form/Button", "dijit/layout/ContentPane", "dojox/widget/Standby"
],
function (declare, Evented, arrayUtils, Memory,
  dom, domConstruct, domStyle, domAttr, on, html, lang, xhr,
  _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, template,
  Map
) {
  var Widget = declare("ngisWidget.AreaReportWidget", [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, Evented], {
    templateString: template,
    baseClass: "AreaReportWidget",

    loadingGif: "//ajax.googleapis.com/ajax/libs/dojo/1.9.5/dijit/themes/claro/images/loadingAnimation.gif",
    
    // Custom setters
    printResultsAvailable: false,
    _setPrintResultsAvailableAttr: function (/*Boolean*/ value) { // attach to DOM node style attribute
      this._set("printResultsAvailable", value);
      domStyle.set(this.printLinkHintNode, "display", (value ? "block" : "none"));
    },
    printDisabled: false,
    _setPrintDisabledAttr: function (/*Boolean*/ value) { // attach to DOM node style attribute
      this._set("printDisabled", value);
      this.printNode.set("disabled", value);
    },
    reportVisible: false,
    _setReportVisibleAttr: function (/*Boolean*/ value) { // attach to DOM node style attribute
      this._set("reportVisible", value);
      domStyle.set(this.reportNode, "display", (value ? "block" : "none"));
      domStyle.set(this.printHeaderNode, "display", (value ? "block" : "none" )); 
    },
    warningVisible: false,
    _setWarningVisibleAttr: function (/*Boolean*/ value) { // attach to DOM node style attribute
      this._set("warningVisible", value);
      domStyle.set(this.warningNode, "display", (value ? "block" : "none"));
    },

    // Widget option defaults (static)
    options: {
      map: null
    },

    constructor: function (options, srcRefNode) {
      /// <summary>
      /// Widget constructor.
      /// <param name="options" type="Object">
      ///   Hash of initialisation options. The hash can contain any of the widget's properties, excluding read-only properties.
      /// </param>
      /// <param name="srcRefNode" type="DOMNode|String">
      ///   HTML source node reference.
      /// </param>
      /// </summary>
      this.domNode = srcRefNode; // widget node
      var defaults = lang.mixin({}, this.options, options); // mix in param options with defaults
      // Properties
      this.set("loaded", false);
      this.set("report_conservationLayers", []);
      this.set("map", defaults.map);
      this.set("mapId", defaults.mapId);
      this.set("webServiceUrl", defaults.webServiceUrl);
      this.set("handlerUrl", defaults.handlerUrl);
      this.set("layerConfig", defaults.layerConfig);
      this.set("conservationCommitmentLayerIds", defaults.conservationCommitmentLayerIds);
      this.set("userToken", defaults.userToken);
      this.set("printTimeout", defaults.printTimeout); //NOTE: timeout not currently used
    },

    postCreate: function () {
      /// <summary>
      /// General widget initialisations.
      /// </summary>
        this.inherited(arguments);
      this.own(
        on(this.printNode, "click", lang.hitch(this, this.PrintReport))
      );
      this.set("printDisabled", false);
      this.set("reportVisible", false);
      this.set("warningVisible", false);
      this.set("printResultsAvailable", false);
      this._clearReport();
    },

    startup: function () {
      /// <summary>
      /// Start widget - called by user.
      /// </summary>
      this.inherited(arguments);
      // Ensure map was specified
      if (!this.map) {
        this.destroy();
        console.log(this.baseClass + ' :: map required before startup!');
      }
      // Ensure map is loaded before initialising
      if (this.map.loaded) {
        this._init();
      } else {
        on.once(this.map, "load", lang.hitch(this, function () {
          this._init();
        }));
      }
    },

    destroy: function () {
      /// <summary>
      /// Connections/subscriptions will be cleaned up during the destroy() lifecycle phase.
      /// </summary>
      this.inherited(arguments);
      this.map = null;
    },

    /* ---------------- */
    /* Public Functions */
    /* ---------------- */
    
    SetCurrentSelection: function(selectedGraphics) {
      this.set("report_selectedGraphics", selectedGraphics);
    },

    RunReport: function (selectedGraphics) {
      this.set("report_selectedGraphics", selectedGraphics);
      // Show busy animation
      this._showBusyReport();
      // Clear report
      this._clearReport(false);
      // Check valid selection layer
      var lyr = this._getValidSelectionLayer(selectedGraphics);
      if (lyr != null) {
        // Check conservation layers visible
        var conservationLayers = this._getVisibleConservationLayers();
        if (conservationLayers.length > 0) {
          // Run aggregate area report
          this.set("report_selectedGraphics", selectedGraphics);
          this.set("report_conservationLayers", conservationLayers);
          this.set("report_layerId", lyr.LayerId);
          this.set("report_oidField", lyr.OIDFieldName);
          this.set("report_selectedFeatureId", selectedGraphics[0].attributes[lyr.OIDFieldName]);
          this._aggregateAreaCalcRequest();
        } else {
          this._warnNoVisibleConservationLayers();
          this._showNotBusy();
        }
      } else {
        this._warnInvalidSelection();
        this._showNotBusy();
      }
    },

    ClearReport: function (invalidSelection) {
      this._clearReport();
      if (invalidSelection) {
        this._warnInvalidSelection();
      }
      this._showNotBusy();
    },

    PrintReport: function (evt) {
      /// <summary>
      /// DOM event handler for the print button. Requests a report print from the server handler.
      /// </summary>
      this._showBusyPrint();
      this._aggregateAreaPrintRequest();
    },

    /* ----------------- */
    /* Private Functions */
    /* ----------------- */

    _init: function () {
      /// <summary>
      /// Map dependant widget initialisations.
      /// </summary>
      var layer = this.map.getLayer(this.mapId);
      this.own(
        on(layer, "update-end", lang.hitch(this, function (e) {  // DEV Note: visible-layers-change event doesn't fire when sub-layer visibility changed so using update-end event instead
          if (this.reportVisible || this.warningVisible) {
            if (this._checkVisibleConservationLayersChanged()) {
              this.RunReport(this.report_selectedGraphics);
            }
          }
        }))
      );
      // Widget is now loaded - emit the loaded event
      this.set("loaded", true);
      this.emit("load", {});
    },

    _clearReport: function () {
      /// <summary>
      /// Clear details from report tab.
      /// </summary>
      this.set("reportVisible", false);
      this.set("warningVisible", false);
      this.printResultsAvailable = false;
      domConstruct.empty(this.headerRows);
      domConstruct.empty(this.ccRows);
      domConstruct.empty(this.pdfNode);
      cell = domConstruct.toDom('PDF');
      domConstruct.place(cell, this.pdfNode);
      domConstruct.empty(this.xlsNode);
      cell = domConstruct.toDom('Excel');
      domConstruct.place(cell, this.xlsNode);
    },

    _aggregateAreaPrintRequest: function () {
      /// <summary>
      /// Request printable report data from server.
      /// </summary>
      var url = this.handlerUrl; //this.webServiceUrl + "/getAggregateAreaSummaryReport";
      var printType = (this.pdfRadioNode.checked ? "pdf" : "xls");
      // SAMPLE INPUT:: token: "123", mapId: 0, mapName: "BISV_Internal", assessmentToolLayerId: 1, selectedFeatureId: 3, conservationCommitmentLayerIds: [14,15,16,17,18,19,20,21,22,23,24,25,26,27], reportType: "pdf"
      if (!window.XDomainRequest) {
        var $request = {
          token: this.userToken, 
          mapId: 0,
          mapName: this.mapId,
          assessmentToolLayerId: this.report_layerId,
          selectedFeatureId: this.report_selectedFeatureId, 
          conservationCommitmentLayerIds: this.report_conservationLayers,
          reportType: printType
        };
			  $.support.cors=true;
			  wcfUrl = url;
			  $.ajax({
			    url: wcfUrl,
			    type: "POST",
			    data: $request,
			    success: lang.hitch(this, this._printComplete),
			    error: lang.hitch(this, this._printError)
			  });
        //DEV NOTE: Dojo XHR request doesn't work with handler => Using JQuery instead (above). Dojo.xhrGet limitation -> unable to post binary data.
        //xhr.post(url, {
        //  //data: ({
        //  //  token: this.userToken, 
        //  //  mapId: 0,
        //  //  mapName: this.mapId,
        //  //  assessmentToolLayerId: this.report_layerId,
        //  //  selectedFeatureId: this.report_selectedFeatureId, 
        //  //  conservationCommitmentLayerIds: this.report_conservationLayers,
        //  //  reportType: printType
        //  //}),
        //  //headers: { 'X-Requested-With': null , 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
        //  //,
        //  //headers: { 'Content-Type': 'application/json' }
        //}).then(lang.hitch(this, function (data) {
        //  this._printComplete(data); //data.d);
        //}), lang.hitch(this, this._printError));
      } else {
        this._xdr(url, {
          method: "POST",
          //data: JSON.stringify({
          data: ({
            token: this.userToken,
            mapId: 0,
            mapName: this.mapId,
            assessmentToolLayerId: this.report_layerId, 
            selectedFeatureId: this.report_selectedFeatureId, 
            conservationCommitmentLayerIds: this.report_conservationLayers, 
            reportType: printType
          })
        }).then(lang.hitch(this, function (response) {
          //var data = JSON.parse(response, true);
          this._printComplete(data); //data.d);
        }), lang.hitch(this, this._printError));
      }
    },

    _aggregateAreaCalcRequest: function () {
      /// <summary>
      /// Request report data from server.
      /// </summary>
      var url = this.webServiceUrl + "/calculateAggregateAreaFromDatabase";
      var printType = (this.pdfRadioNode.checked ? "pdf" : "xls");
      // SAMPLE INPUT:: token: "123", mapId: 0, mapName: "BISV_Internal", assessmentToolLayerId: 6, selectedFeatureId: 3, conservationCommitmentLayerIds: [16,17,18,19,20,21,22,23,24,25,26,27], reportType: "pdf"
      // SAMPLE INPUT:: token: "123", mapId: 0, mapName: "BISV_Internal", assessmentToolLayerId: 1, selectedFeatureId: 3, conservationCommitmentLayerIds: [14,15,16,17,18,19,20,21,22,23,24,25,26,27], reportType: "pdf"
      if (!window.XDomainRequest) {
        //console.log("input data...");
        /*console.log(JSON.stringify({
          token: this.userToken,
          mapId: 0,
          mapName: this.mapId,
          assessmentToolLayerId: this.report_layerId,
          selectedFeatureId: this.report_selectedFeatureId,
          conservationCommitmentLayerIds: this.report_conservationLayers,
          reportType: printType
        }));*/
        xhr.post(url, {
           handleAs: "json",
           data: JSON.stringify({
            token: this.userToken,
            mapId: 0,
            mapName: this.mapId,
            assessmentToolLayerId: this.report_layerId,  
            selectedFeatureId: this.report_selectedFeatureId, 
            conservationCommitmentLayerIds: this.report_conservationLayers,
            reportType: printType
          }),
          headers: { 'Content-Type': 'application/json' }
        }).then(lang.hitch(this, function (data) {
          this._processReportResponse(data.d);
        }), lang.hitch(this, this._processReportResponseError));
      } else {
        this._xdr(url, {
          method: "POST",
          data: JSON.stringify({
            token: this.userToken,
            mapId: 0,
            mapName: this.mapId,
            assessmentToolLayerId: this.report_layerId, 
            selectedFeatureId: this.report_selectedFeatureId, 
            conservationCommitmentLayerIds: this.report_conservationLayers, 
            reportType: printType
          })
        }).then(lang.hitch(this, function (response) {
          var data = JSON.parse(response, true);
          this._processReportResponse(data.d);
        }), lang.hitch(this, this._processReportResponseError));
      }
    },
	  
    _processReportResponse: function (response) {
      /// <summary>
      /// Server request completed successfully. Present report to the user.
      /// <param name="response" type="Object">Report data response from server.</param>
      /// </summary>
      // Format the report response
      var row1 = domConstruct.toDom("<tr><td><strong>Search Area Type:</strong></td><td>" + response.AssessmentToolLayerName + "</td></tr>");
      var row2 = domConstruct.toDom("<tr><td><strong>Search Area (ha):</strong></td><td>" + response.AreaOfSerachedArea + "</td></tr>");
      var row3 = domConstruct.toDom("<tr><td width='220'><strong>Conservation Commitment Types:</strong></td><td>" + response.ConservationCommitmentTypeSearched + "</td></tr>");
      domConstruct.place(row1, this.headerRows);
      domConstruct.place(row2, this.headerRows);
      domConstruct.place(row3, this.headerRows);
      arrayUtils.forEach(response.ConservationCommitmentTypes, function (cc) {
        var row = domConstruct.toDom("<tr><td width='350'>" + cc.ConservationCommitmentLayerName + "</td><td>" + cc.AreaWithinSearchedArea + "</td><td>" + cc.ProportionOfSearchedArea + "</td></tr>");
        domConstruct.place(row, this.ccRows);
      }, this);
      var row4 = domConstruct.toDom("<tr><td width='350'><strong>Total area within conservation commitments*</strong></td><td><strong>" +
        response.TotalAreaWithinConservationCommitments + "</strong></td><td><strong>" + response.TotalProportionWithinConservationCommitments + "</strong></td></tr>");
      domConstruct.place(row4, this.ccRows);
      // Show the report
      this.set("warningVisible", false);
      this.set("reportVisible", true);
      this._showNotBusy();
    },

    _printComplete: function (result) {
      /// <summary>
      /// Print report server request completed successfully. Present printed report to the user as a download link. Additionally open it in new window.
      /// <param name="result" type="base64 encoded string">PDF or XLS file streamed from server.</param>
      /// </summary>
      var cell;
      if (this.pdfRadioNode.checked) {
        domConstruct.empty(this.pdfNode);
        cell = domConstruct.toDom('<a href="data:Application/PDF;base64,' + result + '" download="AreaAggregateReport.pdf">PDF</a>');
        domConstruct.place(cell, this.pdfNode);
        // Open in new window
        window.open("data:application/PDF;base64," + result, "", "width=800, height=750");
      } else {
        domConstruct.empty(this.xlsNode);
        var cell = domConstruct.toDom('<a href="data:Application/XLS;base64,' + result + '" download="AreaAggregateReport.xls">Excel</a>');
        domConstruct.place(cell, this.xlsNode);
        // Open in new window
        window.open("data:application/XLS;base64," + result, "", "width=800, height=750");
      }
      this.set("printResultsAvailable", true);
      this._showNotBusy();
    },

    _processReportResponseError: function (error) {
      /// <summary>
      /// Warn user: report request error
      /// </summary>
      console.log("ERROR :: " + this.baseClass + "_processReportResponseError...");
      console.log(error);
      var warningNode = dom.byId(this.baseClass + "_warningNode");
      domConstruct.empty(warningNode);
      var errString = domConstruct.toDom("<div class='warning'><strong><span class='printError'>Report error!</span></strong>" +
      "<div class='warningInfo'>An error occurred while requesting the aggregate area report from the server.</div></div>");
      domConstruct.place(errString, warningNode);
      this.set("warningVisible", true);
      this._showNotBusy();
    },

    _printError: function (error) {
      /// <summary>
      /// Print report request onError event error back. Report error to user.
      /// </summary>   
      console.log("ERROR :: " + this.baseClass + "_printError...");
      console.log(error);   
      var cell;
      if (this.pdfRadioNode.checked) {
        domConstruct.empty(this.pdfNode);
        cell = domConstruct.toDom('<span class="errorText">PDF...Print Error!</span>');
        domConstruct.place(cell, this.pdfNode);
      } else {
        domConstruct.empty(this.xlsNode);
        var cell = domConstruct.toDom('<span class="errorText">Excel...Print Error!</span>');
        domConstruct.place(cell, this.xlsNode);
      }
      this._showNotBusy();
    },
    
    _warnNoVisibleConservationLayers: function () {
      /// <summary>
      /// Warn user: no visible conservation commitment layers
      /// </summary>
      this.set("report_conservationLayers", []);
      var warningNode = dom.byId(this.baseClass + "_warningNode");
      domConstruct.empty(warningNode);
      var errString = domConstruct.toDom("<div class='warning'><strong><span class='printError'>No visible conservation commitment layers!</span></strong>" +
      "<div class='warningInfo'>This report requires at least one visible conservation commitment layer!</div></div>");
      domConstruct.place(errString, warningNode);
      this.set("warningVisible", true);
    },

    _warnInvalidSelection: function () {
      /// <summary>
      /// Warn user: invalid selection
      /// </summary>
      var warningNode = dom.byId(this.baseClass + "_warningNode");
      domConstruct.empty(warningNode);
      var errString = domConstruct.toDom("<div class='warning'><strong><span class='printError'>No valid search selection!</span></strong>" +
        "<div class='warningInfo'>Please ensure only one of the following feature types is selected using the Search tool:<br />" +
        "<ul><li>Local Government Area</li>" +
        "<li>Sub-Bioregion</li>" +
        "<li>CMA</li>" +
        "<li>LLS</li>" +
        "<li>Mitchell Landscapes</li></ul></div></div>");
      domConstruct.place(errString, warningNode);
      this.set("warningVisible", true);
    },

    _checkVisibleConservationLayersChanged: function () {
      /// <summary>
      /// Check if visible conservation commitment layers has changed
      /// </summary>
      var visibleLayers = this._getVisibleConservationLayers();
      visibleLayers.sort();
      this.report_conservationLayers.sort();
      if (JSON.stringify(visibleLayers) == JSON.stringify(this.report_conservationLayers)) {
        return false; // conservation layer visibility hasn't changed
      } else {
        return true; // conservation layer visibility has changed
      }
    },

    _getVisibleConservationLayers: function () {
      /// <summary>
      /// Return visible conservation commitment layers.
      /// </summary>
      var visConservationLayers = [];
      arrayUtils.forEach(this.conservationCommitmentLayerIds, function (lyr) {
        if (this._isLayerVisible(lyr.MAPSERVICE_LAYER_URL)) {
          visConservationLayers.push(lyr.databaseLayerID);
        }
      }, this);
      return visConservationLayers;
    },

    _getValidSelectionLayer: function (selectedGraphics) {
      /// <summary>
      /// Return search layer details.
      /// </summary>
      if (!selectedGraphics) {
        return null;
      }
      if (selectedGraphics.length == 1) {
        if ( selectedGraphics[0].sourceUrl == this.layerConfig.LGA_SEARCH.MAPSERVICE_LAYER_URL) { 
          return { LayerId: this.layerConfig.LGA_SEARCH.databaseLayerID, OIDFieldName: this.layerConfig.LGA_SEARCH.OBJECTID }; 
        }
        if ( selectedGraphics[0].sourceUrl == this.layerConfig.IBRA_SEARCH.MAPSERVICE_LAYER_URL) { 
          return {  LayerId: this.layerConfig.IBRA_SEARCH.databaseLayerID, OIDFieldName: this.layerConfig.IBRA_SEARCH.OBJECTID }; 
        }
        if ( selectedGraphics[0].sourceUrl == this.layerConfig.CMA_SEARCH.MAPSERVICE_LAYER_URL) {
          return {  LayerId: this.layerConfig.CMA_SEARCH.databaseLayerID, OIDFieldName: this.layerConfig.CMA_SEARCH.OBJECTID };
        }
        if ( selectedGraphics[0].sourceUrl == this.layerConfig.LLS_SEARCH.MAPSERVICE_LAYER_URL) {
          return {  LayerId: this.layerConfig.LLS_SEARCH.databaseLayerID, OIDFieldName: this.layerConfig.LLS_SEARCH.OBJECTID };
        }
        if (selectedGraphics[0].sourceUrl == this.layerConfig.MITCHELL_SEARCH.MAPSERVICE_LAYER_URL) {
          return {  LayerId: this.layerConfig.MITCHELL_SEARCH.databaseLayerID, OIDFieldName: this.layerConfig.MITCHELL_SEARCH.OBJECTID };
        }
      } 
      return null;
    },

    _isLayerVisible: function (url) {
      /// <summary>
      /// Return true if layer is visible in map, false if not visible.
      /// </summary>
      var lyrId = url.substring(url.lastIndexOf("/") + 1);
      var visLayers = this.map.getLayersVisibleAtScale();
      for (var i = 0; i < visLayers.length; i++) {
        if (!visLayers[i].url) { continue; } // graphics layers have null url
        if (url.indexOf(visLayers[i].url) > -1) {
          var lyrInfos = visLayers[i].layerInfos;
          for (var j = 0; j < lyrInfos.length; j++) {
            if (lyrInfos[j].id.toString() == lyrId) {
              if (lyrInfos[j].visible) { return true; }
            }
          }
        }
      }
      return false;
    },

    _showBusyReport: function () {
      /// <summary>
      /// Show busy animation on report panel
      /// </summary>
      this.busyNode.set("target", this.domNode.parentNode);
      this.busyNode.show();
    },
    _showBusyPrint: function () {
      /// <summary>
      /// Show busy animation on print panel
      /// </summary>
      this.busyNode.set("target", this.printHeaderNode);
      this.busyNode.show();
    },
    _showNotBusy: function () {
      /// <summary>
      /// Clear busy animation
      /// </summary>
      this.busyNode.hide();
    },

    _xdr: function (url, options) {
      /// <summary>
      /// IE9 CORS AJAX request using XDomainRequest.
      /// DEV NOTE: (KC) IE9 XHR CORS issue ...http://stackoverflow.com/questions/14587347/dojo-cross-domain-access-is-denied
      /// </summary>
      var def = new Deferred();
      var xdr = new XDomainRequest();
      if (xdr) {
        xdr.timeout = 30000;
        xdr.onload = function (e) {
          def.resolve(xdr.responseText);
        }
        xdr.onerror = function () {
          console.log("ERROR :: " + this.baseClass + " ... XDR Error");
        };
        xdr.ontimeout = function () {
          console.log("ERROR :: " + this.baseClass + "... XDR Timeout Error");
        };
        // bug fix: this must be set
        xdr.onprogress = function () {
          //console.log('progress');
        };
        xdr.open(options.method, url);
        xdr.send(options.data);
        return def;
      }
      def.reject(new Error("ERROR :: " + this.baseClass + "... XDomainRequest not supported."));
      return def;
    }

  })

  return Widget;

});