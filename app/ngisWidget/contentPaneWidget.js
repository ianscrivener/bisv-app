/*
 | Author: NGIS 2015 (www.ngis.com.au) -> Kristy Carter
 | Module: contentPaneWidget
 | Description: contentPane Widget - custom layout widget. 
     contentPaneWidget is a similar implementation of a dijit/layout/ContentPane but with a title bar (similar to the accordion container child) at the top.
 | Usage: See readme.
 |
 | CHANGE LOG:
 |   2015-03-17 Kristy Carter: Initial code release.
 */
 
define([
	"dojo/_base/declare",
	"dijit/_WidgetBase",
  "dijit/layout/_LayoutWidget",
	"dijit/_TemplatedMixin",
  "dijit/_WidgetsInTemplateMixin",
	"dojo/dom-style",
  "dojo/dom-geometry",
	"dojo/text!./templates/contentPaneWidget.html"
],
function(declare,
  _WidgetBase,
  _LayoutWidget,
  _TemplatedMixin,
  _WidgetsInTemplateMixin,
  domStyle,
  domGeom,
  template
) {

	var Widget = declare("ngisWidget.ContentPaneWidget", [_WidgetBase, _LayoutWidget, _TemplatedMixin, _WidgetsInTemplateMixin], {
		templateString: template,
    baseClass: "ContentPaneWidget",
    
    title: "Title",
    // title custom setter - attach to DOM node innerHTML
    _setTitleAttr: { node: "titleNode", type: "innerHTML" },
    
    titlePaneColor: "rgb(67, 87, 113)",
    // title custom setter - attach to DOM node style attribute
    _setTitlePaneColorAttr: function(/*String*/ color){
      this._set("titlePaneColor", color);
      domStyle.set(this.titleNode, "backgroundColor", color);
    },
    
    titleTextColor: "#FFFFFF",
    // titleTextColor custom setter - attach to DOM node style attribute
    _setTitleTextColorAttr: function(/*String*/ color){
      this._set("titlePaneColor", color);
      domStyle.set(this.titleNode, "color", color);
    },
    
    contentColor: "#FFFFFF", //"#E7E7E7",   
    // contentColor custom setter - attach to DOM node style attribute
    _setContentColorAttr: function(/*String*/ color){
      this._set("titlePaneColor", color);
      domStyle.set(this.contentPaneNode, "backgroundColor", color);
    },
    
    
    constructor: function() {
      this.inherited(arguments);
    },

    startup: function() {
      this.inherited(arguments);
    },
        
    postCreate: function() {
      this.inherited(arguments);
    },
    
    destroy: function() {
      /// <summary>
      /// Connections/subscriptions will be cleaned up during the destroy() lifecycle phase.
      /// </summary>
      this.inherited(arguments);
    },
    
    layout: function() {
      /// <summary>
      /// Resize children.
      /// </summary>
      // Resize child nodes
      this.inherited(arguments);
    //  domStyle.set(this.contentPaneNode, "overflow", "hidden"); // Important Note: resize without scrollbars
    //  console.log(this.domNode.parentNode);
    //  var box = { w: (domGeom.position(this.domNode.parentNode, true).w ),
    //              h: (domGeom.position(this.domNode.parentNode, true).h ) };
    //  domGeom.setContentSize(this.containerNode, box, computedStyle);
    //  domGeom.setMarginBox(this.titleNode, {w: box.w}, computedStyle);
    //  domStyle.set(this.contentPaneNode, "overflow", "auto"); // restore scrollbar setting
    }
    
	});
  
  return Widget;
  
});