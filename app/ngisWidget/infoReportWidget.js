﻿/*
 | Author: NGIS 2015 (www.ngis.com.au) -> Kristy Carter
 | Module: InfoReportWidget
 | Description: Info Report Widget - user interface for presenting the plan info report.
 | Usage: See readme.
 |
 | CHANGE LOG:
 |   2015-07-08 Kristy Carter: Initial code release.
 */

define([
	"dojo/_base/declare", "dojo/_base/Color", "dojo/Evented", "dojo/_base/array", "dojo/store/Memory", "dojo/query", "dojo/NodeList-dom",
  "dojo/dom", "dojo/dom-construct", "dojo/dom-style", "dojo/dom-attr", "dojo/on", "dojo/html", "dojo/_base/lang", "dojo/request/xhr", "dojo/Deferred", 
  "dijit/_WidgetBase", "dijit/_TemplatedMixin", "dijit/_WidgetsInTemplateMixin", "dojo/text!./templates/infoReportWidget.html",
  "esri/map", "esri/tasks/GeometryService", "esri/layers/GraphicsLayer", "esri/tasks/QueryTask", "esri/tasks/query",
  "esri/symbols/SimpleLineSymbol", "esri/symbols/SimpleFillSymbol", "esri/graphic",
  "dijit/Fieldset", "dijit/form/Select", "dijit/form/TextBox", "dijit/form/RadioButton", "dijit/TitlePane",
  "dijit/form/Button", "dijit/layout/ContentPane", "dojox/widget/Standby"
],
function (declare, Color, Evented, arrayUtils, Memory, query, nodeListDom,
  dom, domConstruct, domStyle, domAttr, on, html, lang, xhr, Deferred,
  _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, template,
  Map, GeometryService, GraphicsLayer, QueryTask, Query,
  SimpleLineSymbol, SimpleFillSymbol, Graphic,
  Fieldset
) {
  var Widget = declare("ngisWidget.InfoReportWidget", [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, Evented], {
    templateString: template,
    baseClass: "InfoReportWidget",

    loadingGif: "//ajax.googleapis.com/ajax/libs/dojo/1.9.5/dijit/themes/claro/images/loadingAnimation.gif",
    
    // Custom setters
    printResultsAvailable: false,
    _setPrintResultsAvailableAttr: function (/*Boolean*/ value) { // attach to DOM node style attribute
      this._set("printResultsAvailable", value);
      domStyle.set(this.printLinkHintNode, "display", (value ? "block" : "none"));
    },
    printDisabled: false,
    _setPrintDisabledAttr: function (/*Boolean*/ value) { // attach to DOM node style attribute
      this._set("printDisabled", value);
      this.printNode.set("disabled", value);
    },
    reportVisible: false,
    _setReportVisibleAttr: function (/*Boolean*/ value) { // attach to DOM node style attribute
      this._set("reportVisible", value);
      domStyle.set(this.reportNode, "display", (value ? "block" : "none"));
      domStyle.set(this.printHeaderNode, "display", (value ? "block" : "none" )); 
    },
    warningVisible: false,
    _setWarningVisibleAttr: function (/*Boolean*/ value) { // attach to DOM node style attribute
      this._set("warningVisible", value);
      domStyle.set(this.warningNode, "display", (value ? "block" : "none"));
    },

    // Widget option defaults (static)
    options: {
      map: null
    },

    constructor: function (options, srcRefNode) {
      /// <summary>
      /// Widget constructor.
      /// <param name="options" type="Object">
      ///   Hash of initialisation options. The hash can contain any of the widget's properties, excluding read-only properties.
      /// </param>
      /// <param name="srcRefNode" type="DOMNode|String">
      ///   HTML source node reference.
      /// </param>
      /// </summary>
      this.domNode = srcRefNode; // widget node
      var defaults = lang.mixin({}, this.options, options); // mix in param options with defaults
      // Properties
      this.set("loaded", false);
      this.set("report_conservationLayers", []);
      this.set("map", defaults.map);
      this.set("geomService", defaults.geomService);
      this.set("mapId", defaults.mapId);
      this.set("webServiceUrl", defaults.webServiceUrl);
      this.set("handlerUrl", defaults.handlerUrl);
      this.set("lotSearchLayerConfig", defaults.lotSearchLayerConfig);
      this.set("conservationCommitmentLayerIds", defaults.conservationCommitmentLayerIds);
      this.set("userToken", defaults.userToken);
      this.set("printTimeout", defaults.printTimeout); //NOTE: timeout not currently used
    },

    postCreate: function () {
      /// <summary>
      /// General widget initialisations.
      /// </summary>
        this.inherited(arguments);
      this.own(
        on(this.printNode, "click", lang.hitch(this, this._printReport))
      );
      this.set("printDisabled", false);
      this.set("reportVisible", false);
      this.set("warningVisible", false);
      this.set("printResultsAvailable", false);
      this._clearReport();
    },

    startup: function () {
      /// <summary>
      /// Start widget - called by user.
      /// </summary>
      this.inherited(arguments);
      // Ensure map was specified
      if (!this.map) {
        this.destroy();
        console.log(this.baseClass + ' :: map required before startup!');
      }
      // Ensure map is loaded before initialising
      if (this.map.loaded) {
        this._init();
      } else {
        on.once(this.map, "load", lang.hitch(this, function () {
          this._init();
        }));
      }
    },

    destroy: function () {
      /// <summary>
      /// Connections/subscriptions will be cleaned up during the destroy() lifecycle phase.
      /// </summary>
      this.inherited(arguments);
      this.map = null;
    },

    /* ---------------- */
    /* Public Functions */
    /* ---------------- */
    
    SetCurrentSelection: function(selectedGraphics) {
      this.set("report_selectedGraphics", selectedGraphics);
    },

    RunReport: function (selectedGraphics) {
      this.set("report_selectedGraphics", selectedGraphics);
      // Cancel any existing requests before issuing new report
      this._cancelRequest(); 
      // Show busy animation
      this._showBusyReport();
      // Clear report
      this._clearReport(false);
      // Check valid selection
      if (this._checkValidSelection(selectedGraphics)) {
        // Check conservation layers visible
        var conservationLayers = this._getVisibleConservationLayers();
        if (conservationLayers.length > 0) {
          // Run info report
          this.set("report_conservationLayers", conservationLayers);
          this.set("report_lotOIDs", []);
          this._prepareGraphicsForInfoRequest(selectedGraphics, this.lotSearchLayerConfig.MAPSERVICE_LAYER_URL, this.lotSearchLayerConfig.OBJECTID);
        } else {
          this._warnNoVisibleConservationLayers();
          this._showNotBusy();
        }
      } else {
        this._warnInvalidSelection();
        this._showNotBusy();
      }
    },

    ClearReport: function () {
      // Cancel any existing requests before clearing the report
      this._cancelRequest(); 
      this._clearReport();
      this._showNotBusy();
    },


    /* ----------------- */
    /* Private Functions */
    /* ----------------- */

    _init: function () {
      /// <summary>
      /// Map dependant widget initialisations.
      /// </summary>
      var layer = this.map.getLayer(this.mapId);
      this.own(
        on(layer, "update-end", lang.hitch(this, function (e) {  // DEV Note: visible-layers-change event doesn't fire when sub-layer visibility changed so using update-end event instead
          if (this.reportVisible || this.warningVisible) {
            if (this._checkVisibleConservationLayersChanged()) {
              this.RunReport(this.report_selectedGraphics);
            }
          }
        }))
      );
      // Create graphics layer for displaying selected item for this widget
      var selectionGraphicsLayer = new GraphicsLayer();
      this.set("graphicsLayer", selectionGraphicsLayer);
      this.map.addLayer(selectionGraphicsLayer);
      // Widget is now loaded - emit the loaded event
      this.set("loaded", true);
      this.emit("load", {});
    },

    _clearReport: function () {
      /// <summary>
      /// Clear details from report tab.
      /// </summary>
      this.set("reportVisible", false);
      this.set("warningVisible", false);
      this.printResultsAvailable = false;
      domConstruct.empty(this.resultRows);
      domConstruct.empty(this.pdfNode);
      cell = domConstruct.toDom('PDF');
      domConstruct.place(cell, this.pdfNode);
      domConstruct.empty(this.xlsNode);
      cell = domConstruct.toDom('Excel');
      domConstruct.place(cell, this.xlsNode);
      this._clearSelectedFeature(); // clear the graphics layer
    },
    
    _printReport: function (evt) {
      /// <summary>
      /// DOM event handler for the print button. Requests a report print from the server handler.
      /// </summary>
      this._showBusyPrint();
      this._infoPrintRequest();
    },

    _prepareGraphicsForInfoRequest: function (selectedGraphics, lotLayerURL, OIDFieldName) {
      if (selectedGraphics.length == 1) {
        this._selectLotsForInfoRequest(selectedGraphics[0].geometry, lotLayerURL, OIDFieldName);
      } else {
        var selectedGeometries = [];
        // Union geometries in the selectedGraphics
        for (var i = 0; i < selectedGraphics.length; i++) {
          selectedGeometries.push(selectedGraphics[i].geometry)
        }
        this.geomService.union(selectedGeometries, lang.hitch(this, function (result) {
          this._selectLotsForInfoRequest(result, lotLayerURL, OIDFieldName);
        }));
      }
    },

    _selectLotsForInfoRequest: function (geometry, lotLayerURL, OIDFieldName) {
      /// <summary>
      /// Selects all lots within the given geometry.
      /// </summary>
      var query = new Query();
      query.spatialRelationship = Query.SPATIAL_REL_CONTAINS;
      query.returnGeometry = false;
      query.outFields = [OIDFieldName];
      query.geometry = geometry;
      query.where = OIDFieldName + " > 0";
      var queryTask = new QueryTask(lotLayerURL);
      queryTask.execute(query, lang.hitch(this, function (results) {
        // Get the list of objectids and create a list containing the first 100 results
        var recordcount = 100;
        if (results.features.length < 100) {
          recordcount = results.features.length;
        }
        var objectidlist = [];
        for (var i = 0; i < recordcount; i++) {
          objectidlist.push(results.features[i].attributes[OIDFieldName]);
        }
        this.set("report_lotOIDs", objectidlist);
        this._infoRequest();
      }));
    },

    _infoPrintRequest: function () {
      /// <summary>
      /// Request printable report data from server.
      /// </summary>
      var url = this.handlerUrl; //this.webServiceUrl + "/getPlanInfoSummaryReport";
      var printType = (this.pdfRadioNode.checked ? "pdf" : "xls");
      if (!window.XDomainRequest) {
        var $request = {
          token: this.userToken, 
          mapId: 0,
          mapName: this.mapId,
          conservationCommitmentLayerIds: this.report_conservationLayers,
          objectIds: this.report_lotOIDs,
          reportType: printType
        };
			  $.support.cors=true;
			  wcfUrl = url;
			  var deferredXhr = $.ajax({
			    url: wcfUrl,
			    type: "POST",
			    data: $request,
			    success: lang.hitch(this, this._printComplete),
			    error: lang.hitch(this, this._printError)
			  });
			  this.set("deferredJQXhrRequest", deferredXhr);
      } else {
        this._xdr(url, {
          method: "POST",
          //data: JSON.stringify({
          data: ({
            token: this.userToken,
            mapId: 0,
            mapName: this.mapId,
            conservationCommitmentLayerIds: this.report_conservationLayers,
            objectIds: this.report_lotOIDs,
            reportType: printType
          })
        }).then(lang.hitch(this, function (response) {
          //var data = JSON.parse(response, true);
          this._printComplete(data); //data.d);
        }), lang.hitch(this, this._printError));
      }
    },

    _cancelRequest: function () {
      // Cancel any processing dojo XHR request
      if ((!this.deferredXhrRequest) == false) { // ensure request not null or undefined
        if (!this.deferredXhrRequest.isCanceled() && !this.deferredXhrRequest.isFulfilled()) { // ensure request not canceled or fulfilled already
          this.deferredXhrRequest.cancel("User interrupted the request by triggering a new request."); // cancel it
        }
      }
      // Abort any processing jQuery XHR request
      if ((!this.deferredJQXhrRequest) == false) { // ensure request not null or undefined
        this.deferredJQXhrRequest.abort("User interrupted the request by triggering a new request."); // abort it
      }
    },

    _infoRequest: function () {
      /// <summary>
      /// Request report data from server.
      /// </summary>
      var url = this.webServiceUrl + "/getPlanInfoByObjectId";
      var printType = (this.pdfRadioNode.checked ? "pdf" : "xls");
      if (!window.XDomainRequest) {
        //console.log("input data...");
        //console.log(JSON.stringify({ token: this.userToken, mapId: 0, mapName: this.mapId, conservationCommitmentLayerIds: this.report_conservationLayers, objectIds: this.report_lotOIDs, reportType: printType }));
        var deferredXhr = xhr.post(url, {
           handleAs: "json",
           data: JSON.stringify({
            token: this.userToken,
            mapId: 0,
            mapName: this.mapId,
            conservationCommitmentLayerIds: this.report_conservationLayers,
            objectIds: this.report_lotOIDs,
            reportType: printType
          }),
          headers: { 'Content-Type': 'application/json' }
        }).then(lang.hitch(this, function (data) {
          this._processReportResponse(data.d);
        }), lang.hitch(this, this._processReportResponseError));
        this.set("deferredXhrRequest", deferredXhr);
      } else {
        this._xdr(url, {
          method: "POST",
          data: JSON.stringify({
            token: this.userToken,
            mapId: 0,
            mapName: this.mapId,
            conservationCommitmentLayerIds: this.report_conservationLayers,
            objectIds: this.report_objectIds,
            reportType: printType
          })
        }).then(lang.hitch(this, function (response) {
          var data = JSON.parse(response, true);
          this._processReportResponse(data.d);
        }), lang.hitch(this, this._processReportResponseError));
      }
    },
	  
    _reformatReportResults: function (results) {
      /// <summary>
      /// Reformat report results so that objectIds are listed uniquely and multiple layer names are combined.
      /// Returns reformated array of object... [ { ObjectId, Lot, Plan, Section, LotDPSection, IntersectedLayers } ]
      /// </summary>
      var idx = -1;
      var layers = "";
      var listItem = {};
      var items = [];
      var uniqueIds = [];
      arrayUtils.forEach(results, function (obj) {
        if (uniqueIds.length > 0) {
          idx = uniqueIds.indexOf(obj.ObjectId);
          if (idx > -1) {
            // obj added already - combine layer name to intersected
            listItem = items[idx];
           layers = listItem.IntersectedLayers;
            listItem.IntersectedLayers = layers + "<br />" + obj.LayerName;
          } else {
            uniqueIds.push(obj.ObjectId);
            listItem = { ObjectId: obj.ObjectId, Lot: obj.LotNumber, Plan: obj.PlanNumber, Section: obj.SectionNumber, LotDPSection: obj.LotDPSection, IntersectedLayers: obj.LayerName };
            items.push(listItem);
          }
        } else { //first item
          uniqueIds.push(obj.ObjectId);
          listItem = { ObjectId: obj.ObjectId, Lot: obj.LotNumber, Plan: obj.PlanNumber, Section: obj.SectionNumber, LotDPSection: obj.LotDPSection, IntersectedLayers: obj.LayerName };
          items.push(listItem);
        }
      });
      return items;
    },

    _selectRow: function(id, OID) {
      var selRow = dom.byId(id);
      var sel = domAttr.get(selRow, "selected");
      query(".infoRow").attr({ selected: "false" });
      domAttr.set(selRow, "selected", (sel == "true" ? "false" : "true"));
      if (sel == "true") {
        this._clearSelectedFeature();
      } else {
        this._selectFeatureByOID(OID, false);
      }
    },

    _selectRowAndZoomTo: function(id, OID) {      
      var selRow = dom.byId(id);
      var sel = domAttr.get(selRow, "selected");
      query(".infoRow").attr({ selected: "false" });
      domAttr.set(selRow, "selected", "true");
      this._selectFeatureByOID(OID, true);
    }, 

    _selectFeatureByOID: function(OID, zoomSelected) {
      var OIDFieldName = this.lotSearchLayerConfig.OBJECTID;
      var queryTask = new QueryTask(this.lotSearchLayerConfig.MAPSERVICE_LAYER_URL);
      var query = new Query();
      query.spatialRelationship = Query.SPATIAL_REL_INTERSECTS;
      query.returnGeometry = true;
      query.outFields = [OIDFieldName];
      query.where = OIDFieldName + " = " + OID;
      queryTask.execute(query, lang.hitch(this, function(results) {
        var symbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
          new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
            new Color([0, 255, 255]), 2), new Color([0, 255, 255, 0.4]));
        var graphic = new Graphic(results.features[0].geometry, symbol)
        this._clearSelectedFeature();
        this.graphicsLayer.add(graphic); // add new graphics
        if (zoomSelected) {
          var fullExtent = graphic.geometry.getExtent();
          this.map.setExtent(fullExtent, true); 
        }
      }))
    },

    _clearSelectedFeature: function () {
      if (!this.graphicsLayer) {
        return;
      }
      this.graphicsLayer.clear(); // remove existing graphics
    },

    _processReportResponse: function (response) {
      /// <summary>
      /// Server request completed successfully. Present report to the user.
      /// <param name="response" type="Object">Report data response from server.</param>
      /// </summary>
      //console.log("_processReportResponse...");
      //console.log(response);
      var results = this._reformatReportResults(response);
      // Format the report response
      var fs;
      var row;
      var idx = 0;
      arrayUtils.forEach(results, function (obj) {
        idx += 1;
        fs = new Fieldset({ title: "+ / -", content: obj.IntersectedLayers });
        row = domConstruct.toDom("<tr class='infoRow' selected='false' id='" + this.baseClass + "_" + obj.ObjectId + "'>" +
          "<td width='30'><strong>" + idx + "</strong></td>" +
          "<td width='70'><a id='" + this.baseClass + "_select' href='#'>Select</a></td>" +
          "<td width='70'><a id='" + this.baseClass + "_zoomTo' href='#'>Zoom To</a></td>" +
          "<td width='70'>" + obj.Lot + "</td>" +
          "<td width='90'>" + obj.Plan + "</td>" +
          "<td width='90'>" + obj.Section + "</td>" +
          "<td width='55%' id='" + this.baseClass + "_fs_" + obj.ObjectId + "'></td>" +
          "</tr>");
        domConstruct.place(row, this.resultRows);
        dom.byId(this.baseClass + "_fs_" + obj.ObjectId).appendChild(fs.domNode);
        fs.startup();
        this.own(
          on(dom.byId(this.baseClass + "_" + obj.ObjectId), "click", lang.hitch(this, function (evt) {
            if (evt.target.id == this.baseClass + "_zoomTo") {
              this._selectRowAndZoomTo(this.baseClass + "_" + obj.ObjectId, obj.ObjectId);
            } else if (evt.target.id == this.baseClass + "_select") {
              this._selectRow(this.baseClass + "_" + obj.ObjectId, obj.ObjectId); 
            }
          }))
        );
      }, this);
      // show the report
      this.set("warningVisible", false);
      this.set("reportVisible", true);
      this._showNotBusy();
    },

    _printComplete: function (result) {
      /// <summary>
      /// Print report server request completed successfully. Present printed report to the user as a download link. Additionally open it in new window.
      /// <param name="result" type="base64 encoded string">PDF or XLS file streamed from server.</param>
      /// </summary>
      var cell;
      if (this.pdfRadioNode.checked) {
        domConstruct.empty(this.pdfNode);
        cell = domConstruct.toDom('<a href="data:Application/PDF;base64,' + result + '" download="PlanInfoReport.pdf">PDF</a>');
        domConstruct.place(cell, this.pdfNode);
        // Open in new window
        window.open("data:application/PDF;base64," + result, "", "width=800, height=750");
      } else {
        domConstruct.empty(this.xlsNode);
        var cell = domConstruct.toDom('<a id="infoReportWidget_xlsReportLink" href="data:Application/XLS;base64,' + result + '" download="PlanInfoReport.xls">Excel</a>');
        domConstruct.place(cell, this.xlsNode);
        // << DEV NOTE: This doesn't work for XLS - see alternative solution below, simulate link mouse click instead
        // Open in new window
        //window.open("data:application/XLS;base64," + result, "", "width=800, height=750"); 
        // -- WORKAROUND...
        // Simulate mouse click on 'a' link
        lang.hitch(this, setTimeout(this._actuateLink(document.getElementById("infoReportWidget_xlsReportLink")), 500));
        // >>
      }
      this.set("printResultsAvailable", true);
      this._showNotBusy();
    },

    _actuateLink: function(link) {
       var allowDefaultAction = true;
       if (link.click) {
          link.click();
          return;
       }
       else if (document.createEvent) {
          var e = document.createEvent('MouseEvents');
          e.initEvent(
             'click'     // event type
             ,true      // can bubble?
             ,true      // cancelable?
          );
          allowDefaultAction = link.dispatchEvent(e);           
       } 
       if (allowDefaultAction) {
          var f = document.createElement('form');
          f.action = link.href;
          document.body.appendChild(f);
          f.submit();
       }
    },

    _processReportResponseError: function (error) {
      /// <summary>
      /// Warn user: report request error
      /// </summary>
      console.log("ERROR :: " + this.baseClass + "_processReportResponseError...");
      console.log(error);
      var warningNode = dom.byId(this.baseClass + "_warningNode");
      domConstruct.empty(warningNode);
      var errString = domConstruct.toDom("<div class='warning'><strong><span class='printError'>Report error!</span></strong>" +
      "<div class='warningInfo'>An error occurred while requesting the info report from the server.</div></div>");
      domConstruct.place(errString, warningNode);
      this.set("warningVisible", true);
      this._showNotBusy();
    },

    _printError: function (error) {
      /// <summary>
      /// Print report request onError event error back. Report error to user.
      /// </summary>   
      console.log("ERROR :: " + this.baseClass + "_printError...");
      console.log(error);   
      var cell;
      if (this.pdfRadioNode.checked) {
        domConstruct.empty(this.pdfNode);
        cell = domConstruct.toDom('<span class="errorText">PDF...Print Error!</span>');
        domConstruct.place(cell, this.pdfNode);
      } else {
        domConstruct.empty(this.xlsNode);
        var cell = domConstruct.toDom('<span class="errorText">Excel...Print Error!</span>');
        domConstruct.place(cell, this.xlsNode);
      }
      this._showNotBusy();
    },
    
    _warnNoVisibleConservationLayers: function () {
      /// <summary>
      /// Warn user: no visible conservation commitment layers
      /// </summary>
      this.set("report_conservationLayers", []);
      var warningNode = dom.byId(this.baseClass + "_warningNode");
      domConstruct.empty(warningNode);
      var errString = domConstruct.toDom("<div class='warning'><strong><span class='printError'>No visible conservation commitment layers!</span></strong>" +
      "<div class='warningInfo'>This report requires at least one visible conservation commitment layer!</div></div>");
      domConstruct.place(errString, warningNode);
      this.set("warningVisible", true);
    },

    _warnInvalidSelection: function () {
      /// <summary>
      /// Warn user: invalid selection
      /// </summary>
      var warningNode = dom.byId(this.baseClass + "_warningNode");
      domConstruct.empty(warningNode);
      var errString = domConstruct.toDom("<div class='warning'><strong><span class='printError'>No valid selection!</span></strong>" +
        "<div class='warningInfo'>Please ensure at least one feature is selected using the Search or Select tool.</div></div>");
      domConstruct.place(errString, warningNode);
      this.set("warningVisible", true);
    },

    _checkVisibleConservationLayersChanged: function () {
      /// <summary>
      /// Check if visible conservation commitment layers has changed
      /// </summary>
      var visibleLayers = this._getVisibleConservationLayers();
      visibleLayers.sort();
      this.report_conservationLayers.sort();
      if (JSON.stringify(visibleLayers) == JSON.stringify(this.report_conservationLayers)) {
        return false; // conservation layer visibility hasn't changed
      } else {
        return true; // conservation layer visibility has changed
      }
    },

    _getVisibleConservationLayers: function () {
      /// <summary>
      /// Return visible conservation commitment layers.
      /// </summary>
      var visConservationLayers = [];
      arrayUtils.forEach(this.conservationCommitmentLayerIds, function (lyr) {
        if (this._isLayerVisible(lyr.MAPSERVICE_LAYER_URL)) {
          visConservationLayers.push(lyr.databaseLayerID);
        }
      }, this);
      return visConservationLayers;
    },

    _checkValidSelection: function (selectedGraphics) {
      /// <summary>
      /// Check there is a valid selection
      /// </summary>
      if (!selectedGraphics) {
        return false;
      }
      return (selectedGraphics.length > 0);
    },

    _isLayerVisible: function (url) {
      /// <summary>
      /// Return true if layer is visible in map, false if not visible.
      /// </summary>
      var lyrId = url.substring(url.lastIndexOf("/") + 1);
      var visLayers = this.map.getLayersVisibleAtScale();
      for (var i = 0; i < visLayers.length; i++) {
        if (!visLayers[i].url) { continue; } // graphics layers have null url
        if (url.indexOf(visLayers[i].url) > -1) {
          var lyrInfos = visLayers[i].layerInfos;
          for (var j = 0; j < lyrInfos.length; j++) {
            if (lyrInfos[j].id.toString() == lyrId) {
              if (lyrInfos[j].visible) { return true; }
            }
          }
        }
      }
      return false;
    },

    _showBusyReport: function () {
      /// <summary>
      /// Show busy animation on report panel
      /// </summary>
      this.busyNode.set("target", this.domNode.parentNode);
      this.busyNode.show();
    },
    _showBusyPrint: function () {
      /// <summary>
      /// Show busy animation on print panel
      /// </summary>
      this.busyNode.set("target", this.printHeaderNode);
      this.busyNode.show();
    },
    _showNotBusy: function () {
      /// <summary>
      /// Clear busy animation
      /// </summary>
      this.busyNode.hide();
    },

    _xdr: function (url, options) {
      /// <summary>
      /// IE9 CORS AJAX request using XDomainRequest.
      /// DEV NOTE: (KC) IE9 XHR CORS issue ...http://stackoverflow.com/questions/14587347/dojo-cross-domain-access-is-denied
      /// </summary>
      var def = new Deferred();
      var xdr = new XDomainRequest();
      if (xdr) {
        xdr.timeout = 30000;
        xdr.onload = function (e) {
          def.resolve(xdr.responseText);
        }
        xdr.onerror = function () {
          console.log("ERROR :: " + this.baseClass + " ... XDR Error");
        };
        xdr.ontimeout = function () {
          console.log("ERROR :: " + this.baseClass + "... XDR Timeout Error");
        };
        // bug fix: this must be set
        xdr.onprogress = function () {
          //console.log('progress');
        };
        xdr.open(options.method, url);
        xdr.send(options.data);
        return def;
      }
      def.reject(new Error("ERROR :: " + this.baseClass + "... XDomainRequest not supported."));
      return def;
    }

  })

  return Widget;

});
