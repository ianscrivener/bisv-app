/*
 | Author: NGIS 2014 (www.ngis.com.au) -> Kristy Carter
 | Module: MapScaleControlWidget
 | Description: Map Scale Control Widget - user control for manually adjusting the visible map scale.
 |    Provides user interface elements that allow the user to change the current map scale by selecting from the drop down list or entering any numeric scale.
 | Usage: See readme.
 |
 | CHANGE LOG:
 |   2015-03-04 Kristy Carter: Initial code release.
 */

define([
        "dojo/_base/declare",
        "dojo/Evented",
        "dojo/_base/lang",
        "dijit/_WidgetBase",
        "dijit/_TemplatedMixin",
        "dijit/_WidgetsInTemplateMixin",
        "dojo/dom-style",
        "dojo/on",
        "dojo/keys",
        "dojo/store/Memory",
        "dojo/_base/array",
        "dojo/text!./templates/mapScaleControlWidget.html",
        "dijit/form/Button",
        "dijit/form/ComboBox"
    ],
    function (declare,
              Evented,
              lang,
              _WidgetBase,
              _TemplatedMixin,
              _WidgetsInTemplateMixin,
              domStyle,
              on,
              keys,
              Memory,
              arrayUtils,
              template) {

        var Widget = declare("ngisWidget.MapScaleControlWidget", [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, Evented], {
            templateString: template,
            baseClass: "MapScaleControlWidget",

            // Widget option defaults (static)
            options: {
                map: null,
                scaleOptions: [500000, 250000, 100000, 50000, 25000, 10000, 5000, 2500, 1000, 500],
                visible: true
            },

            constructor: function (options, srcRefNode) {
                /// <summary>
                /// Widget constructor.
                /// <param name="options" type="Object">
                ///   Hash of initialisation options. The hash can contain any of the widget's properties, excluding read-only properties.
                /// </param>
                /// <param name="srcRefNode" type="DOMNode|String">
                ///   HTML source node reference.
                /// </param>
                /// </summary>
                this.domNode = srcRefNode; // widget node
                var defaults = lang.mixin({}, this.options, options); // mix in param options with defaults
                // Properties
                this.set("loaded", false);
                this.set("map", defaults.map);
                this.set("scaleOptions", defaults.scaleOptions);
                this.set("visible", defaults.visible);
                // Listeners
                this.watch("visible", this._visible);
            },

            postCreate: function () {
                /// <summary>
                /// Widget initialisations: populates scale select options and sets widget visibility based on user specified options.
                /// </summary>
                this.inherited(arguments);
                // Populate comboBox option list with formatted values
                var formattedScales = [];
                var formattedScale = "";
                arrayUtils.forEach(this.scaleOptions, function (scale) {
                    // If numeric then format
                    if (!isNaN(parseFloat(scale) && isFinite(scale))) {
                        formattedScale = scale.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    } else {
                        formattedScale = scale;
                    }
                    formattedScales.push({value: formattedScale});
                });
                var scaleStore = new Memory({
                    data: formattedScales
                });
                this.mapScaleInputNode.set("store", scaleStore);
                this.mapScaleInputNode.set("searchAttr", "value");
                // Show or hide the widget
                this._visible();
            },


            startup: function () {
                /// <summary>
                /// Start widget - called by user.
                /// </summary>
                // Ensure map was specified
                if (!this.map) {
                    this.destroy();
                    console.log('MapScaleControlWidget::map required before startup!');
                }
                // Ensure map is loaded before initialising.
                if (this.map.loaded) {
                    this._init();
                } else {
                    on.once(this.map, "load", lang.hitch(this, function () {
                        this._init();
                    }));
                }
            },

            destroy: function () {
                /// <summary>
                /// Connections/subscriptions will be cleaned up during the destroy() lifecycle phase.
                /// </summary>
                this.inherited(arguments);
            },

            /* ---------------- */
            /*   Public Events  */
            /* ---------------- */
            // load

            /* ---------------- */
            /* Public Functions */
            /* ---------------- */

            show: function () {
                /// <summary>
                /// Show widget
                /// </summary>
                this.set("visible", true);
            },

            hide: function () {
                /// <summary>
                /// Hide widget
                /// </summary>
                this.set("visible", false);
            },

            /* ----------------- */
            /* Private Functions */
            /* ----------------- */

            _init: function () {
                /// <summary>
                /// Map dependant widget initialisations.
                /// </summary>
                // Initialise scale input to match current map scale
                this._updateMapScaleControl();
                // Ensure that event handlers are unregistered when the widget is destroyed
                this.own(
                    on(this.map, "update-end", lang.hitch(this, function () {
                        this._updateMapScaleControl();
                    }))
                );
                // Widget is now loaded - emit the loaded event
                this.set("loaded", true);
                this.emit("load", {});
            },

            _updateMapScaleControl: function () {
                /// <summary>
                /// Update the scale text using the current map scale.
                /// </summary>
                // Force map scale to real number and add thousand commas
                var mapScale = Math.round(this.map.getScale()).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                this.mapScaleInputNode.set("value", mapScale);
            },

            _mapScaleUserUpdate: function () {
                /// <summary>
                /// Update the current map scale to match the user specified value.
                /// </summary>
                // Get user specified scale
                var userScale = Number(this.mapScaleInputNode.get("value").replace(/,/g, ""));
                if (!isNaN(parseFloat(userScale) && isFinite(userScale))) {
                    // User specified scale is not the same as current map scale
                    if (Math.round(this.map.getScale()) !== userScale) {
                        // Set map scale
                        this.map.setScale(userScale);
                    }
                }
            },

            _mapScaleUserKeypress: function (e) {
                /// <summary>
                /// onKeyPress event for the widget's combobox.
                /// - Update the current map scale to match the user specified value when user presses Enter.
                /// - Reset the scale text to match the current map scale when user presses ESC.
                /// <param name="e" type="undefined">
                ///   Key event
                /// </param>
                /// </summary>
                if (e.keyCode == keys.ENTER) {
                    this._mapScaleUserUpdate(); // update map scale when user hits enter
                } else if (e.keyCode == keys.ESCAPE) {
                    this._updateMapScaleControl();  // reset scale input to current map scale when user hits ESC
                }
            },

            _visible: function () {
                /// <summary>
                /// Show or hide the widget.
                /// </summary>
                if (this.get("visible")) {
                    domStyle.set(this.domNode, 'display', 'block');
                } else {
                    domStyle.set(this.domNode, 'display', 'none');
                }
            }

        });

        return Widget;

    });