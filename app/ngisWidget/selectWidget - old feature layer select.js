/*
 | Author: NGIS 2015 (www.ngis.com.au) -> Kristy Carter
 | Module: SelectWidget
 | Description: Select Widget - user interface controls for selecting features from layers in the map.
 | Usage: See readme.
 |
 | CHANGE LOG:
 |   2015-04-20 Kristy Carter: Initial code release.
 */
 
define([
	"dojo/_base/declare", "dojo/dom", "dojo/_base/lang", "dojo/_base/array", "dojo/html",
  "dijit/_WidgetBase", "dijit/_TemplatedMixin", "dijit/_WidgetsInTemplateMixin", 
  "dojo/text!./templates/selectWidget.html", "dojo/dom-class", "dojo/on", 
  "esri/map", "esri/toolbars/draw", "esri/tasks/query", "esri/graphic", "esri/layers/FeatureLayer", 
  "esri/Color", "esri/tasks/GeometryService", "esri/tasks/BufferParameters",
  "esri/symbols/SimpleMarkerSymbol", "esri/symbols/SimpleLineSymbol", "esri/symbols/SimpleFillSymbol",
  "dijit/MenuBarItem", "dijit/form/NumberSpinner", "dijit/form/Select", "dijit/form/Button", "dojox/widget/Standby"
],
function(declare, dom, lang, arrayUtils, html,
  _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin,
  template, domClass, on,
  Map, Draw, Query, Graphic, FeatureLayer,
  Color, GeometryService, BufferParameters,
  SimpleMarkerSymbol, SimpleLineSymbol, SimpleFillSymbol
) {
	var Widget = declare("ngisWidget.SelectWidget", [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
		templateString: template,
		baseClass: "SelectWidget",
    
    graphicCount : -2,
    selectionLayerCount : 0,
    
    // Widget option defaults (static)
		options: {
      map: null
    },
    
    constructor: function(options, srcRefNode) {
      /// <summary>
      /// Widget constructor.
      /// <param name="options" type="Object">
      ///   Hash of initialisation options. The hash can contain any of the widget's properties, excluding read-only properties.
      /// </param>
      /// <param name="srcRefNode" type="DOMNode|String">
      ///   HTML source node reference.
      /// </param>
      /// </summary>
      this.domNode = srcRefNode; // widget node
      var defaults = lang.mixin({}, this.options, options); // mix in param options with defaults
      // Properties
      this.set("loaded", false);
      this.set("map", defaults.map);
      this.set("geomService", defaults.geomService);
      this.set("selectableLayers", defaults.selectableLayers);  // { id: <layer id>, name: <layer name> } 
      this.set("shiftDown", false);
      this.set("selectionFeatureLayers", []);
    },
    
    postCreate: function() {
      /// <summary>
      /// General widget initialisations.
      /// </summary>
      this.inherited(arguments);
      this.busyNode.set("target", this.domNode.parentNode);
      this.own(
        on(this.clearSelectedNode, "click", lang.hitch(this, this._clearSelectedClick)),
        // Hook-up tool click event to toolbar div (note: intercepts child tool clicks)
        on(this.toolbarNode, "click", lang.hitch(this, this._selectToolClick))
      )
    },
    
    startup: function() {
      /// <summary>
      /// Start widget - called by user.
      /// </summary>
      this.inherited(arguments);
      // Ensure map was specified
      if (!this.map) {
        this.destroy();
        console.log(this.baseClass+'::map required before startup!');
      }
      // Ensure map is loaded before initialising
      if (this.map.loaded) {
        this._init();
      } else {
        on.once(this.map, "load", lang.hitch(this, function() {
          this._init();
        }));
      }
    },
    
    destroy: function() {
      /// <summary>
      /// Connections/subscriptions will be cleaned up during the destroy() lifecycle phase.
      /// </summary>
      this.inherited(arguments);
      this.featureLayers.length = 0;
      this.geomService = null;
      this.drawToolbar = null;
      this.map = null;
    },
    
    /* ---------------- */
    /* Public Functions */
    /* ---------------- */
    
    activateSelectWidget: function() {
      this.set("active", true);
      // Select the default select tool
      this.toolRectangleNode.domNode.click();
    },
    
    deactivateSelectWidget: function() {
      this.deactivateSelectTools();
      this.set("active", false);
    },
    
    deactivateSelectTools: function() {
      /// <summary>
      /// Deactivate the select tools and also the map graphics draw functions.
      /// </summary>
      // Deactivate the map graphics draw function
      this.drawToolbar.deactivate();
      // Enable map nav and identify
      this.map.enableMapNavigation();
      this.map.setInfoWindowOnClick(true);
      // Unselect all tools
      this.set("selectedToolDesc", "");
      domClass.remove(this.toolRectangleNode.domNode, "selected");
      domClass.remove(this.toolPointNode.domNode, "selected");
      domClass.remove(this.toolPolygonNode.domNode, "selected");
      domClass.remove(this.toolFreehandPolygonNode.domNode, "selected");
      domClass.remove(this.toolCircleNode.domNode, "selected");
      domClass.remove(this.toolBufferNode.domNode, "selected");
      domClass.remove(this.toolExtentNode.domNode, "selected");
    },
    
    /* ----------------- */
    /* Private Functions */
    /* ----------------- */
    
    _init: function() {
      /// <summary>
      /// Map dependant widget initialisations.
      /// </summary>
      this.set("drawToolbar", new Draw(this.map));
      this.set("featureLayers", arrayUtils.map(this.selectableLayers, lang.hitch(this, function(lyr) { 
        return { id: lyr.id, name: lyr.name, selectedFeatureCount: 0, layer: this.map.getLayer(lyr.id) };
      })) ); 
      arrayUtils.forEach(this.featureLayers, lang.hitch(this, function(lyr) {
        this._setupSelectionListener(lyr);
      }));
      this.own(
        on(this.map, "mouse-down", lang.hitch(this, this._drawBufferCircle)),
        on(this.drawToolbar, "DrawEnd", lang.hitch(this, this._processAOIDraw)),
        on(this.map, "key-down", lang.hitch(this, function(evt) {
          if (evt.shiftKey) {
            this.set("shiftDown", true);
          }
        })),
        on(this.map, "key-up", lang.hitch(this, function(evt) {
          if (!evt.shiftKey) {
            this.set("shiftDown", false);
          }
        }))
      );
      this._forceGraphicsLayerVisible();
      // Load options into layer lookup
      this._addLayerOptions();
      // Set selection symbol for selectable layers
      arrayUtils.forEach(this.featureLayers, lang.hitch(this, function(lyr) {
        switch (lyr.layer.geometryType) {
        case "esriGeometryPoint":
          lyr.layer.setSelectionSymbol(this._getSelectionPointSymbol());
          break;
        case "esriGeometryPolygon":
          lyr.layer.setSelectionSymbol(this._getSelectionPolygonSymbol());
          break;
        case "esriGeometryPolyline":
          lyr.layer.setSelectionSymbol(this._getSelectionPolylineSymbol());
        }
      }));
      // Widget is now loaded - emit the loaded event
      this.set("loaded", true);
      this.emit("load", {});
    },
    
    _setupSelectionListener: function(lyr) {
      this.own(
        on(lyr.layer, "selection-complete", lang.hitch(this, function(results) { this._tidyupAfterSelection(lyr); } ))
      );
    },
    
    _forceGraphicsLayerVisible: function() {
      // graphics layer must be visible
      if (this.map.graphics.visible == false) {
        this.map.graphics.setVisibility(true);
      }
    },
    
    _addLayerOptions: function() {
      // Load options into layer lookup
      var optionList;
      optionList = arrayUtils.map(this.featureLayers, function(lyr) {
        return { value: lyr.id, label: lyr.name };
      }, this);
      optionList.unshift({value: "ALL", label: "All Visible Layers" });
      this.layerNode.set("options", optionList);
      this.layerNode.set("value", "ALL");
    },
    
    _selectToolClick: function(e) {
      /// <summary>
      /// Activate or deactivate map graphics draw depending if the clicked tool is also selected.
      /// <param name="e" type="event object">dijit/MenuBarItem.onClick event object. Contains clicked DOM node.</param>
      /// </summary>
      if (e.target.id === "toolbar") { return; }
      // Deselect all tools in the toolbar except for the clicked tool which should be toggled.
      var clickedNode = e.target;
      domClass.toggle(clickedNode, "selected");
      var selected = domClass.contains(clickedNode, "selected");
      this.deactivateSelectTools();
      if (selected) {
        this._forceGraphicsLayerVisible();
        console.log(e.target.title);
        this.set("selectedToolDesc", e.target.title);
        var drawFunct = this._getDrawFunction();
        domClass.add(clickedNode, "selected");
        // Disable radius if buffer not selected
        var bufferEnabled = (this.selectedToolDesc.toUpperCase().indexOf("BUFFER") === -1);
        this.radiusUnitNode.set("disabled", bufferEnabled);
        this.radiusNode.set("disabled", bufferEnabled);
        if (this.selectedToolDesc.toUpperCase().indexOf("EXTENT") !== -1) {
          // No need to use the draw toolbar to define AOI - AOI is the current map extent
          this._selectByMapExtent();
          domClass.remove(clickedNode, "selected");
        } else {
          // Activate draw toolbar and disable map navigation
          if (drawFunct) {
            this.drawToolbar.activate(drawFunct);
          }
          this.map.disableMapNavigation();
          this.map.setInfoWindowOnClick(false);
        }
      }
    },
    
    _getSelectedFeatureLayerIDs: function() {
      var selectedVal = this.layerNode.get("value");
      var lyrs = [];
      if (selectedVal == "ALL") {
        lyrs = arrayUtils.map(this.featureLayers, function(lyr) {
          return lyr.id;
        });
      } else {
        lyrs.push(selectedVal);
      }
      return lyrs;
    },
    
    _drawBufferCircle:  function(evt) {
      if (this.active) {
        if (this.selectedToolDesc) { 
          if (this.selectedToolDesc.toUpperCase().indexOf("BUFFER") !== -1) {
            this._bufferPoint(evt.mapPoint);
          }
        }
      }
    },
    
    _processAOIDraw: function(geometry) {
      if (this.active) {
        if (this.selectedToolDesc) { 
          if (this.selectedToolDesc.toUpperCase().indexOf("BUFFER") === -1) {
            this._selectByDrawAOI(geometry);
          }
        }
      }        
    },
    
    _bufferPoint: function(pt) {
      var params = new BufferParameters();
      params.geometries = [ pt ];
      // Buffer in linear units: meters / km
      params.distances = [this.radiusNode.get("value")];
      switch (this.radiusUnitNode.get("value")) {
      case "metre":
        params.unit = GeometryService.UNIT_METER;
        break;
      case "kilometre":
        params.unit = GeometryService.UNIT_KILOMETER;
      }
      params.geodesic = true;
      this.geomService.buffer(params, lang.hitch(this, this._returnBuffer), this._bufferError);
    },
    
    _bufferError: function(err) {
      console.log(err);
    },
    
    _updateFeatureLayerCount: function(lyr) {
      lyr.selectedFeatureCount = lyr.layer.getSelectedFeatures().length;
      this._showResults(false);
    },
    
    _tidyupAfterSelection: function(lyr) {
      this._updateFeatureLayerCount(lyr);
      this.selectionLayerCount--;
      if (this.selectionLayerCount == 0) {
        this.busyNode.hide();
      }
      if (this.bufferGraphic) {
        this.map.graphics.remove(this.bufferGraphic);
        this.set("bufferGraphic", null);
      }
    },
    
    _returnBuffer: function(bufferedGeometries) {
      arrayUtils.forEach(bufferedGeometries, lang.hitch(this, function(geometry) {
        var graphic = new Graphic(geometry, this._getBufferSymbol())
        this.set("bufferGraphic", graphic);
        this.map.graphics.add(graphic);
        this._selectByDrawAOI(geometry);
      }));
    },
    
    _getLyr: function(id) {
      var foundLyr = null;
      arrayUtils.forEach(this.featureLayers, function(lyr) {
        if (id == lyr.id) {
          foundLyr = lyr;
        }
      });
      return foundLyr;
    },
    
    _selectByDrawAOI: function(geometry) {
      var layerIDs = this._getSelectedFeatureLayerIDs();
      this.set("selectionLayerCount", layerIDs.length);
      this.busyNode.show();
      var selectQuery = new Query();
      selectQuery.geometry = geometry;
      this.set("selectionFeatureLayers", []);
      if (!this.shiftDown) { this._clearSelectedClick(); }
      arrayUtils.forEach(layerIDs, lang.hitch(this, function(lyrID) {
        var lyr = this._getLyr(lyrID);
        var fLayer = lyr.layer;
        this.selectionFeatureLayers.push(fLayer);
        fLayer.selectFeatures(selectQuery, 
          (this.shiftDown ? FeatureLayer.SELECTION_ADD : FeatureLayer.SELECTION_NEW));
      }));
    },
    
    _selectByBuffer: function() {
      var geometry = this.map.extent;
      this._selectByDrawAOI(geometry);
    },
    
    _selectByMapExtent: function() {
      var geometry = this.map.extent;
      this._selectByDrawAOI(geometry);
    },
    
    _drawGraphic: function(evt) {
      /// <summary> 
      /// Clear all graphics from graphics layer
      /// <param name="evt" type="event object">drawToolbar.draw-end event object. Contains new Geometry</param>
      /// </summary>
      if ( evt !== undefined) {
        var symbol;
        // create appropriate symbol for the graphic type
        switch (evt.geometry.type) {
          case "point":
            symbol = this._getCurrentPointSymbol();
            break;
          default:
            symbol = this._getCurrentPolySymbol();
            break;
        }
        var graphic = new Graphic(evt.geometry, symbol);
        this.map.graphics.add(graphic);
      }
    },
  
    _clearSelectedClick: function(evt) {
      /// <summary>
      /// Clear all selection graphics from graphics layer
      /// </summary>
      this.busyNode.show();
      arrayUtils.forEach(this.featureLayers, lang.hitch(this, function(lyr) {
        lyr.layer.clearSelection();
        this._updateFeatureLayerCount(lyr);
      }));
      this.selectionFeatureLayers.length = 0;
      this._showResults(true);
      this.busyNode.hide();
    },
    
    _showResults: function(cleared) {
      var resultText = "";
      if (!cleared) {
        arrayUtils.forEach(this.featureLayers, lang.hitch(this, function(lyr) {
          resultText += "<tr><td><i><small>(" + lyr.name + ": " + lyr.selectedFeatureCount + " selected)</small></i></td></tr>";
        }));
      }
      html.set(this.resultsNode, resultText);
    },
    
    _getDrawFunction: function() {
      var draw = this.selectedToolDesc.toUpperCase().replace("SELECT BY ", "");
      switch (draw) {
        case "CLICK":
          return Draw.POINT;
        case "FREEHAND POLYGON":
          return Draw.FREEHAND_POLYGON;
        case "POLYGON":
          return Draw.POLYGON;
        case "RECTANGLE":
          return Draw.RECTANGLE;
        case "CIRCLE":
          return Draw.CIRCLE;
        default:
          return null;
      }
    },
    
    _toolIsSelected: function() {
      return (domClass.contains(this.toolRectangleNode.domNode, "selected") ||
        domClass.contains(this.toolPointNode.domNode, "selected") ||
        domClass.contains(this.toolPolygonNode.domNode, "selected") ||
        domClass.contains(this.toolFreehandPolygonNode.domNode, "selected") ||
        domClass.contains(this.toolCircleNode.domNode, "selected") ||
        domClass.contains(this.toolBufferNode.domNode, "selected") ||
        domClass.contains(this.toolExtentNode.domNode, "selected")) ? true : false;
    },
    
    _getSelectionPointSymbol: function() {
      /// <summary> 
      /// Compile selection point symbol
      /// </summary>
      return new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 10,
             new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 0, 0], 2),
             new Color([255, 255, 0, 0.5])));
    },
    
    _getSelectionPolygonSymbol: function() {
      /// <summary> 
      /// Compile selection polygon symbol
      /// </summary>
      return new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
             new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
             new Color([255, 0, 0]), 2), new Color([255, 255, 0, 0.5]));
    },
    
    _getSelectionPolylineSymbol: function() {
      /// <summary> 
      /// Compile selection line symbol
      /// </summary>
      return new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 0, 0]), 2);
    },
    
    _getBufferSymbol: function() {
      /// <summary> 
      /// Compile selection buffer symbol
      /// </summary>
      return new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
             new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
             new Color([255, 0, 0]), 2), new Color([0, 0, 0, 0.3]));
    }
    
	})

  return Widget;
  
});