﻿/*
 | Author: NGIS 2015 (www.ngis.com.au) -> Kristy Carter
 | Module: PrintWidget
 | Description: Print Widget - user interface controls for printing maps and reports.
 | Usage: See readme.
 |
 | CHANGE LOG:
 |   2015-06-3 Kristy Carter: Initial code release.
 */

define([
	"dojo/_base/declare", "dojo/Evented", "dojo/_base/array", "dojo/store/Memory",
  "dojo/dom", "dojo/dom-construct", "dojo/dom-class", "dojo/on", "dojo/html", "dojo/_base/lang",
  "dijit/_WidgetBase", "dijit/_TemplatedMixin", "dijit/_WidgetsInTemplateMixin", "dojo/text!./templates/printWidget.html", 
  "esri/map", "esri/tasks/PrintTask", "esri/tasks/PrintParameters", "esri/tasks/PrintTemplate",
  "dijit/Dialog", "dijit/form/Select", "dijit/form/TextBox", "dijit/form/Button", "dijit/layout/ContentPane"
],
function (declare, Evented, arrayUtils, Memory,
  dom, domConstruct, domClass, on, html, lang,
  _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, template,
  Map, PrintTask, PrintParameters, PrintTemplate, Dialog
) {
  var Widget = declare("ngisWidget.PrintWidget", [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, Evented], {
    templateString: template,
    baseClass: "PrintWidget",

    loadingGif: "//ajax.googleapis.com/ajax/libs/dojo/1.9.5/dijit/themes/claro/images/loadingAnimation.gif",

    // Custom setters
    mapPrintGoDisabled: false,
    _setMapPrintGoDisabledAttr: function (/*Boolean*/ value) { // attach to DOM node style attribute
      this._set("mapPrintGoDisabled", value);
      this.mapPrintGoNode.set("disabled", value);
    },
    titleMaxChar: 100,

    // Widget option defaults (static)
    options: {
      map: null,
      printTimeout: 180000,
      titleMaxChar: 100,
      mapTemplates: [],
      printMapServiceUrl: "",
      printMapTitleDefault: ""
    },

    constructor: function (options, srcRefNode) {
      /// <summary>
      /// Widget constructor.
      /// <param name="options" type="Object">
      ///   Hash of initialisation options. The hash can contain any of the widget's properties, excluding read-only properties.
      /// </param>
      /// <param name="srcRefNode" type="DOMNode|String">
      ///   HTML source node reference.
      /// </param>
      /// </summary>
      this.domNode = srcRefNode; // widget node
      var defaults = lang.mixin({}, this.options, options); // mix in param options with defaults
      // Properties
      this.set("loaded", false);
      this.set("map", defaults.map);
      this.set("mapTemplates", defaults.mapTemplates);
      this.set("mapServiceConnectionFile", defaults.mapServiceConnectionFile);
      this.set("mapLegend", defaults.mapLegend);
      this.set("printMapServiceUrl", defaults.printMapServiceUrl);
      this.set("printTimeout", defaults.printTimeout);
      this.set("titleMaxChar", defaults.titleMaxChar);
      this.set("printMapTitleDefault", defaults.printMapTitleDefault);
      this.set("printCount", 0);
    },

    postCreate: function () {
      /// <summary>
      /// General widget initialisations.
      /// </summary>
      this.inherited(arguments);
      this.own(
        on(this.mapPrintGoNode, "click", lang.hitch(this, this.MapPrintGo))
      )
      this.mapTitleInputNode.set("maxlength", this.titleMaxChar);
      this._loadMapTemplates();
    },

    startup: function () {
      /// <summary>
      /// Start widget - called by user.
      /// </summary>
      this.inherited(arguments);
      // Ensure map was specified
      if (!this.map) {
        this.destroy();
        console.log(this.baseClass + '::map required before startup!');
      }
      // Ensure map is loaded before initialising
      if (this.map.loaded) {
        this._init();
      } else {
        on.once(this.map, "load", lang.hitch(this, function () {
          this._init();
        }));
      }
    },

    destroy: function () {
      /// <summary>
      /// Connections/subscriptions will be cleaned up during the destroy() lifecycle phase.
      /// </summary>
      this.inherited(arguments);
      this.map = null;
    },

    /* ---------------- */
    /* Public Functions */
    /* ---------------- */

    MapPrintGo: function (evt) {
      /// <summary>
      /// DOM event handler for the print button. Requests a map print document from the ArcGIS REST print service.
      ///
      /// Important: When setting up the geoprocessing tool, the Web_Map_as_JSON, Format, Layout_Template, and 
      ///   Output_File parameter names must be spelled exactly as shown so they match the tool signature of the 
      ///   Print Task in the ArcGIS web APIs. (FMI see http://resources.arcgis.com/en/help/main/10.2/index.html#//0057000000mq000000)
      /// </summary>
      var title = this.mapTitleInputNode.get("value");
      if (title == "") {
        title = this.printMapTitleDefault;
      }
      var printFormat = this.printFormatNode.get("value");
      var selectedIdx = this.mapTemplateNode.get("value");
      var printLayoutPath = this.mapTemplates[selectedIdx].Path;
      var printLayoutName = this.mapTemplates[selectedIdx].LayoutName;
      var printLegendPath = this.mapLegend.replace(/\\/g, "/");
      //var printLegendPath = this.mapTemplates[selectedIdx].LegendPath.replace(/\\/g, "/"); // Doesn't use this one anymore
      var printParams = new PrintParameters();
      printParams.map = this.map;
      printParams.outSpatialReference = this.map.spatialReference;
      var printTemplate = new PrintTemplate();
      printTemplate.layout = printLayoutPath;
      printTemplate.format = printFormat;
      printTemplate.preserveScale = true;
      printParams.template = printTemplate;
      printParams.extraParameters = { Title: title, ServerConnectionFile: this.mapServiceConnectionFile };
      this.printCount++;
      var printRefNum = this.printCount;
      // << DEV NOTE ~ the onError callback seems to only work for REST errors (like invalid URL), 
      //  not for errors thrown in the geoprocessing service. Using setTimeout instead.
      //printTask.on("error", printError);
      this.printTask.execute(printParams,
        lang.hitch(this, function (results) { this._printMapComplete(results, printLegendPath, printLayoutName, printRefNum); }),
        lang.hitch(this, function (err) { this._printMapError(err, printLayoutName, printRefNum); }));
      // DEV NOTE >>
      this.set("printTaskReturnedFromServer", setTimeout(lang.hitch(this, function () { this._printMapTimeout(printLayoutName, printRefNum); }), this.printTimeout));
      this.mapPrintGoDisabled = true;
      var row = domConstruct.toDom("<tr id='PrintMap_" + printRefNum + "'><td class='narrowColumn'><b>" + printRefNum + "</b></td>" +
        "<td>" + printLayoutName + "&nbsp;&nbsp;...printing&nbsp;</td>" +
        "<td  class='narrowColumn'><img src=" + this.loadingGif + "></img></td></tr>");
      domConstruct.place(row, this.mapPrintResultsTableNode);
    },

    /* ----------------- */
    /* Private Functions */
    /* ----------------- */

    _init: function () {
      /// <summary>
      /// Map dependant widget initialisations.
      /// </summary>\
      this._addPrintMapService();
      // Widget is now loaded - emit the loaded event
      this.set("loaded", true);
      this.emit("load", {});
    },

    _addPrintMapService: function() {
      /// <summary>
      /// Initialise the print map geoprocessing service.
      /// </summary>
      this.set("printTask", new PrintTask(this.printMapServiceUrl, { async: true }));
    },

    _loadMapTemplates: function() {
      var idx = -1;
      var optionList = [];
      optionList = arrayUtils.map(this.mapTemplates, function (template) {
        idx++;
        return { value: idx, label: template.LayoutName };
      }, this);
      this.mapTemplateNode.set("options", optionList);
      this.mapTemplateNode.set("value", 0);
    },
    
    _printMapComplete: function(result, printLegendPath, printLayoutName, printRefNum) {
      /// <summary>
      /// PrintTask.execute onComplete event callback. Present the completed printed map output to the user as a link.
      /// <param name="result" type="string">URL to the printed source.</param>
      /// </summary>
      clearTimeout(this.printTaskReturnedFromServer);
      this.mapPrintGoDisabled = false;
      var latestPrintResultNode = dom.byId("PrintMap_" + printRefNum);
      // << DEV NOTE ~ IE9 objects to setting node.innerHTML on tables. Using html.set() instead.
      html.set(latestPrintResultNode, "<td class='narrowColumn'><b>" + printRefNum + "</b></td>" +
        "<td>pg1:<a target='_blank' href='" + result.url + "'>" + printLayoutName + "</a></td>" +
        "<td class='narrowColumn'>pg2:<a target='_blank' href='" + printLegendPath + "'>Legend</a></td>");
      // DEV NOTE >>
    },

    _printMapError: function(err, printLayoutName, printRefNum) {
      /// <summary>
      /// PrintTask.execute onError event error back. Display failed print error to user.
      /// </summary>
      console.log("ERROR :: printWidget.PrintMapError...");
      console.log(err);
      clearTimeout(this.printTaskReturnedFromServer);
      this.mapPrintGoDisabled = false;
      var latestPrintResultNode = dom.byId("PrintMap_" + printRefNum);
      domConstruct.empty(latestPrintResultNode);
      html.set(latestPrintResultNode, "<td class='narrowColumn'><b>" + printRefNum + "</b></td>" +
        "<td><span class='printError'>" + printLayoutName + "</span></td>" +
        "<td class='narrowColumn'></td>");
      this._showError("Print Map Error", "Failed to print the map!");
    },

    _printMapTimeout: function (printLayoutName, printRefNum) {
      /// <summary>
      /// Display print timeout error to user.
      /// </summary>
      this.mapPrintGoDisabled = false;
      var latestPrintResultNode = dom.byId("PrintMap_" + printRefNum);
      domConstruct.empty(latestPrintResultNode);
      html.set(latestPrintResultNode, "<td class='narrowColumn'><b>" + printRefNum + "</b></td>" +
        "<td><span class='printError'>" + printLayoutName + "</span></td>" +
        "<td class='narrowColumn'></td>");
      this._showError("Print Map Error", "Failed to print map due to timeout.")
    },

    _showError: function(title, message) {
      /// <summary>
      /// Displays an error message in a dialog box to the user.
      /// <param name="title" type="string">Title is shown in the dialog header after "! ERROR ! - ".</param>
      /// <param name="message" type="string">Error message to display in the content section of the dialog.</param>
      /// </summary>
      var errDialog = new Dialog({
        title: "<b style='color:red'>! ERROR !  "+title+"</b>",
        content: message,
        style: "width: 500px"
      });
      errDialog.show();
    }

  })

  return Widget;

});