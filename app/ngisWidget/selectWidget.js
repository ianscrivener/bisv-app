/*
 | Author: NGIS 2015 (www.ngis.com.au) -> Kristy Carter
 | Module: SelectWidget
 | Description: Select Widget - user interface controls for selecting features from layers in the map.
 | Usage: See readme.
 |
 | CHANGE LOG:
 |   2015-05-14 Kristy Carter: Initial code release.
 */
 
define([
	"dojo/_base/declare", "dojo/Evented", "dojo/dom", "dojo/_base/lang", "dojo/_base/array", "dojo/html",
  "dijit/_WidgetBase", "dijit/_TemplatedMixin", "dijit/_WidgetsInTemplateMixin", 
  "dojo/text!./templates/selectWidget.html", "dojo/dom-class", "dojo/on",	"dojo/json",
  "esri/tasks/query", "esri/tasks/QueryTask", "esri/tasks/GeometryService", "esri/tasks/BufferParameters",
  "esri/map", "esri/graphic", "esri/layers/GraphicsLayer", "esri/geometry/Extent", "esri/geometry/Multipoint",
  "esri/Color", "esri/toolbars/draw", "esri/symbols/SimpleMarkerSymbol", "esri/symbols/SimpleLineSymbol", "esri/symbols/SimpleFillSymbol",
  "dijit/Dialog", "dijit/MenuBarItem", "dijit/form/NumberSpinner", "dijit/form/Select", "dijit/form/Button", "dojox/widget/Standby"
],
function (declare, Evented, dom, lang, arrayUtils, html,
  _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin,
  template, domClass, on, JSON,
  Query, QueryTask, GeometryService, BufferParameters,
  Map, Graphic, GraphicsLayer, Extent, Multipoint,
  Color, Draw, SimpleMarkerSymbol, SimpleLineSymbol, SimpleFillSymbol,
  Dialog
) {
  var Widget = declare("ngisWidget.SelectWidget", [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, Evented], {
    templateString: template,
    baseClass: "SelectWidget",
    
    graphicCount: -2,
    bufferGraphicLayer: null,
    selectedFeatureGraphicLayer: null,
    shiftDown: false,
    selectedObjectIds: [],
    executeCount: 0,
    returnedExecuteCount: 0,

    // Custom setters
    noSelection: true,
    _setNoSelectionAttr: function (/*Boolean*/ value) { // attach to DOM node style attribute
      this._set("noSelection", value);
      this.clearSelectedNode.set("disabled", value);
      this.zoomToNode.set("disabled", value);
    },

    // Widget option defaults (static)
    options: {
      map: null
    },
    
    constructor: function(options, srcRefNode) {
      /// <summary>
      /// Widget constructor.
      /// <param name="options" type="Object">
      ///   Hash of initialisation options. The hash can contain any of the widget's properties, excluding read-only properties.
      /// </param>
      /// <param name="srcRefNode" type="DOMNode|String">
      ///   HTML source node reference.
      /// </param>
      /// </summary>
      this.domNode = srcRefNode; // widget node
      var defaults = lang.mixin({}, this.options, options); // mix in param options with defaults
      // Properties
      this.set("loaded", false);
      this.set("map", defaults.map);
      this.set("geomService", defaults.geomService);
      this.set("graphicsLayer", defaults.graphicsLayer);
      this.set("selectConfig", defaults.selectConfig);
    },
    
    postCreate: function() {
      /// <summary>
      /// General widget initialisations.
      /// </summary>
      this.inherited(arguments);
      this.busyNode.set("target", this.domNode.parentNode);
      this.own(
        on(this.clearSelectedNode, "click", lang.hitch(this, this._clearSelectedClick)),
        on(this.zoomToNode, "click", lang.hitch(this, this._showSelectionClick)),
        // Hook-up tool click event to toolbar div (note: intercepts child tool clicks)
        on(this.toolbarNode, "click", lang.hitch(this, this._selectToolClick)),
        on(this.toolbarNode, "keypress", lang.hitch(this, this._selectToolEnterClick)) // tab click funtionality
      )
    },
    
    startup: function() {
      /// <summary>
      /// Start widget - called by user.
      /// </summary>
      this.inherited(arguments);
      // Ensure map was specified
      if (!this.map) {
        this.destroy();
        console.log(this.baseClass+'::map required before startup!');
      }
      // Ensure map is loaded before initialising
      if (this.map.loaded) {
        this._init();
      } else {
        on.once(this.map, "load", lang.hitch(this, function() {
          this._init();
        }));
      }
    },
    
    destroy: function() {
      /// <summary>
      /// Connections/subscriptions will be cleaned up during the destroy() lifecycle phase.
      /// </summary>
      this.inherited(arguments);
      this.geomService = null;
      this.drawToolbar = null;
      this.map.removeLayer(this.bufferGraphicLayer);
      this.map = null;
      this.bufferGraphicLayer = null;
    },
    
    /* ---------------- */
    /* Public Functions */
    /* ---------------- */

    clear: function () {
      this._clearSelectedClick();
    },

    activateSelectWidget: function() {
      this.set("active", true);
      // Select the default select tool
      this.toolBufferNode.domNode.click(); //this.toolRectangleNode.domNode.click();
    },
    
    deactivateSelectWidget: function() {
      this.deactivateSelectTools();
      this.set("active", false);
    },
    
    deactivateSelectTools: function() {
      /// <summary>
      /// Deactivate the select tools and also the map graphics draw functions.
      /// </summary>
      // Deactivate the map graphics draw function
      this.drawToolbar.deactivate();
      // Enable map nav and identify
      this.map.enableMapNavigation();
      this.map.setInfoWindowOnClick(true);
      // Unselect all tools
      this.set("selectedToolDesc", "");
      domClass.remove(this.toolRectangleNode.domNode, "selected");
      domClass.remove(this.toolPointNode.domNode, "selected");
      domClass.remove(this.toolPolygonNode.domNode, "selected");
      domClass.remove(this.toolFreehandPolygonNode.domNode, "selected");
      domClass.remove(this.toolCircleNode.domNode, "selected");
      domClass.remove(this.toolBufferNode.domNode, "selected");
      domClass.remove(this.toolExtentNode.domNode, "selected");
    },
    
    /* ----------------- */
    /* Private Functions */
    /* ----------------- */
    
    _init: function() {
      /// <summary>
      /// Map dependant widget initialisations.
      /// </summary>
      var gLyr = new GraphicsLayer();
      this.set("bufferGraphicLayer", gLyr);
      this.map.addLayer(gLyr);
      this.set("drawToolbar", new Draw(this.map));
      this.own(
        on(this.map, "mouse-down", lang.hitch(this, this._drawBufferCircle)),
        on(this.drawToolbar, "DrawEnd", lang.hitch(this, this._processAOIDraw)),
        on(this.map, "key-down", lang.hitch(this, function(evt) {
          if (evt.shiftKey) {
            this.set("shiftDown", true);
          }
        })),
        on(this.map, "key-up", lang.hitch(this, function(evt) {
          if (!evt.shiftKey) {
            this.set("shiftDown", false);
          }
        }))
      );
      this._forceGraphicsLayerVisible();
      // Load options into layer lookup
      this._addLayerOptions();
      // Widget is now loaded - emit the loaded event
      this.set("loaded", true);
      this.emit("load", {});
    },
    
    _forceGraphicsLayerVisible: function() {
      // selection graphics layer must be visible
      if (this.graphicsLayer.visible == false) {
        this.graphicsLayer.setVisibility(true);
      }
    },

    _addLayerOptions: function() {
      // Load options into layer lookup
      var optionList;
      optionList = arrayUtils.map(this.selectConfig, function(lyr) {
        return { value: lyr.url, label: lyr.name };
      }, this);
      optionList.unshift({value: "ALL", label: "All Visible Select Layers" });
      this.layerNode.set("options", optionList);
      this.layerNode.set("value", "ALL");
    },

    _selectToolEnterClick: function(e) {
      if (e.which === 13) {
        this._selectToolClick(e);
      }
    },
    
    _selectToolClick: function(e) {
      /// <summary>
      /// Activate or deactivate map graphics draw depending if the clicked tool is also selected.
      /// <param name="e" type="event object">dijit/MenuBarItem.onClick event object. Contains clicked DOM node.</param>
      /// </summary>
      if (e.target.id === "toolbar") { return; }
      // Deselect all tools in the toolbar except for the clicked tool which should be toggled.
      var clickedNode = e.target;
      domClass.toggle(clickedNode, "selected");
      var selected = domClass.contains(clickedNode, "selected");
      this.deactivateSelectTools();
      if (selected) {
        this._forceGraphicsLayerVisible();
        this.set("selectedToolDesc", e.target.title);
        var drawFunct = this._getDrawFunction();
        domClass.add(clickedNode, "selected");
        // Disable radius if buffer not selected
        var bufferEnabled = (this.selectedToolDesc.toUpperCase().indexOf("BUFFER") === -1);
        this.radiusUnitNode.set("disabled", bufferEnabled);
        this.radiusNode.set("disabled", bufferEnabled);
        if (this.selectedToolDesc.toUpperCase().indexOf("EXTENT") !== -1) {
          // No need to use the draw toolbar to define AOI - AOI is the current map extent
          this._selectByMapExtent();
          domClass.remove(clickedNode, "selected");
        } else {
          // Activate draw toolbar and disable map navigation
          if (drawFunct) {
            this.drawToolbar.activate(drawFunct);
          }
          this.map.disableMapNavigation();
          this.map.setInfoWindowOnClick(false);
        }
      }
    },
    
    _drawBufferCircle:  function(evt) {
      if (this.active) {
        if (this.selectedToolDesc) { 
          if (this.selectedToolDesc.toUpperCase().indexOf("BUFFER") !== -1) {
            this._bufferPoint(evt.mapPoint);
          }
        }
      }
    },
    
    _processAOIDraw: function(geometry) {
      if (this.active) {
        if (this.selectedToolDesc) { 
          if (this.selectedToolDesc.toUpperCase().indexOf("BUFFER") === -1) {
            this._selectByDrawAOI(geometry);
          }
        }
      }        
    },
    
    _bufferPoint: function(pt) {
      var params = new BufferParameters();
      params.geometries = [ pt ];
      // Buffer in linear units: meters / km
      params.distances = [this.radiusNode.get("value")];
      switch (this.radiusUnitNode.get("value")) {
        case "metre":
          params.unit = GeometryService.UNIT_METER;
          break;
        case "kilometre":
          params.unit = GeometryService.UNIT_KILOMETER;
      }
      params.geodesic = true;
      this.geomService.buffer(params, lang.hitch(this, this._returnBuffer), this._bufferError);
    },
    
    _bufferError: function(err) {
      console.log(err);
    },
    
    _returnBuffer: function(bufferedGeometries) {
      arrayUtils.forEach(bufferedGeometries, lang.hitch(this, function(geometry) {
        var graphic = new Graphic(geometry, this._getBufferSymbol())
        this.bufferGraphicLayer.add(graphic);
        this._selectByDrawAOI(geometry);
      }));
    },

    _selectByBuffer: function () {
      var geometry = this.map.extent;
      this._selectByDrawAOI(geometry);
    },

    _selectByMapExtent: function () {
      var geometry = this.map.extent;
      this._selectByDrawAOI(geometry);
    },

    _selectByDrawAOI: function (geometry) {
      this._beforeSelect();
      this.executeCount = 0;
      this.returnedExecuteCount = 0;
      this.busyNode.show();
      var qryTask;
      var selectQuery = new Query();
      selectQuery.returnGeometry = true;
      selectQuery.spatialRelationship = Query.SPATIAL_REL_INTERSECTS;
      selectQuery.geometry = geometry;
      var addToSelection = true;
      if (!this.shiftDown) { this._clearSelectedClick(); addToSelection = false; }
      if (this.layerNode.get("value") == "ALL") {
        var nothingToSelect = true;
        arrayUtils.forEach(this.selectConfig, function (lyr) {
          if (this._isLayerVisible(lyr.url)) { // ensure layer is visible before searching
            nothingToSelect = false;
            qryTask = new QueryTask(lyr.url);
            selectQuery.outFields = [lyr.objectID];
            this.executeCount = this.executeCount + 1;
            qryTask.execute(selectQuery, lang.hitch(this, function (featureSet) { this._processQueryResults(featureSet, lyr, addToSelection); }));
          }
        }, this);
        if (nothingToSelect) {
          this._tidyupAfterSelection();
          this._showResults(false, true);
          this.busyNode.hide();
        }
      } else {
        var lyr;
        var selectedValue = this.layerNode.get("value");
        // find layer in config
        arrayUtils.forEach(this.selectConfig, function (layer) {
          if (layer.url == selectedValue) {
            lyr = layer;
          }
        }, this);
        qryTask = new QueryTask(lyr.url);
        selectQuery.outFields = [lyr.objectID];
        this.executeCount = this.executeCount + 1;
        qryTask.execute(selectQuery, lang.hitch(this, function(featureSet) { this._processQueryResults(featureSet, lyr, addToSelection); }),
                                     lang.hitch(this, function (err) { this._processQueryError(err, lyr.name); }));
      }
    },
    
    _isLayerVisible: function(url) {
      var lyrId = url.substring(url.lastIndexOf("/")+1);
      var visLayers = this.map.getLayersVisibleAtScale();
      for (var i = 0; i < visLayers.length; i++) {
        if (!visLayers[i].url) { continue; } // graphics layers have null url
        if (url.indexOf(visLayers[i].url) > -1) {
          var lyrInfos = visLayers[i].layerInfos;
          for (var j = 0; j < lyrInfos.length; j++) {
            if (lyrInfos[j].id.toString() == lyrId) {
              if (lyrInfos[j].visible) { return true; }
            }
          }
        }
      }
      return false;
    },

    _processQueryError: function(err, layerName) {
     /// <summary>
      /// Displays an error message in a dialog box to the user.
      /// <param name="err" type="javascript error">Error object</param>
      /// <param name="layerName" type="string">Name of the layer that returned the query error.</param>
      /// </summary>
      this.returnedExecuteCount = this.returnedExecuteCount + 1;
      errDialog = new Dialog({
        title: "<b style='color:red'>Select Error</b>",
        content: "An error occurred while selecting from " + layerName + ".",
        style: "width: 400px"
      });
      errDialog.show();
      console.log(err);
      this._tidyupAfterSelection();
      this._showResults(false, false);
      if (this.executeCount == this.returnedExecuteCount) {
        this.busyNode.hide();
      }
    },

    _processQueryResults: function(featureSet, layer, addToSelection) {
      this.returnedExecuteCount = this.returnedExecuteCount + 1;
      if (!("selectedObjectIds" in layer)) {
        layer["selectedObjectIds"] = [];
      }
      if (!("limitExceeded" in layer)) {
        layer["limitExceeded"] = false;
      }
      if (typeof featureSet.features === "undefined") { return; } // bail if nothing returned
      var resultFeatures = featureSet.features;
      if ("exceededTransferLimit" in featureSet) {
        layer.limitExceeded = featureSet["exceededTransferLimit"];
      }
      var fullExtent = null;
      var multiPoint = new Multipoint(this.map.spatialReference);
      for (var i=0; i<resultFeatures.length; i++) {
        var extent;
        var point;
        var graphic = resultFeatures[i];
        if (addToSelection) {
          // Check that the graphic isn't already selected
          var found = false;
          for (var j = 0; j < layer.selectedObjectIds.length; j++) {
            if (graphic.attributes[layer.objectID] == layer.selectedObjectIds[j]) {
              found = true;
              break;
            }
          }
          if (found) { continue; }
        }
        // Apply selection symbol to graphic
        switch (graphic.geometry.type) {
          case "point":
            point = graphic.geometry;
            graphic.setSymbol(this._getSelectionPointSymbol());
            break;
          case "multipoint":
            extent = graphic.geometry.getExtent();
            graphic.setSymbol(this._getSelectionPointSymbol());
            break;
          case "polyline":
            extent = graphic.geometry.getExtent();
            graphic.setSymbol(this._getSelectionPolylineSymbol());
            break;
          case "polygon":
            extent = graphic.geometry.getExtent();
            graphic.setSymbol(this._getSelectionPolygonSymbol());
            break;
        }
        // Determine extent so we can zoom to later
        if (point) {
          multiPoint.addPoint(point);
          fullExtent = multiPoint.getExtent();
        } else {
          if (fullExtent) {
            fullExtent = fullExtent.union(extent);
          } else {
            fullExtent = extent;
          }
        }
        // Add graphic to the map graphics layer.
        this.graphicsLayer.add(graphic);
        layer.selectedObjectIds.push(graphic.attributes[layer.objectID]);
        layer["selected"] = layer.selectedObjectIds.length;
      }
      if (this.fullExtent && fullExtent) {
        var currentFullExtent = this.fullExtent;
        this.set("fullExtent", currentFullExtent.union(fullExtent));
      } else {
        this.set("fullExtent", fullExtent);
      }
      this._tidyupAfterSelection();
      this._showResults(false, false);
      this._selectionChanged();
      if (this.executeCount == this.returnedExecuteCount) {
        this.busyNode.hide();
      }
    },
    
    _showResults: function (cleared, nothingToSelect) {
      var resultText = "";
      if (nothingToSelect) { resultText = "<tr><td><i><small><strong><font color='red'>(No visible select layers found!)</font></strong></small></i></td></tr>"; }
      if (!cleared) {
        arrayUtils.forEach(this.selectConfig, function (lyr) {
          if (lyr.limitExceeded) {
            resultText += "<tr><td><i><small><font color='red'>(" + lyr.name + ": " + lyr.selected + " selected - limit exceeded!)<font color='red'></small></i></td></tr>";
          } else {
            resultText += "<tr><td><i><small>(" + lyr.name + ": " + lyr.selected + " selected)</small></i></td></tr>";
          }
        }, this);
      }
      html.set(this.resultsNode, resultText);
    },

    _tidyupAfterSelection: function (lyr) {
      // Remove the buffer graphic
      if (this.bufferGraphicLayer) {
        this.bufferGraphicLayer.clear();
      }
      this.set("bufferGraphic", null);
    },
    
    _beforeSelect: function () {
      // Emit the selection_change event
      this.emit("before_select", {});
    },

    _selectionChanged: function() {
      // Emit the selection_change event
      this.emit("selection_change", { selectedGraphics: this.graphicsLayer.graphics });
      this.set("noSelection", !(this.graphicsLayer.graphics.length > 0));
    },

    _clearSelectedClick: function(evt) {
      /// <summary>
      /// Clear all selection graphics from graphics layer
      /// </summary>
      this.graphicsLayer.clear();
      this.set("fullExtent", null);
      arrayUtils.forEach(this.selectConfig, function (lyr) {
        lyr["selected"] = 0;
        lyr["selectedObjectIds"] = [];
        lyr["limitExceeded"] = false;
      }, this);
      this._showResults(true, false);
      this._selectionChanged();
    },

    _showSelectionClick: function(evt) {
      /// <summary>
      /// Zoom to the selected features.
      /// </summary>
      if (this.fullExtent) {
        this.map.setExtent(this.fullExtent);
      }
    },
    
    _getDrawFunction: function() {
      var draw = this.selectedToolDesc.toUpperCase().replace("SELECT BY ", "");
      switch (draw) {
        case "CLICK":
          return Draw.POINT;
        case "FREEHAND POLYGON":
          return Draw.FREEHAND_POLYGON;
        case "POLYGON":
          return Draw.POLYGON;
        case "RECTANGLE":
          return Draw.RECTANGLE;
        case "CIRCLE":
          return Draw.CIRCLE;
        default:
          return null;
      }
    },
    
    _toolIsSelected: function() {
      return (domClass.contains(this.toolRectangleNode.domNode, "selected") ||
        domClass.contains(this.toolPointNode.domNode, "selected") ||
        domClass.contains(this.toolPolygonNode.domNode, "selected") ||
        domClass.contains(this.toolFreehandPolygonNode.domNode, "selected") ||
        domClass.contains(this.toolCircleNode.domNode, "selected") ||
        domClass.contains(this.toolBufferNode.domNode, "selected") ||
        domClass.contains(this.toolExtentNode.domNode, "selected")) ? true : false;
    },
    
    _getSelectionPointSymbol: function() {
      /// <summary> 
      /// Compile selection point symbol
      /// </summary>
      return new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 10,
             new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 0, 0], 2),
             new Color([255, 255, 0, 0.5])));
    },
    
    _getSelectionPolygonSymbol: function() {
      /// <summary> 
      /// Compile selection polygon symbol
      /// </summary>
      return new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
             new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
             new Color([255, 0, 0]), 2), new Color([255, 255, 0, 0.3]));
    },
    
    _getSelectionPolylineSymbol: function() {
      /// <summary> 
      /// Compile selection line symbol
      /// </summary>
      return new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 0, 0]), 2);
    },
    
    _getBufferSymbol: function() {
      /// <summary> 
      /// Compile selection buffer symbol
      /// </summary>
      return new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
             new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
             new Color([255, 0, 0]), 2), new Color([0, 0, 0, 0.3]));
    }
    
	})

  return Widget;
  
});