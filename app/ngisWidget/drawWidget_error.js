/*
 | Author: NGIS 2015 (www.ngis.com.au) -> Kristy Carter
 | Module: DrawWidget
 | Description: Draw Widget - user interface controls for drawing and editing graphics on the map.
 | Usage: See readme.
 |
 | CHANGE LOG:
 |   2015-04-01 Kristy Carter: Initial code release.
 */
 
define([
	"dojo/_base/declare", "dojo/Evented", "dojo/dom", "dojo/_base/lang", "dojo/_base/array", "dojo/_base/Color", 
  "dojo/number", "dojo/keys", "dojo/dom-geometry", "dojo/query",
  "dijit/registry", "dijit/_WidgetBase", "dijit/_TemplatedMixin", "dijit/_WidgetsInTemplateMixin", 
  "dojo/text!./templates/drawWidget.html", "dojo/dom-style", "dojo/dom-class", "dojo/on", "dojo/store/Memory",
  "esri/map", "esri/toolbars/draw", "esri/toolbars/edit", 
  "esri/undoManager", "esri/SpatialReference", "esri/graphic", "esri/units",
  "esri/geometry/jsonUtils", "esri/geometry/webMercatorUtils", "esri/geometry/geodesicUtils", 
  "esri/geometry/Polygon", "esri/geometry/Polyline", "esri/geometry/Point",
  "esri/symbols/Font", "esri/symbols/TextSymbol",
  "esri/symbols/SimpleMarkerSymbol", "esri/symbols/SimpleLineSymbol", "esri/symbols/SimpleFillSymbol",
  "app/ngisWidget/customOperation",
  "dijit/Menu", "dijit/MenuItem", "dijit/MenuSeparator", "dijit/popup", "dijit/PopupMenuItem", 
  "dijit/form/HorizontalSlider", "dijit/form/TextBox", "dijit/MenuBarItem", "dijit/form/DropDownButton", 
  "dijit/form/NumberSpinner", "dijit/form/CheckBox", "dijit/TooltipDialog", "dijit/ColorPalette"
],
function(declare, Evented, dom, lang, arrayUtils, Color, 
  number, keys, domGeom, query,
  registry, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin,
  template, domStyle, domClass, on, Memory,
  Map, Draw, Edit,
  UndoManager, SpatialReference, Graphic, Units,
  geometryJsonUtils, webMercatorUtils, geodesicUtils,
  Polygon, Polyline, Point,
  Font, TextSymbol,
  SimpleMarkerSymbol, SimpleLineSymbol, SimpleFillSymbol,
  CustomOperation,
  Menu, MenuItem, MenuSeparator, popup, PopupMenuItem, 
  HorizontalSlider, TextBox
) {
  var MAP_PIN_MARKER = 'M0-165c-27.618 0-50 21.966-50 49.054C-50-88.849 0 0 0 0s50-88.849 50-115.946C50-143.034 27.605-165 0-165z'; //SVG Path
  var EMPTY_TEXT_STRING = "[Right click to edit]";
  
	var Widget = declare("ngisWidget.DrawWidget", [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, Evented], {
		templateString: template,
		baseClass: "DrawWidget",
    
    graphicCount : -2,
    
    // Widget option defaults (static)
		options: {
      map: null
    },
    
    // Attributes and custom setters
    text: "",
    _setTextAttr: { node: "drawTextNode", type: "value" }, // attach to DOM node value
    bold: false,
    _setBoldAttr: { node: "boldCheckBoxNode", type: "aria-checked" },
    underline: false,
    _setUnderlineAttr: { node: "underlineCheckBoxNode", type: "aria-checked" },
    italic: false,
    _setItalicAttr: { node: "italicCheckBoxNode", type: "aria-checked" },        
    fontSize: 16,
    _setFontSizeAttr: { node: "fontSizeNode", type: "value" },
    markerSize: 18,
    _setMarkerSizeAttr: { node: "markerSizeNode", type: "value" },
    markerType: SimpleMarkerSymbol.STYLE_PATH,
    _setMarkerTypeAttr: { node: "markerTypeNode", type: "value" },
    fillColor: "#000000",
    _setFillColorAttr: function(/*String*/ color){ // attach to DOM node style attribute
      this._set("fillColor", color);
      if (dom.byId(this.baseClass+"__fillColorSwatchNode")) {
        domStyle.set(dom.byId(this.baseClass+"__fillColorSwatchNode"), "backgroundColor", color);
      }
    },    
    borderWidth: 1,
    _setBorderWidthAttr: { node: "borderWidthNode", type: "value" },
    borderStyle: SimpleLineSymbol.STYLE_SOLID,
    borderColor: "#000000",
    _setBorderColorAttr: function(/*String*/ color){
      this._set("borderColor", color);
      if (dom.byId(this.baseClass+"__borderColorSwatch")) {
        domStyle.set(dom.byId(this.baseClass+"__borderColorSwatch"), "backgroundColor", color);
      }
    },
    textColor: "#000000",
    _setTextColorAttr: function(/*String*/ color){
      this._set("textColor", color);
      if (dom.byId(this.baseClass+"__textColorSwatch")) {
        domStyle.set(dom.byId(this.baseClass+"__textColorSwatch"), "backgroundColor", color);
      }
    },
    fillTransparency: 0.5,
    _setFillTransparencyAttr: function(/*int*/ val){ // attach to DOM node attribute
      this._set("fillTransparency", val);
      var newVal = -(val - 1);
      this.fillTransInputNode.innerHTML = number.format(newVal, {places:0, pattern:"#%"});
    },
    borderTransparency: 1,
    _setBorderTransparencyAttr: function(/*int*/ val){
      this._set("borderTransparency", val);
      var newVal = -(val - 1);
      this.borderTransInputNode.innerHTML = number.format(newVal, {places:0, pattern:"#%"});
    },
    textTransparency: 1,
    _setTextTransparencyAttr: function(/*int*/ val){
      this._set("textTransparency", val);
      var newVal = -(val - 1);
      this.textTransInputNode.innerHTML = number.format(newVal, {places:0, pattern:"#%"});
    },
    
    constructor: function(options, srcRefNode) {
      /// <summary>
      /// Widget constructor.
      /// <param name="options" type="Object">
      ///   Hash of initialisation options. The hash can contain any of the widget's properties, excluding read-only properties.
      /// </param>
      /// <param name="srcRefNode" type="DOMNode|String">
      ///   HTML source node reference.
      /// </param>
      /// </summary>
      this.domNode = srcRefNode; // widget node
      var defaults = lang.mixin({}, this.options, options); // mix in param options with defaults
      // Properties
      this.set("loaded", false);
      this.set("map", defaults.map);
      this.set("graphicsLayer", defaults.graphicsLayer);
    },
    
    postCreate: function() {
      /// <summary>
      /// General widget initialisations.
      /// </summary>
      this.inherited(arguments);
      // Hook up transparency slider widgets
      var fillTrans = new HorizontalSlider({
        name: "fillTransSlider",
        value: 0.5,
        minimum: 0,
        maximum: 1,
        intermediateChanges: true,
        style: "width:100px;",
        onChange: lang.hitch(this, function(val) {
          this.set("fillTransparency", 1 - val);
        })
      }, this.fillTransSliderNode).startup();
      var borderTrans = new HorizontalSlider({
        name: "borderTransSlider",
        value: 0,
        minimum: 0,
        maximum: 1,
        intermediateChanges: true,
        style: "width:100px;",
        onChange: lang.hitch(this, function(val) {
          this.set("borderTransparency", 1 - val);
        })
      }, this.borderTransSliderNode).startup();
      var textTrans = new HorizontalSlider({
        name: "borderTransSlider",
        value: 0,
        minimum: 0,
        maximum: 0.99,
        intermediateChanges: true,
        style: "width:100px;",
        onChange: lang.hitch(this, function(val) {
          this.set("textTransparency", 1 - val);
        })
      }, this.textTransSliderNode).startup();
      // Set defaults
      this.set("textColor", "#0000ff");
      this.set("borderColor", "#0000cd");
      this.set("fillColor", "#87ceeb");
      // Ensure that event handlers are unregistered when the widget is destroyed
      this.own(
        on(this.fillColorPaletteWidgetNode, "change", lang.hitch(this, function(e) {
          popup.close(this.fillColorDialogNode);
          this.set("fillColor", e);
        })),
        on(this.borderColorPaletteWidgetNode, "change", lang.hitch(this, function(e) {
          popup.close(this.borderColorDialogNode);
          this.set("borderColor", e);
        })),
        on(this.textColorPaletteWidgetNode, "change", lang.hitch(this, function(e) {
          popup.close(this.textColorDialogNode);
          this.set("textColor", e);
        })),
        
        on(this.boldCheckBoxNode, "change", lang.hitch(this, function(e) {
          this.set("bold", e);
        })),
        on(this.underlineCheckBoxNode, "change", lang.hitch(this, function(e) {
          this.set("underline", e);
        })),
        on(this.italicCheckBoxNode, "change", lang.hitch(this, function(e) {
          this.set("italic", e);
        })),
        on(this.fontSizeNode, "change", lang.hitch(this, function(e) {
          this.set("fontSize", e);
        })),
        on(this.borderWidthNode, "change", lang.hitch(this, function(e) {
          this.set("borderWidth", e);
        })),
        on(this.markerSizeNode, "change", lang.hitch(this, function(e) {
          this.set("markerSize", e);
        })),
        on(this.markerTypeNode, "change", lang.hitch(this, function(e) {
          this.set("markerType", e);
        })),
        on(this.textNode, "change", lang.hitch(this, function(e) {
          this.set("text", e);
        })),
        
        // Hook-up tool click event to toolbar div (note: intercepts child tool clicks)
        on(this.toolbarNode, "click", lang.hitch(this, this._drawToolClick)),
        
        // Deactivate the map edit toolbar when user clicks anywhere on the document other than the map
        on(document.body, "click", lang.hitch(this, function(evt) {
          var xMin = this.map.position.x;
          var yMin = this.map.position.y;
          var xMax = xMin + this.map.width;
          var yMax = yMin + this.map.height;
          if (evt.clientX < xMin || evt.clientX > xMax) {
            if (evt.clientY < yMin || evt.clientY > yMax) {
              this.editToolbar.deactivate();
            }
          }
        }))
      );
    },
    
    startup: function() {
      /// <summary>
      /// Start widget - called by user.
      /// </summary>
      this.inherited(arguments);
      // Ensure map was specified
      if (!this.map) {
        this.destroy();
        console.log('DrawWidget::map required before startup!');
      }
      // Ensure map is loaded before initialising
      if (this.map.loaded) {
        this._init();
      } else {
        on.once(this.map, "load", lang.hitch(this, function() {
          this._init();
        }));
      }
    },
    
    destroy: function() {
      /// <summary>
      /// Connections/subscriptions will be cleaned up during the destroy() lifecycle phase.
      /// </summary>
      this.inherited(arguments);
    },
    
    /* ---------------- */
    /*   Public Events  */
    /* ---------------- */
    // load
    
    /* ---------------- */
    /* Public Functions */
    /* ---------------- */
    
    activateDrawWidget: function() {
      this.set("active", true);
    },
    
    deactivateDrawWidget: function() {
      this.deactivateDrawTools();
      this.set("active", false);
    },
    
    deactivateDrawTools: function() {
      /// <summary>
      /// Deactivate the draw tools and also the map graphics draw/edit functions.
      /// </summary>
      // Deactivate the map graphics draw/edit function
      this.drawToolbar.deactivate();
      this.editToolbar.deactivate();
      this.map.enableMapNavigation();
      this.map.setInfoWindowOnClick(true);
      // Unselect all tools
      domClass.remove(this.toolTextNode.domNode, "selected");
      domClass.remove(this.toolPointNode.domNode, "selected");
      domClass.remove(this.toolMultipointNode.domNode, "selected");
      domClass.remove(this.toolPolylineNode.domNode, "selected");
      domClass.remove(this.toolFreehandPolylineNode.domNode, "selected");
      domClass.remove(this.toolPolygonNode.domNode, "selected");
      domClass.remove(this.toolFreehandPolygonNode.domNode, "selected");
      domClass.remove(this.toolCircleNode.domNode, "selected");
      domClass.remove(this.toolTriangleNode.domNode, "selected");
      domClass.remove(this.toolRectangleNode.domNode, "selected");
      domClass.remove(this.toolEllipseNode.domNode, "selected");
      domClass.remove(this.toolArrowNode.domNode, "selected");
    },
    
    /* ----------------- */
    /* Private Functions */
    /* ----------------- */
    
    _forceGraphicsLayerVisible: function() {
      // graphics layer must be visible
      if (this.graphicsLayer.visible == false) {
        this.graphicsLayer.setVisibility(true);
      }
    },
    
    _drawToolClick: function(e) {
      /// <summary>
      /// Activate or deactivate map graphics draw depending if the clicked tool is also selected.
      /// <param name="e" type="event object">dijit/MenuBarItem.onClick event object. Contains clicked DOM node.</param>
      /// </summary>
      if (e.target.id === this.baseClass+"__toolbar") { return; }
      // Deselect all tools in the toolbar except for the clicked tool which should be toggled.
      var clickedNode = e.target;
      domClass.toggle(clickedNode, "selected");
      var selected = domClass.contains(clickedNode, "selected");
      this.deactivateDrawTools();
      if (selected) {
        this._forceGraphicsLayerVisible();
        this.set("selectedToolId", e.target.id.replace(this.baseClass+"__", ""));
        this.set("selectedToolDesc", e.target.title);
        domClass.add(clickedNode, "selected");
        var textSelected = domClass.contains(this.toolTextNode.domNode, "selected");
        domStyle.set(this.drawGraphicsNode, "display", (textSelected ? "none" : "block" )); 
        domStyle.set(this.drawTextNode, "display", (textSelected ? "block" : "none" ));
        // Activate draw toolbar and disable map navigation
        this.drawToolbar.activate((textSelected ? "point" : this.selectedToolId));
        this.map.disableMapNavigation();
        this.map.setInfoWindowOnClick(false);
      }
    },
    
    _init: function() {
      /// <summary>
      /// Map dependant widget initialisations.
      /// </summary>
      this.set("drawToolbar", new Draw(this.map));
      this.set("editToolbar", new Edit(this.map));
      this.set("undoManager", new UndoManager());
      // Ensure that event handlers are unregistered when the widget is destroyed
      this.own(
        on(this.drawToolbar, "draw-end", lang.hitch(this, this._drawGraphic)),
        on(this.map, "click", lang.hitch(this, function(evt) {
          this.editToolbar.deactivate();
        })),
        on(this.undoGraphicsNode, "click", lang.hitch(this, function() {
          this.undoManager.undo();
        })),
        on(this.redoGraphicsNode, "click",  lang.hitch(this, function() {
          this.undoManager.redo();
        })),
        on(this.undoManager, "change",  lang.hitch(this, function() {
          //enable or disable buttons depending on current state of application
          this.undoGraphicsNode.set("disabled", (this.undoManager.canUndo ? false : true));
          this.redoGraphicsNode.set("disabled", (this.undoManager.canRedo ? false : true));
          this.undoGraphicsNode.set("title", (this.undoManager.canUndo ? "Undo... " + this.undoManager.peekUndo().label : ""));
          this.redoGraphicsNode.set("title", (this.undoManager.canRedo ? "Redo... " + this.undoManager.peekRedo().label : ""));
        }))
      );
      this._createGraphicsMenu();
      // Widget is now loaded - emit the loaded event
      this.set("loaded", true);
      this.emit("load", {});
      this.activateDrawWidget();
      this._forceGraphicsLayerVisible();
    },
    
    _drawGraphic: function(evt) {
      /// <summary> 
      /// Clear all graphics from graphics layer and reset undo manager
      /// <param name="evt" type="event object">drawToolbar.draw-end event object. Contains new Geometry</param>
      /// </summary>
      if ( evt !== undefined) {
        var symbol;
        // create appropriate symbol for the graphic type
        switch (evt.geometry.type) {
          case "point":
          case "multipoint":
            symbol = this._getCurrentPointSymbol();
            break;
          case "polyline":
            symbol = this._getCurrentLineSymbol();
            break;
          default:
            symbol = this._getCurrentPolySymbol();
            break;
        }
        var graphic = new Graphic(evt.geometry, symbol);
        if (this.selectedToolId == "text") {
          graphic = new Graphic(evt.geometry, this._getCurrentTextSymbol());
        }
        var operation = new CustomOperation.Add({
          graphicsLayer: this.graphicsLayer,
          graphic: graphic,
          label: "Add " + this.selectedToolDesc
        });
        this.undoManager.add(operation);
        this.graphicsLayer.add(graphic);
      }
    },
  
    _clearGraphicsClick: function(evt) {
      /// <summary> 
      /// Clear all graphics from graphics layer and reset undo manager
      /// </summary>
      this.graphicsLayer.clear();
	    this.undoManager.clearRedo();
      this.undoManager.clearUndo();
    },
    
    _hexToRgba: function(hexColor, transparencyVal) {
      /// <summary> 
      /// Convert hex colour to RGBA color
      /// <param name="hexColor" type="dojo/_base/Color">Color to convert</param>
      /// <param name="transparencyVal" type="decimal">Transparency value</param>
      /// </summary>
      var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hexColor);
      return result ? [ parseInt(result[1], 16), parseInt(result[2], 16), parseInt(result[3], 16), transparencyVal ] : null;
    },
    
    _getCurrentPointSymbol: function() {
      /// <summary> 
      /// Compile point symbol from user input
      /// </summary>
      var symbol = new SimpleMarkerSymbol(this.markerType, this.markerSize,
        new SimpleLineSymbol(this.borderStyle, new Color(this._hexToRgba(this.borderColor, this.borderTransparency)), this.borderWidth),
        new Color(this._hexToRgba(this.fillColor, this.fillTransparency))
      );
      if (this.markerType == SimpleMarkerSymbol.STYLE_PATH) {
        symbol.setPath(MAP_PIN_MARKER); 
        symbol.setOutline(new SimpleLineSymbol(this.borderStyle, new Color(this._hexToRgba(this.borderColor, this.borderTransparency)), (this.borderWidth * 6)));
      }
      return symbol;
    },
    
    _getCurrentPolySymbol: function() {
      /// <summary> 
      /// Compile polygon symbol from user input
      /// </summary>
      return new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID, 
        new SimpleLineSymbol(this.borderStyle, new Color(this._hexToRgba(this.borderColor, this.borderTransparency)), this.borderWidth), 
        new Color(this._hexToRgba(this.fillColor, this.fillTransparency))
      );
    },
    
    _getCurrentLineSymbol: function() {
      /// <summary> 
      /// Compile line symbol from user input
      /// </summary>
      return new SimpleLineSymbol(this.borderStyle, new Color(this._hexToRgba(this.borderColor, this.borderTransparency)), this.borderWidth);
    }, 
    
    _getCurrentTextSymbol: function() {
      /// <summary> 
      /// Compile text symbol from user input
      /// </summary>
      var font = new Font(this.fontSize);
      if (this.bold) { font.setWeight(Font.WEIGHT_BOLD); }
      if (this.italic) { font.setStyle(Font.STYLE_ITALIC); }
      if (this.underline) { font.setDecoration("underline"); }
      return new TextSymbol((this.text == "" ? "[Right click to edit]" : this.text), font, new Color(this._hexToRgba(this.textColor, this.textTransparency)));
    },
    
    _createGraphicsMenu: function() {
      /// <summary>
      /// Creates right-click context menu for map graphics
      /// </summary>
      // Right-click graphics context menu
      var ctxMenuForGraphics = new Menu({});
      var selectedGraphic;
      var selectedNode;
      var movedGraphicPosition;
      // Right-click Edit menu item
      var editMenu = new MenuItem({
        label: "Edit"
      });
      // Right-click Move menu item
      var moveMenu = new MenuItem({
        label: "Move"
      });
      // Right-click Rotate/Scale menu item
      var rotateScaleMenu = new MenuItem({
        label: "Rotate/Scale"
      });
      // Right-click Change Text  menu item
      var textPopup = new TextBox({
        name: "Text",
        id: "TextPopup",
        value: ""
      });
      var textMenu = new PopupMenuItem({
        label: "Change Text",
        popup: textPopup
      });
      var separatorMenu = new MenuSeparator();
      ctxMenuForGraphics.addChild(separatorMenu);
      //  Right-click Delete menu item
      var menuDelete = new MenuItem({
        label: "Delete"
      });
      // Compile the menu
      ctxMenuForGraphics.addChild(editMenu);
      ctxMenuForGraphics.addChild(moveMenu);
      ctxMenuForGraphics.addChild(rotateScaleMenu);
      ctxMenuForGraphics.addChild(textMenu);
      ctxMenuForGraphics.addChild(separatorMenu);
      ctxMenuForGraphics.addChild(menuDelete);
      ctxMenuForGraphics.startup();
      // Bind menu to the selected graphic and show the menu
      var openContentMenu = lang.hitch(this, function() {
        ctxMenuForGraphics.bindDomNode(selectedNode);
        textPopup.set("style", "display: inline-block");
        ctxMenuForGraphics.set("style", "display: inline-block");
        this.deactivateDrawTools();
      });
      // Unbind menu to the selected graphic and hide the menu
      var closeContentMenu = function() {
        ctxMenuForGraphics.unBindDomNode(selectedNode); 
        textPopup.set("style", "display: none");
        ctxMenuForGraphics.set("style", "display: none"); 
      };
      var getGraphicType = function() {
        switch(seletegraphic.graphic.type) {
          case "point":
            if (graphic.symbol.type = "textsymbol") {
              return "text";
            } else {
              return "marker";
            }
          case "multipoint":
            return "multipoint marker";
          case "polyline": 
            return "line";
          case "polygon":
            return "polygon";
          default:
            return "";
        }
      };
      // Ensure that event handlers are unregistered when the widget is destroyed
      this.own(
        on(editMenu, "click", lang.hitch(this, function () {
          if (selectedGraphic.geometry.type !== "point") {
            this.editToolbar.activate(Edit.EDIT_VERTICES, selectedGraphic);
          } else {
            this.editToolbar.activate(Edit.MOVE | Edit.EDIT_VERTICES | Edit.EDIT_TEXT | Edit.SCALE, selectedGraphic);
          }
        })),
        on(moveMenu, "click", lang.hitch(this, function() {
          this.editToolbar.activate(Edit.MOVE, selectedGraphic);
        })),
        on(rotateScaleMenu, "click", lang.hitch(this, function () {
          this.editToolbar.activate(Edit.ROTATE | Edit.SCALE, selectedGraphic);
        })),
        on(textPopup, "change", lang.hitch(this, function(val) {
          if (selectedGraphic) {
            selectedGraphic.symbol.setText(val == "" ? EMPTY_TEXT_STRING : val);
            selectedGraphic.draw();
          }
        })),
        on(textPopup, "keypress", lang.hitch(this, function(evt) {
          if (evt.keyCode == keys.ENTER) {
            closeContentMenu();
          }
        })),
        on(menuDelete, "click", lang.hitch(this, function () {
          var undoLabel = "";
          // Find the undo transaction for the selected graphic and remove it from the undo manager
          this.editToolbar.deactivate();
          for (var i = 0; i < this.undoManager.length; i++) {
            if (this.undoManager.get(i)._graphic === selectedGraphic) {
              undoLabel = this.undoManager.get(i).label;
              break;
            }
          }
          // Add delete operation to the undo manager
          var newLabel = undoLabel.replace("Add", "").replace("Move", "");
          newLabel = "Delete" + newLabel;
          var operation = new CustomOperation.Delete({
            graphicsLayer: this.graphicsLayer,
            graphic: selectedGraphic,
            label: newLabel
          });
          this.undoManager.add(operation);
          // Remove the graphic
          this.graphicsLayer.remove(selectedGraphic);
        })),
        on(this.editToolbar, "graphic-first-move", lang.hitch(this, function () {
          movedGraphicPosition = lang.clone(selectedGraphic.geometry);
        })),
        on(this.editToolbar, "graphic-move-stop", lang.hitch(this, function () {
          var undoLabel = "";
          // Find the undo transaction for the selected graphic and remove it from the undo manager
          this.editToolbar.deactivate();
          for (var i = 0; i < this.undoManager.length; i++) {
            if (this.undoManager.get(i)._graphic === selectedGraphic) {
              undoLabel = this.undoManager.get(i).label;
              break;
            }
          }
          var newLabel = undoLabel.replace("Add", "").replace("Delete", "");
          newLabel = "Move" + newLabel;
          // Add move operation to the undo manager
          var operation = new CustomOperation.Update({
            graphicsLayer: this.graphicsLayer,
            originalGeometry: movedGraphicPosition,
            graphic: selectedGraphic,
            label: newLabel
          });
          this.undoManager.add(operation);
        })),
        // Force graphics layer on when using this widget
        on(this.map, "mouse-down", lang.hitch(this, function (evt) {
          if (this.active) { // only show context menu if draw widget is active
            this._forceGraphicsLayerVisible();
          }
        })),
        // Get the selected graphic on right mouse click and show the context menu
        on(this.graphicsLayer, "mouse-down", lang.hitch(this, function (evt) {
          if (this.active) { // only show context menu if draw widget is active
            this._forceGraphicsLayerVisible();
            if (evt.button == 2) { // right-click only
              selectedGraphic = evt.graphic;
              selectedNode = selectedGraphic.getDojoShape().getNode();
              // Disable Change Text menu for non-text graphics
              if (selectedGraphic.symbol.type == "textsymbol") {
                var selectedText = selectedGraphic.symbol.text;
                textPopup.set("value", (selectedText == EMPTY_TEXT_STRING ? "" : selectedText));
                textMenu.set("disabled", false);
              } else {
                textMenu.set("disabled", true);
              }
              openContentMenu();
            }
          } else {
            closeContentMenu();
          }
        }))
      );
    }
    
	})

  return Widget;
  
});