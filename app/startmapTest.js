/*
 | Author: NGIS 2015 (www.ngis.com.au) -> Kristy Carter
 | Module: StartMap
 | Description: Code to drive the ESRI Map and widgets.
 |              Call Start() to load the ESRI Map and widgets.
 | Dependencies: This module requires the following existing ArcGIS Server services:-
 |    1. BISV map and image services.
 |    2. ArcGIS geometry service.
 |
 | CHANGE LOG:
 |   2015-05-?? KC: Initial release - production.
 */
define([
        "esri/config",
        "esri/urlUtils",
        "esri/map",
        "esri/toolbars/draw",
        "esri/layers/ArcGISDynamicMapServiceLayer",
        "esri/layers/FeatureLayer",
        "esri/layers/ImageParameters",
        "esri/layers/ArcGISImageServiceLayer",
        "esri/layers/ImageServiceParameters",
        "esri/layers/GraphicsLayer",
        "esri/layers/ArcGISTiledMapServiceLayer",
        "esri/domUtils",
        "dojo/i18n!esri/nls/jsapi",
        "esri/units",
        "esri/request",
        "esri/dijit/BasemapGallery",
        "esri/dijit/BasemapLayer",
        "esri/dijit/Basemap",
        "esri/dijit/Scalebar",
        "esri/dijit/Measurement",
        "esri/dijit/Popup",
        "esri/dijit/PopupTemplate",
        "esri/tasks/GeometryService",
        "esri/tasks/PrintTask",
        "esri/tasks/PrintParameters",
        "esri/tasks/PrintTemplate",
        "dijit/registry",
        "dijit/Dialog",
        "dojo/dom",
        "dojo/dom-style",
        "dojo/dom-construct",
        "dojo/dom-prop",
        "dojo/on",
        "dojo/dom-attr",
        "dojo/dom-class",
        "dojo/_base/connect",
        "dojo/_base/array",
        "dojo/html",
        "dojo/_base/lang",
        "dojo/promise/all",
        "dojo/request",
        "dojo/json",
        "dojo/request/xhr",
        "app/utility",
        "app/config",
        "app/ngisWidget/mapScaleControlWidget",
        "app/ngisWidget/mapNavigationWidget",
        "app/ngisWidget/drawWidget",
        "app/ngisWidget/printWidget",
        "app/ngisWidget/bookmarkWidget",
        "app/ngisWidget/selectWidget",
        "app/ngisWidget/searchWidget",
        "app/ngisWidget/TOC",
        "dijit/form/CheckBox",
        "dijit/layout/ContentPane",
        "dijit/layout/TabContainer",
        "dijit/layout/BorderContainer",
        "dojo/fx",
        "dojo/domReady!"
    ],
    function (esriConfig, urlUtils, Map, Draw, ArcGISDynamicMapServiceLayer, FeatureLayer,
              ImageParameters, ArcGISImageServiceLayer, ImageServiceParameters, GraphicsLayer,
              ArcGISTiledMapServiceLayer, domUtils, esriBundle, units, esriRequest,
              BasemapGallery, BasemapLayer, Basemap,
              Scalebar, Measurement, Popup, PopupTemplate,
              GeometryService, PrintTask, PrintParameters, PrintTemplate,
              registry, Dialog, dom, domStyle, domConstruct, domProp, on, domAttr, domClass,
              connect, arrayUtils, html, lang, all, request, JSON, xhr,
              utility, config,
              MapScaleControlWidget, MapNavigationWidget, DrawWidget, PrintWidget,
              BookmarkWidget, SelectWidget, SearchWidget, TOC,
              CheckBox, ContentPane, TabContainer) {
        console.log("==> startmap module load");

        //------------------------------------//
        //  CONSTANTS AND MODULE SHARED VARS  //
        //------------------------------------//

        var DEBUG_TRACE_MODE; // set from public Start() parameter
        var ERROR_IMG_SRC = "images/PrintError.png";
        var APP_TITLE = "BISV";

        var userToken = '80D3D31C-0892-4E0E-B061-0028FD4901AE';  // TODO              TODO                 TODO               TODO                           TODO              TODO                 TODO               TODO

        var tiledMapServiceLayer;
        var map;
        var selectionGraphicsLayer;
        var drawGraphicsLayer;
        var currentSelectionWidget;
        var numLayersLoaded = 0;
        var totalMapServicesToLoad = 0;
        var drawWidget;
        var selectWidget;
        var searchWidget;
        var printWidget;
        var measurementWidget;
        var measurementWidgetActive = false;
        var infoReportChecked = false;
        var areaReportChecked = false;

        var geomService = new GeometryService(config.GEOMETRY_SERVICE_URL);

        //------------------------------------//
        //       MAP AND LAYER CREATION       //
        //------------------------------------//

        var start = function (debugTraceMode) {
            /// <summary>
            /// Public function to initialise the map.
            /// </summary>
            DEBUG_TRACE_MODE = debugTraceMode;

            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.start(" + utility.page.uriParameters + ")");
            }

            uiInit();


            // Cross Origin Resource Sharing - proxy settings
            //esriConfig.defaults.io.proxyUrl = "proxy/proxy.ashx";
            //esriConfig.defaults.io.alwaysUseProxy = true;

            // Initialise map services
            initLayers();
        };

        function uiInit() {
            // Add checkboxes to the report tabs and wire to handlers
            domConstruct.place('<input type="checkbox" name="infoCheckbox" id="infoCheckbox"></input>', "resultsPane_tablist_infoReport", "first");
            domConstruct.place('<input type="checkbox" name="areaCheckbox" id="areaCheckbox"></input>', "resultsPane_tablist_areaReport", "first");
            on(dom.byId("infoCheckbox"), "click", function (cb) {
                infoReportChecked = cb.checked;
                if (!infoReportChecked) {
                    clearInfoReport();
                }
            });
            on(dom.byId("areaCheckbox"), "click", function (cb) {
                areaReportChecked = cb.checked;
                if (!areaReportChecked) {
                    clearAreaReport();
                }
            });

            // Initialise report results expanded state
            registry.byId("resultsExpandoPanel").toggle();

            // Conatact page
            //config.CONTACT_EMAIL
            //CONTACT_PHONE
        }

        function initLayers() {
            /// <summary>
            /// Initialise the required map layers.
            /// </summary>
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.initLayers");
            }

            createMapAddLayers();
        }

        function createMapAddLayers() {
            /// <summary>
            /// Create the map, set the extent, and add the services to the map.
            /// <param name="projectionResult" type="esri/geometry/Geometry[]">
            ///   Projected geometries resulting from GeometryService.project()
            /// </param>
            /// </summary>
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.createMapAddLayers");
            }
            // Create new map
            map = new Map("mapDiv");
            map.resize();


            // Configure default geometry service
            esriConfig.defaults.geometryService = geomService;
            // Add basemapGallery
            addBaseMapGallery();       // ---------------------------------------------------------------- TODO: HOOK UP BASE MAPS - TEMPORARILY OFF DUE TO CORS ERROR


            if (DEBUG_TRACE_MODE) {
                console.log("      !!!END MAP LOADING!!!");
            }
            utility.page.endLoading();
            console.log("end");
        }


        //------------------------------------//
        //        Layer REST Properties       //
        //------------------------------------//


        //------------------------------------//
        //      TABLE OF CONTENTS WIDGET      //
        //------------------------------------//


        function addBaseMapGallery() {
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.addBaseMapGallery");
            }

            tiledMapServiceLayer = new ArcGISTiledMapServiceLayer(config.BASEMAP_2_URL); // TEST CACHED MS
            map.addLayer(tiledMapServiceLayer); // TEST CACHED MS

            var basemaps = [];
            var basemapLayer1 = new BasemapLayer({
                url: config.BASEMAP_1_URL
            });
            var basemap1 = new Basemap({
                layers: [basemapLayer1],
                title: config.BASEMAP_1_NAME,
                thumbnailUrl: config.BASEMAP_1_IMAGE
            });
            basemaps.push(basemap1);

            var basemapLayer2 = new BasemapLayer({
                url: config.BASEMAP_2_URL
            });
            var basemap2 = new Basemap({
                layers: [basemapLayer2],
                title: config.BASEMAP_2_NAME,
                thumbnailUrl: config.BASEMAP_2_IMAGE
            });
            basemaps.push(basemap2);

            var basemapGallery = new BasemapGallery({
                showArcGISBasemaps: false,
                basemaps: basemaps,
                map: map
            }, "basemapGallery");
            basemapGallery.startup();
            on(basemapGallery, "error", function (error) {
                console.log("Basemap Gallery Error...");
                console.log(error);
            });
        }

        //------------------------------------//
        //          GENERAL WIDGETS           //
        //------------------------------------//

        function
        addScalebar() {
            // Add scalebar to map
            var scalebar = new Scalebar({
                map: map,
                scalebarUnit: "metric" // "dual" = both miles & km, "english" = miles (default); "metric" = km
            });
        }

        function addBookmarkWidget() {
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.addBookmarkWidget");
            }
            // Create the bookmark widget
            var bookmark = new BookmarkWidget({
                    map: map,
                    bookmarks: [],
                    editable: true,
                    //otherSerialisableData is optional
                    otherSerialisableData_Get: lang.hitch(searchWidget, searchWidget.getSearchCriteria),
                    otherSerialisableData_Set: lang.hitch(searchWidget, searchWidget.setSearchCriteria)
                }, dom.byId('bookmark')
            );
        }

        //------------------------------------//
        //         MAP GRAPHIC WIDGETS        //
        //------------------------------------//

        function addDrawWidget() {
            /// <summary>
            /// Adds draw widget to the page.
            /// </summary>
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.addDrawWidget");
            }
            drawWidget = new DrawWidget({
                map: map,
                graphicsLayer: drawGraphicsLayer
            }, "drawPanel");
            drawWidget.startup();
            // Deactivate the draw tools when draw task panel is hidden
            on(registry.byId("panelDraw"), "hide", function (evt) {
                drawWidget.deactivateDrawWidget();
            });
            // Activate the draw tools when draw task panel is shown
            on(registry.byId("panelDraw"), "show", function (evt) {
                drawWidget.activateDrawWidget();
            });
        }

        function addSearchWidget() {
            /// <summary>
            /// Adds search widget to the page.
            /// </summary>
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.addSearchWidget");
            }
            var selectLayers = arrayUtils.map(config.FEATURE_SERVICES, function (lyr) {
                if (lyr.SELECTABLE) {
                    return {id: lyr.LAYER_ID, name: lyr.LAYER_NAME}
                } else {
                    return null;
                }
            });
            searchWidget = new SearchWidget({
                map: map,
                graphicsLayer: selectionGraphicsLayer,
                searchConfig: config.SEARCH
            }, "searchPanel");
            searchWidget.startup();
            searchWidget.on("before_select", function (evt) {
                // Clear selection from SELECT widget before searching
                if (currentSelectionWidget !== "SEARCH") {
                    selectWidget.clear();
                }
                currentSelectionWidget == "SEARCH";
            });
            // TODO : Info Widget and Area Report Widget will listen to this event and use evt.selectedGraphics as input
            searchWidget.on("selection_change", function (evt) {
                console.log("searchWidget::selection_change. selection count is " + evt.selectedGraphics.length);
                showResultsPanel();
                runInfoReport();
                runAreaReport();
            });
        }

        function addSelectWidget() {
            /// <summary>
            /// Adds select widget to the page.
            /// </summary>
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.addSelectWidget");
            }
            selectWidget = new SelectWidget({
                map: map,
                graphicsLayer: selectionGraphicsLayer,
                geomService: geomService,
                selectConfig: config.SELECTION_LAYERS
            }, "selectPanel");
            selectWidget.startup();
            selectWidget.on("before_select", function (evt) {
                // Clear selection from SEARCH widget before selecting
                if (currentSelectionWidget !== "SELECT") {
                    searchWidget.clear();
                }
                currentSelectionWidget == "SELECT";
            });
            // TODO : Info Widget and Area Report Widget will listen to this event and use evt.selectedGraphics as input
            selectWidget.on("selection_change", function (evt) {
                console.log("selectWidget::selection_change. selection count is " + evt.selectedGraphics.length);
                showResultsPanel();
                runInfoReport();
                runAreaReport();
            });

            // Deactivate the select tools when select task panel is hidden
            on(registry.byId("panelSelect"), "hide", function (evt) {
                selectWidget.deactivateSelectWidget();
            });
            // Activate the select tools when select task panel is shown
            on(registry.byId("panelSelect"), "show", function (evt) {
                selectWidget.activateSelectWidget();
            });
        }

        function addMeasureWidget() {
            /// <summary>
            /// Adds measure widget to the page.
            /// </summary>
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.addMeasureWidget");
            }
            measurementWidget = new Measurement({
                map: map,
                defaultAreaUnit: units.SQUARE_METERS,
                defaultLengthUnit: units.METERS
            }, "measurePanel");
            measurementWidget.startup();
            // Deactivate the measure tools when measure task panel is hidden
            on(registry.byId("panelMeasure"), "hide", function (evt) {
                measurementWidget.clearResult();
                measurementWidget.setTool("area", false);
                measurementWidget.setTool("distance", false);
                measurementWidget.setTool("location", false);
                map.setInfoWindowOnClick(true);
                measurementWidgetActive = false;
            });
            on(registry.byId("panelMeasure"), "show", function (evt) {
                // Disable identify popups when the measure task panel is shown
                map.setInfoWindowOnClick(false);
                measurementWidgetActive = true;
                forceGraphicsLayerActive();
            });
            on(map, "mouse-down", function (evt) {
                if (measurementWidgetActive) {
                    // Force graphics layer on when using this widget
                    forceGraphicsLayerActive();
                }
            });
        }

        function forceGraphicsLayerActive() {
            if (map.graphics.visible == false) {
                map.graphics.setVisibility(true);
            }
        }

        //------------------------------------//
        //            REPORT WIDGETS          //
        //------------------------------------//

        function showResultsPanel() {
            var resultsPanel = registry.byId("resultsExpandoPanel");
            if (!resultsPanel._showing) {
                resultsPanel.toggle();
            }
        }

        function clearInfoReport() {
            alert("TODO - clear info report");
        }

        function clearAreaReport() {
            alert("TODO - clear area report");
        }

        function runInfoReport() {
            if (infoReportChecked) {
                alert("TODO - run info report");
            }
        }

        function runAreaReport() {
            if (infoReportChecked) {
                alert("TODO - run area report");
            }
        }

        //------------------------------------//
        //       MAP NAVIGATION WIDGETS       //
        //------------------------------------//

        function addMapNavigationWidget() {
            /// <summary>
            /// Adds map navigation widget to the map.
            /// </summary>
            var mapNavigationWidget = new MapNavigationWidget({
                map: map
            }, "mapNavigation");
            mapNavigationWidget.startup();
        }

        function addMapScaleWidget() {
            /// <summary>
            /// Adds map scale control to the map.
            /// </summary>
            var mapScaleControl = new MapScaleControlWidget({
                map: map
            }, "mapScaleControl");
            mapScaleControl.startup();
        }

        //------------------------------------//
        //            MAP PRINTING            //
        //------------------------------------//

        function addPrintMapWidget() {
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.addPrintMapWidget");
            }
            xhr.post(config.ADMIN_WEB_SERVICE_URL + "/getBISVRoleLayouts", {
                handleAs: "json",
                data: JSON.stringify({token: userToken}),
                headers: {'Content-Type': 'application/json'}
            }).then(function (data) {
                printWidget = new PrintWidget({
                    map: map,
                    printTimeout: config.PRINTTASK_TIMEOUT,
                    titleMaxChar: config.PRINT_MAP_TITLE_MAX_CHARS,
                    printMapTitleDefault: config.PRINT_MAP_TITLE_DEFAULT,
                    mapTemplates: data.d,
                    printMapServiceUrl: config.PRINT_MAP_SERVICE_URL
                }, "printPanel");
                printWidget.startup();
            }, function (err) {
                console.log("ERROR :: " + config.ADMIN_WEB_SERVICE_URL + "/getBISVRoleLayouts...");
                console.log(err);
            });
        }

        //------------------------------------//
        //              GENERAL               //
        //------------------------------------//

        function showError(title, message) {
            /// <summary>
            /// Displays an error message in a dialog box to the user.
            /// <param name="title" type="string">Title is shown in the dialog header after "! ERROR ! - ".</param>
            /// <param name="message" type="string">Error message to display in the content section of the dialog.</param>
            /// </summary>
            utility.page.endLoading();
            errDialog = new Dialog({
                title: "<b style='color:red'>! ERROR !  " + title + "</b>",
                content: message,
                style: "width: 500px"
            });
            errDialog.show();
        }

        //------------------------------------//
        //          PUBLIC METHODS            //
        //------------------------------------//

        return {
            start: start
        };

    }
);
