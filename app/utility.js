/*
 | Author: NGIS 2014 (www.ngis.com.au) -> Kristy Carter
 | Module: Utility
 | Description: Contains various utility javascript functions.
 |
 | CHANGE LOG: 
 |   2015-03-12 KC: Initial release - production.
 */
define([
        "dojo/_base/declare",
        "dojo/dom",
        "dojo/dom-style",
        "dojo/on",
        "dojo/_base/lang",
        "dojo/_base/fx",
        "app/config",
        "dijit/Dialog",
        "dijit/form/Button"
    ],
    function (declare, dom, domStyle, on, lang, fx, config) {
        /// <summary>
        /// Contains various utility javascript functions.
        /// </summary>
        var DEBUG_TRACE_MODE = false;
        if (DEBUG_TRACE_MODE) {
            console.log("==> utility");
        }

        var myPage = null;

        var _getUriParameters = function () {
            /// <summary>
            /// Get URI parameters and return as associative array.
            ///  - note: all parameter strings are returned lower case.
            /// </summary>
            var params = [], hash;
            var queryString = window.location.href.slice(window.location.href.indexOf('?') + 1);
            queryString = decodeURIComponent(queryString);
            var hashes = queryString.split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                params.push(hash[0].toLowerCase());
                params[hash[0]] = hash[1];
            }
            return params;
        };

        var page = declare(null, {
            /// <summary>
            /// Private Page class. Controls page load animation overlay and query string URI parameters.
            /// </summary>
            uriParameters: null,
            /// <field name='uriParameters' type='associative array'>URI query string parameters</field>
            overlayNode: null,
            constructor: function () {
                this.uriParameters = _getUriParameters();
                this.overlayNode = dom.byId("loadingOverlay");
            },
            startLoading: function () {
                domStyle.set(this.overlayNode, 'display', 'block');
            },
            endLoading: function () {
                /// <summary>
                /// Hides the page load animation overlay.
                /// </summary>
                // fade the overlay gracefully
                fx.fadeOut({
                    node: this.overlayNode,
                    onEnd: function (node) {
                        domStyle.set(node, 'display', 'none');
                    }
                }).play();
            },
            loadDisclaimer: function (accepted, rejected) {
                var response = false;
                //this.accepted = function () { accepted(); };
                //this.rejected = function () { rejected(); };
                dom.byId("disclaimerText").innerHTML = config.DISCLAIMER_TEXT + "<br /><br />Do you agree to the above terms?";
                on(disclaimerDialog, "hide", function (e) {
                    if (response) {
                        accepted();
                    } else {
                        rejected();
                    }
                });
                on(dom.byId("acceptButton"), "click", function (e) {
                    disclaimerDialog.hide();
                    response = true;
                });
                on(dom.byId("declineButton"), "click", function (e) {
                    disclaimerDialog.hide();
                    response = false;
                });
                disclaimerDialog.show();
                domStyle.set(dom.byId("disclaimer"), 'display', 'block');
            }
        });

        function is(obj, type) {
            /// <summary>
            /// Correct way to check the type of an object.
            /// <param name="obj" type="object">Object to check type of.</param>
            /// <param name="type" type="string">Type name of object.</param>
            /// </summary>
            if (DEBUG_TRACE_MODE) {
                console.log(Object.prototype.toString);
            }
            var clas = Object.prototype.toString.call(obj).slice(8, -1);
            return obj !== undefined && obj !== null && clas === type;
        }

        myPage = new page();
        return {
            page: myPage, // Return instantiated page
            is: is
        };
    }
);