/*
 | Author: NGIS 2014 (www.ngis.com.au)
 | Module: Config
 | Description: Parse user configuration items for use in the application.
 |
 | CHANGE LOG: 
 |   2015-03-12 KC: Initial release - production.
 */

define(["dojo/text!app/config.txt"], 
  function(configText) {
    //console.log("==> config module load");
    
    var newConfigText = configText.replace(/#.*#/g, '');
    //console.log("==== "+newConfigText);
    var config = eval("( { "+newConfigText+" } )");
    //console.log("==== "+obj.PRINT_MAP_TEMPLATE_PREFIXES["PVP"]);
    
    return config;
  }
);