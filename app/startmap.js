/*
 | Author: NGIS 2015 (www.ngis.com.au) -> Kristy Carter
 | Module: StartMap
 | Description: Code to drive the ESRI Map and widgets.
 |              Call Start() to load the ESRI Map and widgets.
 | Dependencies: This module requires the following existing ArcGIS Server services:-
 |    1. BISV map and image services.
 |    2. ArcGIS geometry service.
 |
 | CHANGE LOG:
 |   2015-05-?? KC: Initial release - production.
 */
define([
    "esri/config",
    "esri/urlUtils",
    "esri/map",
    "esri/toolbars/draw",
    "esri/layers/ArcGISDynamicMapServiceLayer",
    "esri/layers/FeatureLayer",
    "esri/layers/ImageParameters",
    "esri/layers/ArcGISImageServiceLayer",
    "esri/layers/ImageServiceParameters",
    "esri/layers/GraphicsLayer",
    "esri/layers/ArcGISTiledMapServiceLayer",
    "esri/domUtils",

    "dojo/i18n!esri/nls/jsapi",

    "esri/units", "esri/request",
    "esri/geometry/Extent",
    "esri/SpatialReference",
    "esri/geometry/Point",
    "esri/dijit/BasemapGallery",
    "esri/dijit/BasemapLayer",
    "esri/dijit/Basemap",
    "esri/dijit/Scalebar",
    "esri/dijit/Measurement",
    "esri/dijit/Popup",
    "esri/dijit/PopupTemplate",
    "esri/tasks/GeometryService",
    "esri/tasks/PrintTask",
    "esri/tasks/PrintParameters",
    "esri/tasks/PrintTemplate",

    "dijit/registry",
    "dijit/Dialog",

    "dojo/dom",
    "dojo/dom-style",
    "dojo/dom-construct",
    "dojo/dom-prop",
    "dojo/on",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/query",
    "dojo/NodeList-dom",
    "dojo/_base/connect",
    "dojo/_base/array",
    "dojo/html",
    "dojo/_base/lang",
    "dojo/promise/all",
    "dojo/request",
    "dojo/json",
    "dojo/request/registry",
    "dojo/request/xhr",
    "dojo/Deferred",
    "app/utility",

    "app/config",
    "app/ngisWidget/mapScaleControlWidget",
    "app/ngisWidget/mapNavigationWidget",
    "app/ngisWidget/drawWidget",
    "app/ngisWidget/printWidget",
    "app/ngisWidget/bookmarkWidget",
    "app/ngisWidget/selectWidget",
    "app/ngisWidget/searchWidget",
    "app/ngisWidget/TOC",
    "app/ngisWidget/areaReportWidget",
    "app/ngisWidget/infoReportWidget",

    "dijit/form/CheckBox",
    "dijit/layout/ContentPane",
    "dijit/layout/TabContainer",
    "dijit/layout/BorderContainer",
    "dojo/fx",
    "dojo/domReady!"
    ],
    function (esriConfig, urlUtils, Map, Draw, ArcGISDynamicMapServiceLayer, FeatureLayer,
              ImageParameters, ArcGISImageServiceLayer, ImageServiceParameters, GraphicsLayer,
              ArcGISTiledMapServiceLayer, domUtils, esriBundle, units, esriRequest,
              Extent, SpatialReference, Point,
              BasemapGallery, BasemapLayer, Basemap,
              Scalebar, Measurement, Popup, PopupTemplate,
              GeometryService, PrintTask, PrintParameters, PrintTemplate,
              registry, Dialog, dom, domStyle, domConstruct, domProp, on, domAttr, domClass, query, nodeListDom,
              connect, arrayUtils, html, lang, all, request, JSON,
              request, xhr, Deferred,
              utility, config,
              MapScaleControlWidget, MapNavigationWidget, DrawWidget, PrintWidget,
              BookmarkWidget, SelectWidget, SearchWidget, TOC, AreaReportWidget, InfoReportWidget,
              CheckBox, ContentPane, TabContainer) {

        //------------------------------------//
        //  CONSTANTS AND MODULE SHARED VARS  //
        //------------------------------------//

        var DEBUG_TRACE_MODE; // set from public Start() parameter
        var userToken = '';
        var sitePublicAccess = true;

        var map;
        var baseMapGallery;
        var selectionGraphicsLayer;
        var drawGraphicsLayer;
        var currentSelectionWidget;
        var numLayersLoaded = 0;
        var totalMapServicesToLoad = 0;
        var tocWidget;
        var drawWidget;
        var areaReportWidget;
        var infoReportWidget;
        var selectWidget;
        var searchWidget;
        var printWidget;
        var mapLegend;
        var measurementWidget;
        var measurementWidgetActive = false;
        var infoReportChecked = false;
        var areaReportChecked = false;

        var geomService = new GeometryService(config.GEOMETRY_SERVICE_URL);

        //------------------------------------//
        //       MAP AND LAYER CREATION       //
        //------------------------------------//

        var start = function (debugTraceMode, token, publicAccess) {
            /// <summary>
            /// Public function to initialise the map.
            /// </summary>
            DEBUG_TRACE_MODE = debugTraceMode;
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.start(" + utility.page.uriParameters + ")");
            }
            userToken = token;
            sitePublicAccess = publicAccess;
            // UI Initialisations
            uiInit();

            // Proxy settings :: NOTE - secured services need to be routed through the proxy which supplies the login credentials
            urlUtils.addProxyRule({
                urlPrefix: config.SECURE_SERVICE_PROXY_URL_PREFIX,  //"http://goulbap19.dec.int:6080/arcgis/rest/services/BISVDEV",
                proxyUrl: "proxy/proxy.ashx"
            });

            urlUtils.addProxyRule({
                urlPrefix: config.IMAGE_SERVICE_PROXY_URL_PREFIX,  //"http://appmapdata.environment.nsw.gov.au/ArcGIS/rest/services",
                proxyUrl: "proxy/proxy.ashx"
            });

            //// OLD Proxy settings
            ////esriConfig.defaults.io.proxyUrl = "proxy/proxy.ashx";
            ////esriConfig.defaults.io.alwaysUseProxy = true;

            // Initialise map services
            initLayers();
            if (window.XDomainRequest) {
                showError("Biodiversity Investment Spatial Viewer",
                    "This application is not fully compatible with Internet Explorer 9 (IE9) and earlier versions.  Please use one of the following browsers to access full functionality: Google Chrome; Firefox; IE10 or later.", true);
            }
        };

        function uiInit() {
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.uiInit");
            }

            // Add checkboxes to the report tabs and wire to handlers
            domConstruct.place('<input type="checkbox" title="Report will run if checked. Uncheck to cancel." name="infoCheckbox" id="infoCheckbox"></input>', "resultsPane_tablist_infoReportTab", "first");
            domConstruct.place('<input type="checkbox" title="Report will run if checked" name="areaCheckbox" id="areaCheckbox"></input>', "resultsPane_tablist_areaReportTab", "first");
            on(dom.byId("infoCheckbox"), "click", function (cb) {
                infoReportChecked = cb.target.checked;
                if (!infoReportChecked) {
                    clearInfoReport();
                } else {
                    if (searchWidget.graphicsLayer.graphics.length > 0) {
                        runInfoReport(searchWidget.graphicsLayer.graphics); // use search tool selection
                    } else {
                        runInfoReport(selectWidget.graphicsLayer.graphics); // use select tool selection
                    }
                }
            });
            on(dom.byId("areaCheckbox"), "click", function (cb) {
                areaReportChecked = cb.target.checked;
                if (!areaReportChecked) {
                    clearAreaReport(false);
                } else {
                    runAreaReport(searchWidget.graphicsLayer.graphics);
                }
            });
            // Report hint text
            var hintText = "<small><table><tr><td>Note:</td><td> * Checked reports will run automatically on selection change.</td></tr>" +
                "<tr><td>&nbsp;</td><td> * Plan Info may take a long time to process depending on input. To cancel, uncheck the tab checkbox.</td></tr></table></small>"
            dom.byId("reportHint").innerHTML = hintText;
            // Initialise report results expanded state
            registry.byId("resultsExpandoPanel").toggle();
            // Contact page
            var helpContent = "<table class='helpLinks' width='100%'><tr><td><a href=" + config.HELP_ABOUT_URL + " target='_blank'><strong>About</strong></a>&nbsp;&nbsp;&nbsp;" +
                "<td><a href=" + config.HELP_URL + " target='_blank'><strong>Help</strong></a>&nbsp;&nbsp;&nbsp;" +
                "<td><a href=" + config.HELP_FAQ_URL + " target='_blank'><strong>FAQ</strong></a></td></tr></table>" +
                "<h3>Register</h3>" +
                "<p>Some conservation commitment information is not available to the public because of privacy concerns, but can be viewed by registered users <a href='" + config.CONTACT_LOGIN_URL + "'>here</a>." +
                "To become a registered user you must be a NSW or Commonwealth Government officer or an accredited BioBanking assessor.</p>" +
                "<p>To request registration, email <a href='mailto:" + config.CONTACT_REGISTER_EMAIL + "?Subject=BISV%20Registration%20Request'>" + config.CONTACT_REGISTER_EMAIL +
                "</a> with your name, organisation and contact number. Government officers must use their work email address to send the request. You will be sent a login and password within five working days.</p>" +
                "<h3>Contact Us</h3>" +
                "<p><strong>Email</strong></p>" +
                "<p>Have a question? Email the project team: <a href='mailto:" + config.CONTACT_EMAIL + "?Subject=BISV%20Map'>" + config.CONTACT_EMAIL + "</a>.&nbsp;</>" +
                "<p><strong>Phone</strong></p>" +
                "<p>Alternatively, call the Office of Environment and Heritage on " + config.CONTACT_PHONE + ".</p>";
            dom.byId("helpContent").innerHTML = helpContent;
            if (!sitePublicAccess) {
                dom.byId("logout").innerHTML = "<a href=" + config.LOGOUT_URL + ">Log out</a>"
            }
        }


        function setupFLListeners(featService) {
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.setupFLListeners");
            }
            // featService.FEATURE_LAYER.on("error", function(err) { errorRetrievingLayer(err, featService.LAYER_NAME); });
            featService.FEATURE_LAYER.on("load", initMapAfterAllLayersLoaded);
        }

        function setupMSLListeners(mapService) {
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.setupMSLListeners");
            }
            // mapService.MAPSERVICE_LAYER.on("error", function(err) { errorRetrievingLayer(err, mapService.MAPSERVICE_NAME); });
            mapService.MAPSERVICE_LAYER.on("load", initMapAfterAllLayersLoaded);
        }

        function initMapAfterAllLayersLoaded() {
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.initMapAfterAllLayersLoaded");
            }
            numLayersLoaded++;
            if (numLayersLoaded == (config.FEATURE_SERVICES.length + config.MAP_SERVICES.length)) {
                createMapAddLayers();
            }
        }

        function loadFeatureServices() {
            /// <summary>
            /// Initialise the required map feature layers.
            /// </summary>
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.loadFeatureServices");
            }
            arrayUtils.forEach(config.FEATURE_SERVICES, function (featService) {
                var popupTemplate = null;
                if ("LAYER_IDENTIFY_TEMPLATE" in featService) {
                    popupTemplate = new PopupTemplate(featService.LAYER_IDENTIFY_TEMPLATE);
                }
                var featLayer = new FeatureLayer(featService.LAYER_URL, {
                    id: featService.LAYER_ID,
                    mode: FeatureLayer.MODE_ONDEMAND,
                    infoTemplate: popupTemplate,
                    outFields: ["*"],
                    opacity: featService.INITIAL_OPACITY,
                    visible: true
                });
                featService["FEATURE_LAYER"] = featLayer;
                setupFLListeners(featService);
            });
        }

        function loadMapServices() {
            /// <summary>
            /// Initialise the required map mervice layers.
            /// </summary>
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.loadMapServices");
            }
            arrayUtils.forEach(config.MAP_SERVICES, function (mapService) {
                var popupTemplate = null;
                var infoTemplates = null;
                if ("LAYER_IDENTIFY_TEMPLATES" in mapService) {
                    infoTemplates = {};
                    for (var key in mapService.LAYER_IDENTIFY_TEMPLATES) {
                        popupTemplate = new PopupTemplate(mapService.LAYER_IDENTIFY_TEMPLATES[key]);
                        infoTemplates[key] = {infoTemplate: popupTemplate, layerUrl: null};
                    }
                }
                var imageParameters = new ImageParameters();
                imageParameters.transparent = true;
                imageParameters.format = "png32"; // Note: required for transparency adjustment against basemap (default format didn't allow this)
                var mapServiceLayer = new ArcGISDynamicMapServiceLayer(mapService.MAPSERVICE_URL, {
                    id: mapService.MAPSERVICE_ID,
                    imageParameters: imageParameters,
                    infoTemplates: infoTemplates,
                    opacity: mapService.INITIAL_OPACITY,
                    visible: true
                });
                mapService["MAPSERVICE_LAYER"] = mapServiceLayer;
                setupMSLListeners(mapService);
            });
        }

        function removeUnauthorisedMapServices(restrictedMapServices) {
            var authorisedMapServices = [];
            for (var i = 0; i < config.MAP_SERVICES.length; i++) {
                if (checkAuthorised(restrictedMapServices, config.MAP_SERVICES[i].MAPSERVICE_ID)) {
                    //console.log("authorised for " + config.MAP_SERVICES[i].MAPSERVICE_ID);
                    totalMapServicesToLoad++;
                    authorisedMapServices.push(config.MAP_SERVICES[i]);
                }
            }
            config.MAP_SERVICES = [];
            config.MAP_SERVICES = authorisedMapServices;
            if (config.MAP_SERVICES.length == 0) {
                showError("BISV", "There are no map services configured for this user!");
            }
        }

        function checkAuthorised(restrictedMapServices, mapServiceName) {
            // Ensure user is authorised to view this map service
            for (var i = 0; i < restrictedMapServices.length; i++) {
                if (restrictedMapServices[i].MapName == mapServiceName) {
                    return true;
                }
            }
            return false;
        }

        function initLayers() {
            /// <summary>
            /// Initialise the required map layers.
            /// </summary>
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.initLayers");
            }
            loadFeatureServices();
            getMapServicesForUserRole();
        }

        function xdr(url, options) {
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.xdr");
            }
            var def = new Deferred();
            var xdr = new XDomainRequest();
            if (xdr) {
                xdr.timeout = 30000;
                xdr.onload = function (e) {
                    def.resolve(xdr.responseText);
                };
                xdr.onerror = function () {
                    console.log("XDR Error");
                };
                xdr.ontimeout = function () {
                    console.log("XDR Timeout Error");
                };
                // bug fix: this must be set
                xdr.onprogress = function () {
                    //console.log('progress');
                };
                xdr.open(options.method, url);
                xdr.send(options.data);
                return def;
            }
            def.reject(new Error('XDomainRequest not supported.'));
            return def;
        }

        function getMapServicesForUserRole() {
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.getMapServicesForUserRole");
            }
            //loadMapServices(); //DEBUG - remove
            //return; //DEBUG - remove
            var url = config.ADMIN_WEB_SERVICE_URL + "/getBISVRoleMaps";
            if (!window.XDomainRequest) {
                xhr.post(url, {
                    handleAs: "json",
                    data: JSON.stringify({token: userToken}),
                    headers: {'Content-Type': 'application/json'}
                }).then(function (data) {
                    // Note: BISVRoleMap.MapName from web service matches MAP_SERVICES.MAPSERVICE_ID
                    mapLegend = data.d[0].LegendPath;
                    //console.log(mapLegend);
                    removeUnauthorisedMapServices(data.d);
                    loadMapServices();
                }, function (err) {
                    console.log("ERROR :: " + url);
                    console.log(err);
                });
            } else {
                // DEV NOTE: (KC) IE9 XHR CORS issue ...http://stackoverflow.com/questions/14587347/dojo-cross-domain-access-is-denied
                xdr(url, {
                    method: "POST",
                    data: JSON.stringify({token: userToken})
                }).then(function (response) {
                    var data = JSON.parse(response, true);
                    removeUnauthorisedMapServices(data.d);
                    loadMapServices();
                }, function (err) {
                    console.log("ERROR :: " + url);
                    console.log(err);
                });
            }
        }

        function loadMapData() {
            /// <summary>
            /// Load all map data into the map.
            /// </summary>
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.loadMapData");
            }
            var mapServiceLayers = arrayUtils.map(config.MAP_SERVICES, function (mapService) {
                return mapService.MAPSERVICE_LAYER;
            });
            var featureLayers = arrayUtils.map(config.FEATURE_SERVICES, function (featService) {
                return featService.FEATURE_LAYER;
            });
            // Fetch additional layer properties after adding layers to the map
            map.on("layers-add-result", fetchLayerRESTProperties);
            var layers = mapServiceLayers.concat(featureLayers);
            map.addLayers(layers);
        }

        function createMapAddLayers() {
            /// <summary>
            /// Create the map, set the extent, and add the services to the map.
            /// <param name="projectionResult" type="esri/geometry/Geometry[]">
            ///   Projected geometries resulting from GeometryService.project()
            /// </param>
            /// </summary>
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.createMapAddLayers");
            }
            // Create new map
            var initialExtent = new Extent(config.INITIAL_MAP_EXTENT_XMIN, config.INITIAL_MAP_EXTENT_YMIN, config.INITIAL_MAP_EXTENT_XMAX, config.INITIAL_MAP_EXTENT_YMAX, new SpatialReference({wkid: 4283}));
            map = new Map("mapDiv", {logo: false, extent: initialExtent, fitExtent: true, scale: 4000000}); //DEV Note: Doesn't seem to honor scale when tiled map service used.
            map.resize();
            // << DevNote: Not required - resize issue was due to position: relative css on #mapDiv_root
            // Ensure map gets resized when expando panel is resized
            // var TOCContentCollapse = dom.byId("TOCContentCollapse");
            // on(TOCContentCollapse, "click", function () { map.resize(); });
            // var splitter = registry.byId("taskExpandoPanel_splitter");
            // on(splitter, "mouseLeave", function () { map.resize(); });
            // >>
            // Configure default geometry service
            esriConfig.defaults.geometryService = geomService;
            // Add basemapGallery
            addBaseMapGallery();
            // Add layers
            loadMapData();
            // Create selection graphics layer
            selectionGraphicsLayer = new GraphicsLayer();
            map.addLayer(selectionGraphicsLayer);
            // Create draw graphics layer
            drawGraphicsLayer = new GraphicsLayer();
            map.addLayer(drawGraphicsLayer);
            // Add map navigation widgets
            addMapNavigationWidget();
            // Add scalebar to map
            addScalebar();
            // Add Draw Widget
            addDrawWidget();
            // Add Measure Widget
            addMeasureWidget();
            // Add Map Scale Control widget
            addMapScaleWidget();
            // Add Search Wiget
            addSearchWidget();
            // Add Select Widget
            addSelectWidget();
            // Add Print Map Widget
            addPrintMapWidget();
            // Add Area Report Widget
            addAreaReportWidget();
            // Add Info Report Widget
            addInfoReportWidget();
            // Create the bookmark widget
            addBookmarkWidget();
            // Map is ready! - Hide the page load animation overlay
            if (DEBUG_TRACE_MODE) {
                console.log("      !!!END MAP LOADING!!!");
            }
            utility.page.endLoading();
            // add tabability to zoom level buttons on map
            setTimeout(function () { // delay this call due to rendering delay
                query(".esriSimpleSliderIncrementButton").forEach(function (node) {
                    domAttr.set(node, "tabindex", "0");
                    on(node, "keyup", function (evt) {
                        if (evt.which === 13) {
                            node.click(); // simulate a mouse click
                        }
                    });
                });
                query(".esriSimpleSliderDecrementButton").forEach(function (node) {
                    domAttr.set(node, "tabindex", "0");
                    on(node, "keyup", function (evt) {
                        if (evt.which === 13) {
                            node.click(); // simulate a mouse click
                        }
                    });
                });
            }, 1000);
        }

        function errorRetrievingLayer(err, layerName, layerURL) {
            var msg = err.error.message || "";
            if (msg.length < 1) msg = err.error.details[0];
            console.log("ERROR :: Retrieving Layer...");
            console.log(err);
            if (msg.length < 1) {
                showError("Retrieving layer: " + layerName, layerURL);
            } else {
                // Request cancelled messages should be ignored - usually happens on feature services when the user scrolls too fast
                if (!msg.startsWith("Unable to draw graphic (null): Request can")) {
                    showError("Retrieving layer: " + layerName, msg + "<BR><BR>URL: " + layerURL);
                }
            }
        }

        //------------------------------------//
        //        Layer REST Properties       //
        //------------------------------------//

        function fetchLayerRESTProperties(layers) {
            /// <summary>
            /// Fetch layer properties: metadata and opacity data, from the map service layer's REST endPoint.
            /// This data is pre-requisite data for the TOC Widget.
            /// Finally, once the many async requests are all responded to add the TOC Widget.
            /// </summary>
            if (DEBUG_TRACE_MODE) {
                console.log("======> startmap.fetchLayerRESTProperties");
            }
            var deferredList = [];
            arrayUtils.forEach(config.MAP_SERVICES, function (mapService) {
                var layer = mapService.MAPSERVICE_LAYER;
                mapService["METADATA"] = {};
                mapService["OPACITY"] = {};
                if (layer.layerInfos) {
                    arrayUtils.forEach(layer.layerInfos, function (layerInfo) {
                        deferredList.push(fetchMetadata(layerInfo.id, mapService.MAPSERVICE_URL, mapService["METADATA"]));
                        deferredList.push(fetchOpacity(layerInfo.id, mapService.MAPSERVICE_URL, mapService["OPACITY"]));
                    });
                }
            });
            // Wait for all async metadata and opacity requests to finish before finalising the TOC Widget
            var promises = all(deferredList);
            promises.then(createTOCLegendWidget); //create TOC Widget
        }

        function fetchMetadata(layerId, url, metadata) {
            /// <summary>
            /// Request the metadata from the description in the layer's REST endpoint.
            /// <returns type="deferred">deferred from esriRequest</returns>
            /// </summary>
            var layerUrl = url + "/" + layerId;
            var layerRequest = esriRequest({
                url: layerUrl,
                content: {f: "json"},
                handleAs: "json",
                callbackParamName: "callback",
                load: function (response) {
                    if (response["description"] !== "") {
                        var id = response["id"];
                        var desc = response["description"];
                        if (desc.startsWith("http")) { // check this is a http link
                            metadata[id] = desc; // set metadata
                        }
                    }
                }
            }); //}, {useProxy : true});
            return layerRequest;
        }

        function fetchOpacity(layerId, url, opacity) {
            /// <summary>
            /// Request the opacity from the drawingInfo->transparency of the layer's REST endpoint.
            /// <returns type="deferred">deferred from esriRequest</returns>
            /// </summary>
            var layerUrl = url + "/" + layerId;
            var layerRequest = esriRequest({
                url: layerUrl,
                content: {f: "json"},
                handleAs: "json",
                callbackParamName: "callback",
                load: function (response) {
                    if (response["drawingInfo"]) {
                        if (response["drawingInfo"]["transparency"] != null) {
                            var id = response["id"];
                            var trans = response["drawingInfo"]["transparency"];
                            var op = -((trans - 100) / 100); //convert transparency to opacity
                            opacity[id] = op; //set opacity
                        }
                    }
                }
            }); //}, {useProxy : true});
            return layerRequest;
        }

        //------------------------------------//
        //      TABLE OF CONTENTS WIDGET      //
        //------------------------------------//

        function createTOCLegendWidget() {
            /// <summary>
            /// Create the table of contents widget.
            /// </summary>
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.createTOCLegendWidget");
            }
            try {
                var mapServiceLayers = arrayUtils.map(config.MAP_SERVICES, function (mapService) {
                    return {
                        layer: mapService.MAPSERVICE_LAYER,
                        title: mapService.MAPSERVICE_NAME,
                        slider: true,
                        collapsed: false,
                        metadata: mapService["METADATA"],
                        opacity: mapService["OPACITY"]
                    };
                });
                var featureLayers = arrayUtils.map(config.FEATURE_SERVICES, function (featService) {
                    var metadata = {};
                    var id = featService.FEATURE_LAYER.id;
                    metadata[id] = featService.METADATA;
                    return {
                        layer: featService.FEATURE_LAYER,
                        title: featService.LAYER_NAME,
                        slider: true,
                        collapsed: true,
                        metadata: metadata
                    };
                });
                var layerInfos = [];
                layerInfos.push({
                    layer: drawGraphicsLayer,
                    title: "My Drawings",
                    slider: true,
                    collapsed: true,
                    metadata: null
                });
                layerInfos = layerInfos.concat(mapServiceLayers.concat(featureLayers).reverse());
                tocWidget = new TOC({
                    map: map,
                    layerInfos: layerInfos
                }, 'toc');
                tocWidget.startup();
            } catch (e) {
                console.log("ERROR :: startmap.createTOCLegendWidget...");
                console.log(e);
            }
        }

        function addBaseMapGallery() {
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.addBaseMapGallery");
            }
            var tiledMapServiceLayer = new ArcGISTiledMapServiceLayer(config.BASEMAP_2_URL);
            map.addLayer(tiledMapServiceLayer);
            var basemaps = [];
            var basemapLayer1 = new BasemapLayer({
                url: config.BASEMAP_1_URL
            });
            var basemap1 = new Basemap({
                id: config.BASEMAP_1_ID,
                layers: [basemapLayer1],
                title: config.BASEMAP_1_NAME,
                thumbnailUrl: config.BASEMAP_1_IMAGE
            });
            var basemapLayer2 = new BasemapLayer({
                url: config.BASEMAP_2_URL
            });
            var basemap2 = new Basemap({
                id: config.BASEMAP_2_ID,
                layers: [basemapLayer2],
                title: config.BASEMAP_2_NAME,
                thumbnailUrl: config.BASEMAP_2_IMAGE
            });
            basemaps.push(basemap2);
            basemaps.push(basemap1);
            baseMapGallery = new BasemapGallery({
                showArcGISBasemaps: false,
                basemaps: basemaps,
                map: map
            }, "basemapGallery");
            baseMapGallery.startup();
            baseMapGallery.select(config.BASEMAP_2_ID); // default to OEH Basemap
            // baseMapGallery.select(config.BASEMAP_1_ID); // default to SPOT 5 Basemap
            on(baseMapGallery, "error", function (error) {
                console.log("ERROR :: Basemap Gallery error...");
                console.log(error);
            });
        }

        //------------------------------------//
        //          GENERAL WIDGETS           //
        //------------------------------------//

        function addScalebar() {
            // Add scalebar to map
            var scalebar = new Scalebar({
                map: map,
                scalebarUnit: "metric" // "dual" = both miles & km, "english" = miles (default); "metric" = km
            });
        }

        function addBookmarkWidget() {
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.addBookmarkWidget");
            }
            // Create the bookmark widget
            var bookmark = new BookmarkWidget({
                    map: map,
                    baseMapGallery: baseMapGallery,
                    bookmarks: [],
                    editable: true,
                    //otherSerialisableData is optional
                    otherSerialisableData_Get: lang.hitch(searchWidget, searchWidget.getSearchCriteria),
                    otherSerialisableData_Set: lang.hitch(searchWidget, searchWidget.setSearchCriteria)
                }, dom.byId('bookmark')
            );
        }

        //------------------------------------//
        //         MAP GRAPHIC WIDGETS        //
        //------------------------------------//

        function addDrawWidget() {
            /// <summary>
            /// Adds draw widget to the page.
            /// </summary>
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.addDrawWidget");
            }
            drawWidget = new DrawWidget({
                map: map,
                graphicsLayer: drawGraphicsLayer
            }, "drawPanel");
            drawWidget.startup();
            // Deactivate the draw tools when draw task panel is hidden
            on(registry.byId("panelDraw"), "hide", function (evt) {
                drawWidget.deactivateDrawWidget();
            });
            // Activate the draw tools when draw task panel is shown
            on(registry.byId("panelDraw"), "show", function (evt) {
                drawWidget.activateDrawWidget();
            });
        }

        function addSearchWidget() {
            /// <summary>
            /// Adds search widget to the page.
            /// </summary>
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.addSearchWidget");
            }
            searchWidget = new SearchWidget({
                map: map,
                graphicsLayer: selectionGraphicsLayer,
                searchConfig: config.MAP_SERVICES[0].SEARCH  // NOTE: MapServices[0] determined by user role
            }, "searchPanel");
            searchWidget.startup();
            searchWidget.on("before_select", function (evt) {
                // Clear selection from SELECT widget before searching
                if (currentSelectionWidget == "SELECT") {
                    selectWidget.clear();
                }
                currentSelectionWidget = "SEARCH";
            });
            searchWidget.on("selection_change", function (evt) {
                runInfoReport(evt.selectedGraphics);
                runAreaReport(evt.selectedGraphics);
            });
        }

        function addSelectWidget() {
            /// <summary>
            /// Adds select widget to the page.
            /// </summary>
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.addSelectWidget");
            }
            selectWidget = new SelectWidget({
                map: map,
                graphicsLayer: selectionGraphicsLayer,
                geomService: geomService,
                selectConfig: config.MAP_SERVICES[0].SELECTION_LAYERS  // NOTE: MapServices[0] determined by user role
            }, "selectPanel");
            selectWidget.startup();
            selectWidget.on("before_select", function (evt) {
                // Clear selection from SEARCH widget before selecting
                if (currentSelectionWidget == "SEARCH") {
                    searchWidget.clear();
                }
                currentSelectionWidget = "SELECT";
            });
            selectWidget.on("selection_change", function (evt) {
                runInfoReport(evt.selectedGraphics);
                clearAreaReport(true);
            });

            // Deactivate the select tools when select task panel is hidden
            on(registry.byId("panelSelect"), "hide", function (evt) {
                selectWidget.deactivateSelectWidget();
            });
            // Activate the select tools when select task panel is shown
            on(registry.byId("panelSelect"), "show", function (evt) {
                selectWidget.activateSelectWidget();
            });
        }

        function addMeasureWidget() {
            /// <summary>
            /// Adds measure widget to the page.
            /// </summary>
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.addMeasureWidget");
            }
            measurementWidget = new Measurement({
                map: map,
                defaultAreaUnit: units.SQUARE_METERS,
                defaultLengthUnit: units.METERS
            }, "measurePanel");
            measurementWidget.startup();
            // Deactivate the measure tools when measure task panel is hidden
            on(registry.byId("panelMeasure"), "hide", function (evt) {
                measurementWidget.clearResult();
                measurementWidget.setTool("area", false);
                measurementWidget.setTool("distance", false);
                measurementWidget.setTool("location", false);
                measurementWidgetActive = false;
                // Re-activate identify if no measure tool selected
                map.setInfoWindowOnClick(true);
            });
            on(registry.byId("panelMeasure"), "show", function (evt) {
                measurementWidgetActive = true;
                forceGraphicsLayerActive();
            });
            on(measurementWidget, "tool-change", function (evt) {
                map.setInfoWindowOnClick(false);
            });
            on(map, "mouse-down", function (evt) {
                //console.log(map.extent.xmin);
                //console.log(map.extent.ymin);
                //console.log(map.extent.xmax);
                //console.log(map.extent.ymax);
                if (measurementWidgetActive) {
                    // Re-activate identify if no measure tool is selected
                    var tool = measurementWidget.getTool();
                    if (typeof tool === "undefined") {
                        map.setInfoWindowOnClick(true);
                    }
                    // Force graphics layer on when using this widget
                    forceGraphicsLayerActive();
                }
            });
        }

        function forceGraphicsLayerActive() {
            if (map.graphics.visible == false) {
                map.graphics.setVisibility(true);
            }
        }

        //------------------------------------//
        //            REPORT WIDGETS          //
        //------------------------------------//

        function showResultsPanel() {
            //var resultsPanel = registry.byId("resultsExpandoPanel");
            //if (!resultsPanel._showing) {
            //  resultsPanel.toggle();
            //}
            // Select report tool if not already selected (shows the results panel)
            var reportTool = dom.byId("toolReport");
            if (!domClass.contains(reportTool, "selected")) {
                reportTool.click();
            }
        }

        function addAreaReportWidget() {
            /// <summary>
            /// Adds search widget to the page.
            /// </summary>
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.addAreaReportWidget");
            }
            areaReportWidget = new AreaReportWidget({
                map: map,
                mapId: config.MAP_SERVICES[0].MAPSERVICE_ID,
                userToken: userToken,
                webServiceUrl: config.AREA_AGGREGATE_WEB_SERVICE_URL,
                handlerUrl: config.PRINT_REPORT_HANDLER_URL,
                layerConfig: config.MAP_SERVICES[0].SEARCH,
                conservationCommitmentLayerIds: config.MAP_SERVICES[0].CONSERVATION_COMMITMENT_LAYER_IDS,
                printTimeout: config.PRINTTASK_TIMEOUT
            }, "areaReport");
            areaReportWidget.startup();
        }

        function addInfoReportWidget() {
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.addInfoReportWidget");
            }
            infoReportWidget = new InfoReportWidget({
                map: map,
                geomService: geomService,
                mapId: config.MAP_SERVICES[0].MAPSERVICE_ID,
                userToken: userToken,
                webServiceUrl: config.INFO_TOOL_WEB_SERVICE_URL,
                handlerUrl: config.PRINT_REPORT_HANDLER_URL,
                lotSearchLayerConfig: config.MAP_SERVICES[0].SEARCH.LOT_SEARCH,
                conservationCommitmentLayerIds: config.MAP_SERVICES[0].CONSERVATION_COMMITMENT_LAYER_IDS,
                printTimeout: config.PRINTTASK_TIMEOUT
            }, "infoReport");
            infoReportWidget.startup();
        }

        function clearInfoReport() {
            infoReportWidget.ClearReport();
        }

        function clearAreaReport(invalidSelection) {
            areaReportWidget.ClearReport(invalidSelection && areaReportChecked);
        }

        function runInfoReport(selectedGraphics) {
            infoReportWidget.SetCurrentSelection(selectedGraphics);
            if (infoReportChecked) {
                showResultsPanel();
                infoReportWidget.RunReport(selectedGraphics);
            }
        }

        function runAreaReport(selectedGraphics) {
            areaReportWidget.SetCurrentSelection(selectedGraphics);
            if (areaReportChecked) {
                showResultsPanel();
                areaReportWidget.RunReport(selectedGraphics);
            }
        }

        //------------------------------------//
        //       MAP NAVIGATION WIDGETS       //
        //------------------------------------//

        function addMapNavigationWidget() {
            /// <summary>
            /// Adds map navigation widget to the map.
            /// </summary>
            var initialExtent = new Extent(config.INITIAL_MAP_EXTENT_XMIN, config.INITIAL_MAP_EXTENT_YMIN, config.INITIAL_MAP_EXTENT_XMAX, config.INITIAL_MAP_EXTENT_YMAX, new SpatialReference({wkid: 4283}));
            var mapNavigationWidget = new MapNavigationWidget({
                map: map,
                extent: initialExtent
            }, "mapNavigation");
            mapNavigationWidget.startup();
        }

        function addMapScaleWidget() {
            /// <summary>
            /// Adds map scale control to the map.
            /// </summary>
            var mapScaleControl = new MapScaleControlWidget({
                map: map,
                scaleOptions: [8000000, 4000000, 2000000, 1000000, 500000, 250000, 125000, 64000, 32000, 16000, 8000]
            }, "mapScaleControl");
            mapScaleControl.startup();
        }

        //------------------------------------//
        //            MAP PRINTING            //
        //------------------------------------//

        function addPrintMapWidget() {
            if (DEBUG_TRACE_MODE) {
                console.log("====> startmap.addPrintMapWidget");
            }
            var url = config.ADMIN_WEB_SERVICE_URL + "/getBISVRoleLayouts";
            if (!window.XDomainRequest) {
                xhr.post(url, {
                    handleAs: "json",
                    data: JSON.stringify({token: userToken}),
                    headers: {'Content-Type': 'application/json'}
                }).then(function (data) {
                    configurePrintMapWidget(data.d);
                }, function (err) {
                    console.log("ERROR :: " + url);
                    console.log(err);
                });
            } else {
                // DEV NOTE: (KC) IE9 XHR CORS issue ...http://stackoverflow.com/questions/14587347/dojo-cross-domain-access-is-denied
                xdr(url, {
                    method: "POST",
                    data: JSON.stringify({token: userToken})
                }).then(function (response) {
                    var data = JSON.parse(response, true);
                    configurePrintMapWidget(data.d);
                }, function (err) {
                    console.log("ERROR :: " + url);
                    console.log(err);
                });
            }
        }

        function configurePrintMapWidget(mapTemplates) {
            printWidget = new PrintWidget({
                map: map,
                printTimeout: config.PRINTTASK_TIMEOUT,
                titleMaxChar: config.PRINT_MAP_TITLE_MAX_CHARS,
                printMapTitleDefault: config.PRINT_MAP_TITLE_DEFAULT,
                mapTemplates: mapTemplates,
                mapServiceConnectionFile: config.MAP_SERVICE_CONNECTION_FILE,
                mapLegend: mapLegend,
                printMapServiceUrl: config.PRINT_MAP_SERVICE_URL
            }, "printPanel");
            printWidget.startup();
        }

        //------------------------------------//
        //              GENERAL               //
        //------------------------------------//

        function showError(title, message, warning) {
            /// <summary>
            /// Displays an error message in a dialog box to the user.
            /// <param name="title" type="string">Title is shown in the dialog header after "! ERROR ! - ".</param>
            /// <param name="message" type="string">Error message to display in the content section of the dialog.</param>
            /// </summary>
            utility.page.endLoading();
            errDialog = new Dialog({
                title: (warning ? "<b style='color:red'>  Warning!  " + title + "</b>" : "<b style='color:red'>! ERROR !  " + title + "</b>"),
                content: message,
                style: "width: 500px"
            });
            errDialog.show();
        }

        //------------------------------------//
        //          PUBLIC METHODS            //
        //------------------------------------//

        return {
            start: start
        };

    }
);
