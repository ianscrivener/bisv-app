/*
 | Author: NGIS 2014 (www.ngis.com.au) -> Kristy Carter
 | Module: WireUp
 | Description: Contains javascript to wire and handle events for general 
 |              dom nodes and dijits.
 | Note: In order to use the content collapse, the expandoPane MUST have a minSize set in the data-dojo-props.
 |
 | CHANGE LOG: 
 |   2015-03-12 KC: Initial release - production.
 */
define([
        "dojo/parser",
        "dojo/on",
        "dojo/dom",
        "dojo/dom-class",
        "dojo/dom-prop",
        "dojo/dom-style",
        "dojo/ready",
        "dijit/registry",
        "dojox/layout/ExpandoPane",
        "dijit/layout/AccordionContainer",
        "dijit/layout/ContentPane",
        "dijit/layout/BorderContainer",
        "dojo/domReady!"
    ],
    function (parser, on, dom, domClass, domProp, domStyle, ready, registry, expandoPane) {
        var DEBUG_TRACE_MODE = true;
        if (DEBUG_TRACE_MODE) {
            console.log("==> wireup");
        }
        parser.parse();

        ready(function () {
            if (DEBUG_TRACE_MODE) {
                console.log("====> wireup.ready");
            }

            var mainToolbar = dom.byId("mainToolbar");
            var searchTool = dom.byId("toolSearch");
            var selectTool = dom.byId("toolSelect");
            var drawTool = dom.byId("toolDraw");
            var measureTool = dom.byId("toolMeasure");
            var bookmarkTool = dom.byId("toolBookmark");
            var reportTool = dom.byId("toolReport");
            var printTool = dom.byId("toolPrint");
            var helpTool = dom.byId("toolHelp");
            var tocAccordion = registry.byId("taskStack");
            var selectedTool;

            var tocExpandoPane = registry.byId("taskExpandoPanel");
            var splitter = registry.byId("taskExpandoPanel_splitter");
            var splitterReports = registry.byId("resultsExpandoPanel_splitter");
            var reportResultsPane = registry.byId("resultsExpandoPanel");
            var TOCContentCollapse = dom.byId("TOCContentCollapse");

            var reportEnterClick = function (evt) {
                if (evt.which === 13) {
                    reportResultsPane.toggle();
                }
            };
            var toggleResultsPanel = function () {
                reportResultsPane.toggle();
            };
            var toggleResultsTool = function () {
                // Note: expando's _showing property is being set after the panel is expanded or collapsed so is not immediatly available - setting a 0.25 sec delay to ensure appropriate behaviour
                setTimeout(function () {
                    //console.log(reportResultsPane._showing);
                    if (!reportResultsPane._showing) {
                        domClass.remove(reportTool, "selected");
                    } else {
                        //console.log('set selected');
                        domClass.add(reportTool, "selected");
                    }
                }, 500); //0.5 sec delay
            };

            // Wiring for main toolbar button click events
            var resetTools = function () {
                if (!domClass.contains(searchTool, "toggleButton")) {
                    domClass.remove(searchTool, "selected");
                }
                if (!domClass.contains(selectTool, "toggleButton")) {
                    domClass.remove(selectTool, "selected");
                }
                if (!domClass.contains(drawTool, "toggleButton")) {
                    domClass.remove(drawTool, "selected");
                }
                if (!domClass.contains(measureTool, "toggleButton")) {
                    domClass.remove(measureTool, "selected");
                }
                if (!domClass.contains(bookmarkTool, "toggleButton")) {
                    domClass.remove(bookmarkTool, "selected");
                }
                if (!domClass.contains(reportTool, "toggleButton")) {
                    domClass.remove(reportTool, "selected");
                }
                if (!domClass.contains(printTool, "toggleButton")) {
                    domClass.remove(printTool, "selected");
                }
                if (!domClass.contains(helpTool, "toggleButton")) {
                    domClass.remove(helpTool, "selected");
                }
            };

            var toolbarEnterClick = function (evt) {
                if (evt.which === 13) {
                    toolbarClick(evt);
                }
            };
            var toolbarClick = function (evt) {
                if (evt.target.id === "mainToolbar" || evt.target.id.indexOf("tool") < 0) {
                    return;
                }
                showExpandoPane();
                if (domClass.contains(evt.target, "toggleButton")) { // Report is a toggle button
                    if (!domClass.contains(evt.target, "disabled")) {
                        if (!domClass.contains(evt.target, "selected")) {
                            domClass.add(evt.target, "selected");
                        } else {
                            domClass.remove(evt.target, "selected");
                        }
                    }
                    return;
                }
                if (!domClass.contains(evt.target, "disabled")) {
                    // style the selected TOC tool
                    resetTools();
                    domClass.add(evt.target, "selected");
                    // show the related TOC
                    tocAccordion.selectChild(evt.target.id.replace("tool", "panel"));
                    selectedTool = evt.target;
                }
            };
            var collapseOrExpandTOC = function (evt) {
                if (domClass.contains(TOCContentCollapse, "open")) {
                    tocExpandoPane.toggle();
                    domClass.remove(TOCContentCollapse, "open");
                    domClass.add(TOCContentCollapse, "closed");
                } else {
                    tocExpandoPane.toggle();
                    domClass.remove(TOCContentCollapse, "closed");
                    domClass.add(TOCContentCollapse, "open");
                }
            };
            var showExpandoPane = function () {
                if (domClass.contains(TOCContentCollapse, "closed")) {
                    tocExpandoPane.toggle();
                    domClass.remove(TOCContentCollapse, "closed");
                    domClass.add(TOCContentCollapse, "open");
                }
            };
            var resizeExpandoPane = function () {
                if (domStyle.get(tocExpandoPane, "width") > 200) {
                    domClass.remove(TOCContentCollapse, "closed");
                    domClass.add(TOCContentCollapse, "open");
                }
            };

            on(mainToolbar, "click", toolbarClick); // hook-up tool click event to toolbar div (note: intercepts child tool clicks)
            on(mainToolbar, "keypress", toolbarEnterClick); // tab enter functionality
            on(reportTool, "click", toggleResultsPanel);
            on(reportTool, "keypress", reportEnterClick); // tab enter functionality
            on(reportResultsPane, "click", toggleResultsTool);
            //on(reportResultsPane, "dblclick", toggleResultsTool); //not needed - click gets 2 events
            on(TOCContentCollapse, "click", collapseOrExpandTOC);
            on(splitter, "mouseLeave", resizeExpandoPane);
            on(splitterReports, "mouseLeave", toggleResultsTool);
            bookmarkTool.click();
            //searchTool.click();

        });
    }
);